/* eslint-disable func-names */
const passport = require('passport')
const { BasicStrategy } = require('passport-http')
const boom = require('@hapi/boom')
const axios = require('axios')

require('dotenv').config()

passport.use(
  new BasicStrategy(async function (email, password, cb) {
    const userData = {
      email: email,
      password: password,
      device_token: 'jjklk'
    }

    try {
      const { data, status } = await axios({
        url: `${process.env.API_URL}/login`,
        method: 'post',
        data: userData
      })

      if (status !== 200) {
        return cb(boom.unauthorized(), false)
      }

      return cb(null, data)
    } catch (error) {
      cb(error)
    }
  })
)
