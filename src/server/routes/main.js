import React from 'react'
import { renderToString } from 'react-dom/server'
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import { StaticRouter } from 'react-router'
import { renderRoutes } from 'react-router-config'
import serverRoutes from '../../frontend/routes/serverRoutes'
import { MainLayout } from '../../frontend/layouts/MainLayout'
import reducer from '../../frontend/reducers'
import initialState from '../../frontend/initialState'
import render from '../render'
import axios from 'axios'

require('dotenv')

const main = async (req, res, next) => {
  var moment = require('moment')
  require('moment/locale/es')
  try {
    let getInitialState
    try {
      const { token, role } = req.cookies
      let user = {}
      if (role) {
        user = {
          role
        }
      }

      let API_PERSONAL_URL

      if (role) {
        if (role === 'student') {
          API_PERSONAL_URL = 'student/get/initial-data'
        } else if ((role === 'guardian')) {
          API_PERSONAL_URL = 'guardian/get/initial-data'
        } else if ((role === 'residence')) {
          API_PERSONAL_URL = 'residence/get/initial-data'
        } else {
          API_PERSONAL_URL = 'kitchen/get/initial-data'
        }
      }

      const initialData = await axios({
        url: `${process.env.API_URL}/${API_PERSONAL_URL}?version=2`,
        headers: { Authorization: `Bearer ${token}` },
        method: 'get'
      }).then((response) => {
        console.log('respuesta', response)
        let startInitialData

        if (role === 'student') {
          startInitialData = response.data
        } else if ((role === 'guardian')) {
          startInitialData = response.data[0].studentData
        } else if ((role === 'residence')) {
          startInitialData = response.data
        } else {
          startInitialData = response.data
        }

        getInitialState = {
          user,
          initialData: startInitialData,
          tutorStudentsInfo: (role === 'guardian') ? response.data.map(item => item.studentData.userInfo) : []
        }
      })
      // if ((role === 'residence')) {
      //   const initialFoodPermit = await axios({
      //     url: 'https://campuslaude.herokuapp.com/api/residence/get/food-permits-by-day?date=moment().format(\'YYYY-MM-DD HH:mm:ss\')',
      //     headers: { Authorization: `Bearer ${token}` },
      //     method: 'GET'
      //   }).then((response) => {
      //     console.log('permisos', response)
      //     getInitialState = {
      //       ...getInitialState,
      //       initialFoodPermit: initialFoodPermit
      //     }
      //   })
      // }
      // PROVISIONAL DUMMY
      if (role === 'residence') {
        getInitialState = {
          ...getInitialState,
          referenceFoodDate: moment().format('YYYY-MM-DD HH:mm:ss'),
          referenceTonightDate: moment().format('YYYY-MM-DD HH:mm:ss'),
          referenceExtraordinaryDate: moment().format('YYYY-MM-DD HH:mm:ss'),
          referenceAbscenceDate: moment().format('YYYY-MM-DD HH:mm:ss'),
          initialFoodPermit: [
            {
              id: 876,
              studentName: 'Alberto Trizzino',
              studentAvatar: {
                url: 'https://picsum.photos/id/100/100/100'
              },
              permitName: 'almuerzo',
              eatHere: true,
              whenIeat: 'after',
              note: 'Comer despues',
              picnic: false,
              picnic_date: null
            },
            {
              id: 877,
              studentName: 'Cristian Barceló',
              studentAvatar: {
                url: 'https://picsum.photos/id/100/100/100'
              },
              permitName: 'almuerzo',
              eatHere: false,
              whenIeat: 'onTime',
              note: '',
              picnic: true,
              picnic_date: '2020-09-25 15:30:00'
            },
            {
              id: 876,
              studentName: 'Alberto Trizzino',
              studentAvatar: {
                url: 'https://picsum.photos/id/100/100/100'
              },
              permitName: 'cena',
              eatHere: true,
              whenIeat: 'after',
              note: 'Comer despues',
              picnic: false,
              picnic_date: null
            },
            {
              id: 877,
              studentName: 'Cristian Barceló',
              studentAvatar: {
                url: 'https://picsum.photos/id/100/100/100'
              },
              permitName: 'cena',
              eatHere: false,
              whenIeat: 'onTime',
              note: '',
              picnic: true,
              picnic_date: '2020-09-25 15:30:00'
            }
          ],
          initialTonightPermit: [
            {
              id: 876,
              studentName: 'Alberto Trizzino',
              studentAvatar: {
                url: 'https://picsum.photos/id/100/100/100'
              },
              type: 'tonight',
              status: 'validated',
              dayStart: '2020-07-15 23:00:00',
              note: ''
            },
            {
              id: 877,
              studentName: 'Cristian Barceló',
              studentAvatar: {
                url: 'https://picsum.photos/id/100/100/100'
              },
              type: 'tonight',
              status: 'validated',
              dayStart: '2020-07-15 23:00:00',
              note: ''
            }
          ],
          initialPermit: [
            {
              id: 976,
              studentName: 'Alberto Trizzino',
              studentAvatar: {
                url: 'https://picsum.photos/id/100/100/100'
              },
              type: 'extraordinary',
              status: 'validated',
              dayStart: '2020-07-15 23:00:00',
              comment: '',
              reason: 'pernoctar'
            },
            {
              id: 977,
              studentName: 'Cristian Barceló',
              studentAvatar: {
                url: 'https://picsum.photos/id/100/100/100'
              },
              type: 'extraordinary',
              status: 'pending',
              dayStart: '2020-07-15 23:00:00',
              comment: '',
              reason: 'pernoctar'
            },
            {
              id: 978,
              studentName: 'Cristian Barceló',
              studentAvatar: {
                url: 'https://picsum.photos/id/100/100/100'
              },
              type: 'extraordinary',
              status: 'pending-rejected',
              dayStart: '2020-07-15 23:00:00',
              comment: 'Aqui hay una nota',
              reason: 'other'
            },
            {
              id: 776,
              studentName: 'Alberto Trizzino',
              studentAvatar: {
                url: 'https://picsum.photos/id/100/100/100'
              },
              type: 'abscence',
              status: 'validated',
              dayStart: '2020-07-15 23:00:00',
              dayEnd: '2020-07-21 23:00:00',
              comment: '',
              reason: 'fun'
            },
            {
              id: 777,
              studentName: 'Cristian Barceló',
              studentAvatar: {
                url: 'https://picsum.photos/id/100/100/100'
              },
              type: 'abscence',
              status: 'pending',
              dayStart: '2020-07-15 23:00:00',
              dayEnd: '2020-07-21 23:00:00',
              comment: '',
              reason: 'academic'
            },
            {
              id: 778,
              studentName: 'Cristian Barceló',
              studentAvatar: {
                url: 'https://picsum.photos/id/100/100/100'
              },
              type: 'abscence',
              status: 'pending-rejected',
              dayStart: '2020-07-15 23:00:00',
              dayEnd: '2020-07-21 23:00:00',
              comment: 'Aqui hay una nota',
              reason: 'other'
            }
          ]
        }
      }

      if (role === 'kitchen') {
        getInitialState = {
          ...getInitialState,
          referenceFoodDate: moment().format('YYYY-MM-DD HH:mm:ss'),
          initialFoodPermit: [
            {
              id: 876,
              studentName: 'Alberto Trizzino',
              studentAvatar: {
                url: 'https://picsum.photos/id/100/100/100'
              },
              permitName: 'almuerzo',
              eatHere: true,
              whenIeat: 'after',
              note: 'Comer despues',
              picnic: false,
              picnic_date: null
            },
            {
              id: 877,
              studentName: 'Cristian Barceló',
              studentAvatar: {
                url: 'https://picsum.photos/id/100/100/100'
              },
              permitName: 'almuerzo',
              eatHere: false,
              whenIeat: 'onTime',
              note: '',
              picnic: true,
              picnic_date: '2020-09-25 15:30:00'
            },
            {
              id: 876,
              studentName: 'Alberto Trizzino',
              studentAvatar: {
                url: 'https://picsum.photos/id/100/100/100'
              },
              permitName: 'cena',
              eatHere: true,
              whenIeat: 'after',
              note: 'Comer despues',
              picnic: false,
              picnic_date: null
            },
            {
              id: 877,
              studentName: 'Cristian Barceló',
              studentAvatar: {
                url: 'https://picsum.photos/id/100/100/100'
              },
              permitName: 'cena',
              eatHere: false,
              whenIeat: 'onTime',
              note: '',
              picnic: true,
              picnic_date: '2020-09-25 15:30:00'
            }
          ]
        }
      }
    } catch (err) {
      getInitialState = initialState
      console.log(err)
    }

    const isLogged = getInitialState.user.role
    const store = createStore(reducer, getInitialState)
    const html = renderToString(
      <Provider store={store}>
        <StaticRouter
          context={{}}
          location={req.url}
        >
          <MainLayout>
            {renderRoutes(serverRoutes(isLogged))}
          </MainLayout>
        </StaticRouter>
      </Provider>
    )
    const preloadedState = store.getState()
    res.send(render(html, preloadedState))
  } catch (err) {
    next(err)
  }
}

export default main
