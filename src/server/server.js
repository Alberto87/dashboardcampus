import express from 'express'
import dotenv from 'dotenv'
import webpack from 'webpack'
import helmet from 'helmet'
import main from './routes/main'
import cookieParser from 'cookie-parser'
import boom from '@hapi/boom'
import passport from 'passport'
import axios from 'axios'

dotenv.config()

const app = express()
const ENV = process.env.NODE_ENV
const PORT = process.env.PORT || 3000
app.use(cookieParser())
app.use(express.json())
app.use(express.static(`${__dirname}/public`))
app.use(passport.initialize())
app.use(passport.session())

require('./utils/auth/strategies/basic')

if (ENV === 'development') {
  console.log('Loading dev config')
  const webpackConfig = require('../../webpack.config')
  const webpackDevMiddleware = require('webpack-dev-middleware')
  const webpackHotMiddleware = require('webpack-hot-middleware')
  const compiler = webpack(webpackConfig)
  const serverConfig = {
    contentBase: `http://localhost${PORT}`,
    port: PORT,
    publicPath: webpackConfig.output.publicPath,
    hot: true,
    historyApiFallback: true,
    stats: { colors: true }
  }
  app.use(webpackDevMiddleware(compiler, serverConfig))
  app.use(webpackHotMiddleware(compiler))
} else {
  console.log('Loading prod config')
  app.use(helmet())
  app.use(helmet.permittedCrossDomainPolicies())
  app.disable('x-powered-by')
}

// LogIn Endpoint

app.post('/auth/sign-up', async function (req, res, next) {
  const { body: user } = req

  const details = {
    email: user.email,
    password: user.password,
    device_token: user.deviceToken
  }

  let formBody = []

  for (const property in details) {
    const encodedKey = encodeURIComponent(property)
    const encodedValue = encodeURIComponent(details[property])
    formBody.push(encodedKey + '=' + encodedValue)
  }
  formBody = formBody.join('&')
  try {
    const userData = await axios({
      url: `${process.env.API_URL}/login`,
      method: 'post',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        Accept: 'application/json'
      },
      data: formBody
    })
    if (res.status(200)) {
      res.cookie('token', userData.data.token, {
        httpOnly: !(ENV === 'development'),
        secure: !(ENV === 'development')
      })
      res.cookie('role', userData.data.role, {
        httpOnly: !(ENV === 'development'),
        secure: !(ENV === 'development')
      })
    }
    res.status(200).json({
      email: req.body.email,
      role: userData.data.role
    })
  } catch (error) {
    console.log(error)
    next(error)
  }
})

// Recovery password Endpoint

app.post('/auth/recovery-password', async function (req, res, next) {
  const { email } = req.body
  console.log('esta es la mail que recibo:', email)
  try {
    const userData = await axios({
      url: `${process.env.API_URL}/forgot-password`,
      method: 'post',
      data: {
        email: email
      }
    })
    if (res.status(200)) {
      console.log(res)
    }
    res.status(200).json(userData.data)
  } catch (error) {
    console.log(error)
    next(error)
  }
})

// LogOUT Endpoint

app.post('/auth/sign-out', async function (req, res, next) {
  const { token } = req.cookies

  try {
    const signOut = await axios({
      url: `${process.env.API_URL}/logout`,
      headers: { Authorization: `Bearer ${token}` },
      method: 'POST'
    })
    if (res.status(200)) {
      res.clearCookie('token')
      res.status(200).json({})
      res.end()
    }
  } catch (error) {
    console.log(error)
    next(error)
  }
})

// Check matching food permit

app.post('/checking-food-permit', async function (req, res, next) {
  const { token } = req.cookies
  const { dateToSend, name } = req.body

  const foodName = encodeURIComponent(name)
  const encodedDate = encodeURIComponent(dateToSend)
  let id
  try {
    const matchingPermit = await axios({
      url: `${process.env.API_URL}/student/get/is-meal-in-date?date=${encodedDate}&foodName=${foodName}`,
      method: 'get',
      headers: { Authorization: `Bearer ${token}` }
    })
    if (res.status(200)) {
      id = (matchingPermit.data === true) ? 0 : matchingPermit.data[0].id
      console.log(matchingPermit.data)
    }
    res.status(200).json(id)
  } catch (error) {
    console.log(error)
    next(error)
  }
})

// Add food permit Endpoint

app.post('/add-food-permit', async function (req, res, next) {
  const { token } = req.cookies
  const permitDetail = req.body

  const jsonToSend = encodeURIComponent(JSON.stringify(permitDetail))

  const formBody = 'permission_meal=' + jsonToSend

  try {
    const userData = await axios({
      url: `${process.env.API_URL}/student/insert/student-permission-meal`,
      method: 'post',
      headers: { Authorization: `Bearer ${token}` },
      data: formBody
    })
    if (res.status(200)) {
      console.log('respuest', userData)
    }
    res.status(200).json(userData.data)
  } catch (error) {
    console.log(error)
    next(error)
  }
})

// Modify food permit Endpoint

app.post('/modify-food-permit', async function (req, res, next) {
  const { token } = req.cookies
  const permitDetail = req.body

  const permtitToModify = {
    id: permitDetail.id,
    date: permitDetail.orderedDate,
    eatHere: permitDetail.eatHere,
    picnic: permitDetail.picnic,
    onTime: (permitDetail.whenEat === 'onTime'),
    anticipation: (permitDetail.whenEat === 'before'),
    delay: (permitDetail.whenEat === 'after'),
    mealName: permitDetail.permitName,
    picnic_date: permitDetail.picnic_date,
    note: permitDetail.note
  }

  const jsonToSend = encodeURIComponent(JSON.stringify(permtitToModify))

  const formBody = 'permission_meal=' + jsonToSend

  try {
    const permitData = await axios({
      url: `${process.env.API_URL}/student/update/student-permission-meal`,
      method: 'post',
      headers: { Authorization: `Bearer ${token}` },
      data: formBody
    })
    if (res.status(200)) {
      console.log('respuest', permitData)
    }
    res.status(200).json(permitDetail)
  } catch (error) {
    console.log(error)
    next(error)
  }
})

// Delete food permit Endpoint

app.post('/delete-food-permit', async function (req, res, next) {
  console.log('estoy controlando')
  const { token } = req.cookies
  const { id } = req.body

  try {
    const matchingPermit = await axios({
      url: `${process.env.API_URL}/student/update/cancel-student-permission-meal`,
      method: 'post',
      headers: { Authorization: `Bearer ${token}` },
      data: {
        id: id
      }
    })
    if (res.status(200)) {
      console.log(matchingPermit.data)
    }
    res.status(200).json(id)
  } catch (error) {
    console.log(error)
    next(error)
  }
})

// Add tonight permit endpoints

app.post('/add-tonight-permit', async function (req, res, next) {
  const { token } = req.cookies
  const { dateToSend } = req.body

  try {
    const userData = await axios({
      url: `${process.env.API_URL}/student/insert/tonight-permission`,
      method: 'post',
      headers: { Authorization: `Bearer ${token}` },
      data: {
        date: dateToSend
      }
    })
    if (res.status(200)) {
      console.log('respuest', userData)
    }
    res.status(200).json(userData.data)
  } catch (error) {
    console.log(error)
    next(error)
  }
})

// Add extraordinary permit endpoints

app.post('/add-extraordinary-permit', async function (req, res, next) {
  const { token } = req.cookies
  const { dateToSend, reason, note } = req.body

  try {
    const userData = await axios({
      url: `${process.env.API_URL}/student/insert/extraordinary-permission`,
      method: 'post',
      headers: { Authorization: `Bearer ${token}` },
      data: {
        date: dateToSend,
        reason: reason,
        note: note
      }
    })
    if (res.status(200)) {
      console.log('respuest', userData)
    }
    res.status(200).json(userData.data)
  } catch (error) {
    console.log(error)
    next(error)
  }
})

// Modify extraordinary permit endpoints

app.post('/modify-extraordinary-permit', async function (req, res, next) {
  const { token } = req.cookies
  const permitDetail = req.body
  try {
    const permitData = await axios({
      url: `${process.env.API_URL}/student/update/extraordinary-permission`,
      method: 'post',
      headers: { Authorization: `Bearer ${token}` },
      data: {
        id: permitDetail.id,
        date: permitDetail.dayStart,
        reason: permitDetail.reason,
        note: permitDetail.note
      }
    })
    if (res.status(200)) {
      console.log('respuest', permitData)
    }
    res.status(200).json(permitData.data)
  } catch (error) {
    console.log(error)
    next(error)
  }
})

// Check matching absecnce permit

app.post('/checking-absence-permit', async function (req, res, next) {
  console.log('estoy controlando')
  const { token } = req.cookies
  const { dayStart, dayEnd } = req.body

  const encodeDateCheckIn = encodeURIComponent(dayStart)
  const encodeDateCheckOut = encodeURIComponent(dayEnd)

  try {
    const matchingPermit = await axios({
      url: `${process.env.API_URL}/student/get/is-abscence-permissions-in-date?date_end=${encodeDateCheckOut}&date_start=${encodeDateCheckIn}`,
      method: 'get',
      headers: { Authorization: `Bearer ${token}` }
    })
    if (res.status(200)) {
      console.log(matchingPermit.data)
    }
    res.status(200).json(matchingPermit.data)
  } catch (error) {
    console.log(error)
    next(error)
  }
})

// Add abscence permit endpoints

app.post('/add-abscence-permit', async function (req, res, next) {
  const { token } = req.cookies
  const { dayStart, dayEnd, reason, note } = req.body

  const dataRecived = {
    dayStart: dayStart,
    dayEnd: dayEnd,
    reason: reason,
    comment: note,
    comidaCheckIn: ['', ''],
    comidaCheckOut: ['', '']
  }

  const dataToSend = encodeURIComponent(JSON.stringify(dataRecived))

  const formBody = 'abscence_permission=' + dataToSend

  try {
    const userData = await axios({
      url: `${process.env.API_URL}/student/insert/abscence-permission`,
      method: 'post',
      headers: { Authorization: `Bearer ${token}` },
      data: formBody
    })
    if (res.status(200)) {
      console.log('respuest', userData)
    }
    res.status(200).json(userData.data)
  } catch (error) {
    console.log(error)
    next(error)
  }
})

// Modify abscence permit endpoints

app.post('/modify-abscence-permit', async function (req, res, next) {
  const { token } = req.cookies
  const permitDetail = req.body

  const dataToStringify = {
    id: permitDetail.id,
    dayStart: permitDetail.dayStart,
    dayEnd: permitDetail.dayEnd,
    reason: permitDetail.reason,
    comment: permitDetail.comment,
    comidaCheckIn: ['', ''],
    comidaCheckOut: ['', '']
  }

  const dataToSend = encodeURIComponent(JSON.stringify(dataToStringify))

  const formBody = 'abscence_permission=' + dataToSend
  try {
    const permitData = await axios({
      url: `${process.env.API_URL}/student/update/abscence-permission`,
      method: 'post',
      headers: { Authorization: `Bearer ${token}` },
      data: formBody
    })
    if (res.status(200)) {
      console.log('respuest', permitData.data)
    }
    res.status(200).json(permitDetail)
  } catch (error) {
    console.log(error)
    next(error)
  }
})

// Delete absecnce permit

app.post('/delete-absence-permit', async function (req, res, next) {
  console.log('estoy controlando')
  const { token } = req.cookies
  const { id } = req.body

  const permitToDelete = {
    IDsComidasCheckIn: ['', ''],
    IDsComidasCheckOut: ['', ''],
    IDAbscencePermission: id
  }

  const dataToSend = encodeURIComponent(JSON.stringify(permitToDelete))

  const formBody = 'abscence_permission=' + dataToSend

  try {
    const matchingPermit = await axios({
      url: `${process.env.API_URL}/student/update/cancel-abscence-permission`,
      method: 'post',
      headers: { Authorization: `Bearer ${token}` },
      data: formBody
    })
    if (res.status(200)) {
      console.log(matchingPermit.data)
    }
    res.status(200).json(id)
  } catch (error) {
    console.log(error)
    next(error)
  }
})

// Check extraordinary permit matching Endpoint

app.post('/checking-permit', async function (req, res, next) {
  console.log('estoy controlando')
  const { token } = req.cookies
  const { dayStart } = req.body

  const encodedDate = encodeURIComponent(dayStart)

  try {
    const matchingPermit = await axios({
      url: `${process.env.API_URL}/student/get/is-permission-in-date?date=${encodedDate}`,
      method: 'get',
      headers: { Authorization: `Bearer ${token}` }
    })
    if (res.status(200)) {
      console.log(matchingPermit.data)
    }
    res.status(200).json(matchingPermit.data)
  } catch (error) {
    console.log(error)
    next(error)
  }
})

// Delete tonight and extraordinary permit

app.post('/delete-permit', async function (req, res, next) {
  console.log('estoy controlando')
  const { token } = req.cookies
  const { id } = req.body

  try {
    const matchingPermit = await axios({
      url: `${process.env.API_URL}/student/update/cancel-permission`,
      method: 'post',
      headers: { Authorization: `Bearer ${token}` },
      data: {
        id: id
      }
    })
    if (res.status(200)) {
      console.log(matchingPermit.data)
    }
    res.status(200).json(id)
  } catch (error) {
    console.log(error)
    next(error)
  }
})

// Validate - reject permit

app.post('/validate-reject-permit', async function (req, res, next) {
  const { token } = req.cookies
  const { id, status } = req.body

  try {
    const permitData = await axios({
      url: `${process.env.API_URL}/guardian/update/student-permission`,
      method: 'post',
      headers: { Authorization: `Bearer ${token}` },
      data: {
        permission_id: id,
        status: status
      }
    })
    if (res.status(200)) {
      console.log('respuest', permitData)
    }
    res.status(200).json(id)
  } catch (error) {
    console.log(error)
    next(error)
  }
})

// Guardian change student Endpoint

app.post('/tutor-change-student', async function (req, res, next) {
  const { token } = req.cookies
  const { name } = req.body

  try {
    const permitData = await axios({
      url: `${process.env.API_URL}/guardian/get/initial-data?version=2`,
      method: 'get',
      headers: { Authorization: `Bearer ${token}` }

    })
    if (res.status(200)) {
      console.log('respuest', permitData.data)
      const initialData = permitData.data.filter(item => item.studentData.userInfo.name === name)
      res.status(200).json(initialData[0].studentData)
    }
  } catch (error) {
    console.log(error)
    next(error)
  }
})

app.get('*', main)

app.listen(PORT, (err) => {
  if (err) console.log(err)
  console.log(`Server running on ${PORT}`)
})
