import axios from 'axios'

// Actions students

export const openingPermitFormRequestRequest = payload => ({
  type: 'OPENING_PERMIT_FORM',
  payload
})

export const idFindRequest = payload => ({
  type: 'ID_FIND',
  payload
})

export const loginRequest = payload => ({
  type: 'LOGIN_REQUEST',
  payload
})

export const errorLoginRequest = payload => ({
  type: 'ERROR_LOGIN_REQUEST',
  payload
})

export const recoveryPasswordRequest = payload => ({
  type: 'RECOVERY_PASSWORD_REQUEST',
  payload
})

export const logoutRequest = payload => ({
  type: 'LOGOUT_REQUEST',
  payload
})

export const getPermitById = payload => ({
  type: 'GET_PERMIT_BY_ID',
  payload
})

export const getResidenceTonightPermitById = payload => ({
  type: 'GET_RESIDENCE_TONIGHT_PERMIT_BY_ID',
  payload
})

export const getResidencePermitById = payload => ({
  type: 'GET_RESIDENCE_PERMIT_BY_ID',
  payload
})

export const getFoodById = payload => ({
  type: 'GET_FOOD_BY_ID',
  payload
})

export const getResidenceFoodById = payload => ({
  type: 'GET_RESIDENCE_FOOD_BY_ID',
  payload
})

export const changePermitDetail = payload => ({
  type: 'CHANGE_PERMIT_DETAIL',
  payload
})

export const checkMatchingFoodPermitRequest = payload => ({
  type: 'CHECKING_MATCHING_FOOD_PERMIT',
  payload
})

export const removeCheckingPermitRequest = payload => ({
  type: 'REMOVE_CHECKING_MATCHING_PERMIT',
  payload
})

export const addFoodRequest = payload => ({
  type: 'ADD_FOOD_REQUEST',
  payload
})

export const modifyFoodRequest = payload => ({
  type: 'MODIFY_FOOD_REQUEST',
  payload
})

export const removeFoodRequest = payload => ({
  type: 'REMOVE_FOOD_REQUEST',
  payload
})

export const checkMatchingAbsencePermitRequest = payload => ({
  type: 'CHECKING_MATCHING_PERMIT',
  payload
})

export const addPermitRequest = payload => ({
  type: 'ADD_PERMIT_REQUEST',
  payload
})

export const modifyPermitRequest = payload => ({
  type: 'MODIFY_EXTRAORDINARY_REQUEST',
  payload
})

export const addPermitRequestSucceeded = payload => ({
  type: 'ADD_PERMIT_REQUEST_SUCCEEDED',
  payload
})

export const removePermitRequestSucceeded = payload => ({
  type: 'REMOVE_PERMIT_REQUEST_SUCCEEDED',
  payload
})

export const removePermitRequest = payload => ({
  type: 'REMOVE_PERMIT_REQUEST',
  payload
})

// Actions guardians

export const validatePermitRequest = payload => ({
  type: 'VALIDATE_PERMIT_REQUEST',
  payload
})

export const rejectPermitRequest = payload => ({
  type: 'REJECT_PERMIT_REQUEST',
  payload
})

export const tutorChangeStudentRequest = payload => ({
  type: 'TUTOR_CHANGE_STUDENT_REQUEST',
  payload
})

// Endpoints Student

export const loginUser = (payload, redirectUrl) => {
  return (dispatch) => {
    axios.post('/auth/sign-up', payload)
      .then(({ data }) => {
        dispatch(loginRequest(data))
      })
      .then(() => {
        window.location.href = redirectUrl
      })
      .catch(() => {
        dispatch(errorLoginRequest(true))
      })
  }
}

export const recoveryPassword = (payload) => {
  return (dispatch) => {
    axios.post('/auth/recovery-password', payload)
      .then(({ data }) => {
        dispatch(recoveryPasswordRequest(data))
      })
      .catch(error => console.log(error))
  }
}

export const logoutUser = (payload, redirectUrl) => {
  return (dispatch) => {
    axios.post('/auth/sign-out', payload)
      .then(() => {
        window.location.href = redirectUrl
      })
      .then(({ data }) => {
        dispatch(logoutRequest(data))
      })
      .catch(error => console.log(error))
  }
}

export const checkFoodMatchingPermit = (payload) => {
  return (dispatch) => {
    axios.post('/checking-food-permit', payload)
      .then(({ data }) => {
        dispatch(checkMatchingFoodPermitRequest(data))
      })
      .catch(error => console.log(error))
  }
}

export const addFoodPermit = (payload) => {
  return (dispatch) => {
    axios.post('/add-food-permit', payload)
      .then(({ data }) => {
        dispatch(addFoodRequest(data))
        dispatch(addPermitRequestSucceeded({ open: true, status: 'create', permit: data }))
      })
      .catch(error => console.log(error))
  }
}

export const modifyFoodPermit = (payload) => {
  return (dispatch) => {
    axios.post('/modify-food-permit', payload)
      .then(({ data }) => {
        dispatch(modifyFoodRequest(data))
        dispatch(addPermitRequestSucceeded({ open: true, status: 'modify', permit: data }))
      })
      .catch(error => console.log(error))
  }
}

export const removeFoodPermit = (payload, redirectUrl, type) => {
  let objectTosend = {}
  if (type === 'picnic') {
    objectTosend = { picnic: true, permitName: type }
  } else {
    objectTosend = { permitName: type }
  }
  return (dispatch) => {
    axios.post('/delete-food-permit', payload)
      .then(({ data }) => {
        dispatch(removeFoodRequest(data))
        dispatch(addPermitRequestSucceeded({ open: true, status: 'delete', permit: objectTosend }))
      })
      .then(() => {
        window.location.href = redirectUrl
      })
      .catch(error => console.log(error))
  }
}

export const checkAbsenceMatchingPermit = (payload) => {
  return (dispatch) => {
    axios.post('/checking-absence-permit', payload)
      .then(({ data }) => {
        dispatch(checkMatchingAbsencePermitRequest(data))
      })
      .catch(error => console.log(error))
  }
}

export const modifyAbscencePermit = (payload) => {
  return (dispatch) => {
    axios.post('/modify-abscence-permit', payload)
      .then(({ data }) => {
        dispatch(modifyPermitRequest(data))
        dispatch(changePermitDetail(data))
        dispatch(addPermitRequestSucceeded({ open: true, status: 'modify', permit: data }))
      })
      .catch(error => console.log(error))
  }
}

export const checkMatchingPermit = (payload) => {
  return (dispatch) => {
    axios.post('/checking-permit', payload)
      .then(({ data }) => {
        dispatch(checkMatchingAbsencePermitRequest(data))
      })
      .catch(error => console.log(error))
  }
}

export const addTonightPermit = (payload) => {
  return (dispatch) => {
    axios.post('/add-tonight-permit', payload)
      .then(({ data }) => {
        dispatch(addPermitRequest(data))
        dispatch(addPermitRequestSucceeded({ open: true, status: 'create', permit: data }))
      })
      .catch(error => console.log(error))
  }
}

export const addExtraordinaryPermit = (payload) => {
  return (dispatch) => {
    axios.post('/add-extraordinary-permit', payload)
      .then(({ data }) => {
        dispatch(addPermitRequest(data))
        dispatch(addPermitRequestSucceeded({ open: true, status: 'create', permit: data }))
      })
      .catch(error => console.log(error))
  }
}

export const modifyExtraordinaryPermit = (payload) => {
  return (dispatch) => {
    axios.post('/modify-extraordinary-permit', payload)
      .then(({ data }) => {
        dispatch(modifyPermitRequest(data))
        dispatch(changePermitDetail(data))
        dispatch(addPermitRequestSucceeded({ open: true, status: 'modify', permit: data }))
      })
      .catch(error => console.log(error))
  }
}

export const addAbscencePermit = (payload) => {
  return (dispatch) => {
    axios.post('/add-abscence-permit', payload)
      .then(({ data }) => {
        dispatch(addPermitRequest(data))
        dispatch(addPermitRequestSucceeded({ open: true, status: 'create', permit: data }))
      })
      .catch(error => console.log(error))
  }
}

export const removeMatchingAbscencePermit = (payload) => {
  return (dispatch) => {
    axios.post('/delete-absence-permit', payload)
      .then(({ data }) => {
        console.log(data)
        dispatch(removePermitRequest(data))
      })
      .catch(error => console.log(error))
  }
}

export const removeMetchingPermit = (payload) => {
  return (dispatch) => {
    axios.post('/delete-permit', payload)
      .then(({ data }) => {
        dispatch(removePermitRequest(data))
      })
      .catch(error => console.log(error))
  }
}

export const removePermit = (payload, redirectUrl, type) => {
  return (dispatch) => {
    axios.post('/delete-permit', payload)
      .then(({ data }) => {
        dispatch(removePermitRequest(data))
        dispatch(addPermitRequestSucceeded({ open: true, status: 'delete', permit: { type: type } }))
      })
      .then(() => {
        window.location.href = redirectUrl
      })
      .catch(error => console.log(error))
  }
}

export const removeAbscencePermit = (payload, redirectUrl, type) => {
  return (dispatch) => {
    axios.post('/delete-absence-permit', payload)
      .then(({ data }) => {
        dispatch(removePermitRequest(data))
        dispatch(addPermitRequestSucceeded({ open: true, status: 'delete', permit: { type: type } }))
      })
      .then(() => {
        window.location.href = redirectUrl
      })
      .catch(error => console.log(error))
  }
}

// Endpoints Guardian

export const validatePermit = (payload, redirectUrl) => {
  return (dispatch) => {
    axios.post('/validate-reject-permit', payload)
      .then(({ data }) => {
        dispatch(validatePermitRequest(data))
        dispatch(addPermitRequestSucceeded({ open: true, status: 'validated', permit: {} }))
      })
      .then(() => {
        window.location.href = redirectUrl
      })
      .catch(error => console.log(error))
  }
}

export const rejectPermit = (payload, redirectUrl) => {
  return (dispatch) => {
    axios.post('/validate-reject-permit', payload)
      .then(({ data }) => {
        dispatch(rejectPermitRequest(data))
        dispatch(addPermitRequestSucceeded({ open: true, status: 'rejected', permit: {} }))
      })
      .then(() => {
        window.location.href = redirectUrl
      })
      .catch(error => console.log(error))
  }
}

export const tutorChangeStudent = (payload, redirectUrl) => {
  return (dispatch) => {
    axios.post('tutor-change-student', payload)
      .then(({ data }) => {
        dispatch(tutorChangeStudentRequest(data))
      })
      .catch(error => console.log(error))
  }
}
