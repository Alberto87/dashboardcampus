const reducer = (state, action) => {
  switch (action.type) {
    case 'OPENING_PERMIT_FORM':
      return {
        ...state,
        permitFormIsOpening: action.payload
      }
    case 'LOGIN_REQUEST':
      return {
        ...state,
        user: action.payload
      }
    case 'ERROR_LOGIN_REQUEST':
      return {
        ...state,
        errorLogin: action.payload
      }
    case 'RECOVERY_PASSWORD_REQUEST':
      return {
        ...state,
        recoverySent: action.payload
      }
    case 'LOGOUT_REQUEST':
      return {
        ...state,
        user: action.paylod
      }
    case 'GET_PERMIT_BY_ID':
      return {
        ...state,
        idFind: true,
        permitDetail: Array.prototype.concat.apply([], state.initialData.historicalPermits.map((permits) => permits.permits.map((permit) => permit))).find(item => item.id === Number(action.payload)) || state.initialData.permits.find(item => item.id === Number(action.payload)) || {}
      }
    case 'GET_RESIDENCE_TONIGHT_PERMIT_BY_ID':
      return {
        ...state,
        idFind: true,
        permitDetail: state.initialTonightPermit.find(item => item.id === Number(action.payload))
      }
    case 'GET_RESIDENCE_PERMIT_BY_ID':
      return {
        ...state,
        idFind: true,
        permitDetail: state.initialPermit.find(item => item.id === Number(action.payload))
      }
    case 'GET_FOOD_BY_ID':
      return {
        ...state,
        idFind: true,
        permitDetail: Array.prototype.concat.apply([], state.initialData.historicalFoodPermits.map((permits) => permits.permits.map((permit) => permit))).find(item => item.id === Number(action.payload)) || state.initialData.foodPermit.find(item => item.id === Number(action.payload)) || {}
      }
    case 'GET_RESIDENCE_FOOD_BY_ID':
      return {
        ...state,
        idFind: true,
        permitDetail: state.initialFoodPermit.find(item => item.id === Number(action.payload))
      }
    case 'ID_FIND':
      return {
        ...state,
        idFind: action.payload
      }
    case 'CHANGE_PERMIT_DETAIL':
      return {
        ...state,
        permitDetail: action.payload
      }
    case 'CHECKING_MATCHING_FOOD_PERMIT':
      return {
        ...state,
        matchingFoodPermit: state.initialData.foodPermit.find(item => item.id === action.payload) || {}
      }
    case 'REMOVE_CHECKING_MATCHING_PERMIT':
      return {
        ...state,
        matchingFoodPermit: {},
        matchingPermit: []
      }
    case 'ADD_FOOD_REQUEST':
      return {
        ...state,
        initialData: {
          ...state.initialData,
          foodPermit: [...state.initialData.foodPermit, action.payload]
        }
      }
    case 'MODIFY_FOOD_REQUEST':
      return {
        ...state,
        initialData: {
          ...state.initialData,
          foodPermit: state.initialData.foodPermit.map((item) => item.id === action.payload.id ? action.payload : item)
        }
      }
    case 'CHECKING_MATCHING_PERMIT':
      return {
        ...state,
        matchingPermit: action.payload || []
      }
    case 'ADD_PERMIT_REQUEST':
      return {
        ...state,
        initialData: {
          ...state.initialData,
          permits: [...state.initialData.permits, action.payload]
        }
      }
    case 'MODIFY_EXTRAORDINARY_REQUEST':
      return {
        ...state,
        initialData: {
          ...state.initialData,
          permits: state.initialData.permits.map((item) => item.id === action.payload.id ? action.payload : item)
        }
      }
    case 'ADD_PERMIT_REQUEST_SUCCEEDED':
      return {
        ...state,
        permitRequestSucceeded: action.payload
      }
    case 'REMOVE_PERMIT_REQUEST_SUCCEEDED':
      return {
        ...state,
        permitRequestSucceeded: false
      }
    case 'REMOVE_PERMIT_REQUEST':
      return {
        ...state,
        matchingPermit: typeof matchingPermit !== 'undefined' ? state.matchingPermit.filter(items => items.id !== action.payload) : [],
        initialData: {
          ...state.initialData,
          permits: state.initialData.permits.filter(items => items.id !== action.payload),
          historicalPermits: state.initialData.historicalPermits.map((permits) => { return { month: permits.month, permits: permits.permits.filter(item => item.id !== action.payload) } })
        }
      }
    case 'REMOVE_FOOD_REQUEST':
      return {
        ...state,
        initialData: {
          ...state.initialData,
          foodPermit: state.initialData.foodPermit.filter(items => items.id !== action.payload),
          historicalFoodPermits: state.initialData.historicalFoodPermits.map((permits) => { return { month: permits.month, permits: permits.permits.filter(item => item.id !== action.payload) } })
        }
      }
    case 'VALIDATE_PERMIT_REQUEST':
      return {
        ...state,
        initialData: {
          ...state.initialData,
          permits: state.initialData.permits.map(item => { if (item.id === action.payload) { item.status = 'validated' } return item }),
          historicalPermits: state.initialData.historicalPermits.map((permits) => { return { month: permits.month, permits: permits.permits.map(item => { if (item.id === action.payload) { item.status = 'validated' } return item }) } })
        }
      }
    case 'REJECT_PERMIT_REQUEST':
      return {
        ...state,
        initialData: {
          ...state.initialData,
          permits: state.initialData.permits.map(item => { if (item.id === action.payload) { item.status = 'pending-rejected' } return item }),
          historicalPermits: state.initialData.historicalPermits.map((permits) => { return { month: permits.month, permits: permits.permits.map(item => { if (item.id === action.payload) { item.status = 'pending-rejected' } return item }) } })
        }
      }
    case 'TUTOR_CHANGE_STUDENT_REQUEST':
      return {
        ...state,
        initialData: action.payload
      }
    default:
      return state
  }
}

export default reducer
