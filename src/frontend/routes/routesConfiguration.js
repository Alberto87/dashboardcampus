export const routeConfiguration = {
  residents: { food: false, type: 'residents', breadcrumbs: ['Residentes', 'Residentes', 'Residentes'], button: 'Nueva solicitud de cena' },
  dinner: { food: true, type: 'cena', breadcrumbs: ['Comidas', 'Comidas', 'Cenas'], button: 'Nueva solicitud de cena' },
  lunch: { food: true, type: 'almuerzo', breadcrumbs: ['Comidas', 'Comidas', 'Almuerzos'], button: 'Nueva solicitud de almuerzo' },
  shortPermit: { food: false, type: 'tonight', breadcrumbs: ['Salidas nocturnas', 'Permisos', 'Salidas nocturnas'], button: 'Nueva salida nocturna' },
  extraordinary: { food: false, type: 'extraordinary', breadcrumbs: ['Permisos extraordinarios', 'Permisos', 'Permisos extraordinarios'], button: 'Nuevo permiso extraordinario' },
  long: { food: false, type: 'abscence', breadcrumbs: ['Permisos de ausencia', 'Permisos', 'Permisos de ausencia'], button: 'Nuevo permiso ausencia' }
}
