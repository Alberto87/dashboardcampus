import React from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'

import { MainLayout } from '../layouts/MainLayout'
import { Login } from '../pages/Login'
import { RecoveryPassword } from '../pages/RecoveryPassword'
import { StudentFood } from '../pages/StudentFood'
import { ResidenceFood } from '../pages/ResidenceFood'
import { ListOfResidents } from '../pages/ListOfResidents'
import { StudentPermit } from '../pages/StudentPermit'
import { ResidencePermit } from '../pages/ResidencePermit'
import { PermitSingle } from '../pages/PermitSingle'
import { FoodSingle } from '../pages/FoodSingle'
import { NotFound } from '../pages/NotFound'
import { Forbidden } from '../pages/Forbidden'
import { Home } from '../pages/Home'
import { routeConfiguration } from './routesConfiguration'

export const App = ({ isLogged }) => {
  return (
    <BrowserRouter>
      <MainLayout>
        <Switch>
          {/* Login Route */}
          <Route exact path='/login' component={(props) => <Login {...props} />} />

          {/* Recovery Password Route */}
          <Route exact path='/recuperar-contrasena' component={RecoveryPassword} />

          {/* Home Route */}
          <Route exact path='/' component={isLogged ? (props) => (<Home properties={routeConfiguration.dinner} />) : () => (<Login />)} />

          {/* Lunches Route */}
          <Route exact path='/comidas-cenas' component={(isLogged && (isLogged !== 'residence' && isLogged !== 'kitchen')) ? (props) => (<StudentFood properties={routeConfiguration.dinner} />) : ((isLogged && (isLogged === 'residence' || isLogged === 'kitchen')) ? (props) => (<ResidenceFood properties={routeConfiguration.dinner} />) : () => (<Login />))} />

          {/* Dinners Route */}
          <Route exact path='/comidas-almuerzos' component={(isLogged && isLogged !== 'residence' && isLogged !== 'kitchen') ? (props) => (<StudentFood properties={routeConfiguration.lunch} />) : ((isLogged && (isLogged === 'residence' || isLogged === 'kitchen')) ? (props) => (<ResidenceFood properties={routeConfiguration.lunch} />) : () => (<Login />))} />

          {/* Tonight Permits Route */}
          <Route exact path='/permisos-salidas-nocturnas' component={(isLogged && isLogged !== 'residence') ? (props) => (<StudentPermit properties={routeConfiguration.shortPermit} />) : ((isLogged && isLogged === 'residence') ? (props) => (<ResidencePermit properties={routeConfiguration.shortPermit} />) : () => (<Login />))} />

          {/* Extraordinary Permits Route */}
          <Route exact path='/permisos-extraordinarios' component={(isLogged && isLogged !== 'residence') ? (props) => (<StudentPermit properties={routeConfiguration.extraordinary} />) : ((isLogged && isLogged === 'residence') ? (props) => (<ResidencePermit properties={routeConfiguration.extraordinary} />) : () => (<Login />))} />

          {/* Abscence Permits Route */}
          <Route exact path='/permisos-ausencia' component={(isLogged && isLogged !== 'residence') ? (props) => (<StudentPermit properties={routeConfiguration.long} />) : ((isLogged && isLogged === 'residence') ? (props) => (<ResidencePermit properties={routeConfiguration.long} />) : () => (<Login />))} />

          <Route exact path='/permisos-salidas-nocturnas/:id' component={isLogged ? (props) => (<PermitSingle {...props} type='short' />) : () => (<Login />)} />
          <Route exact path='/permisos-ausencia/:id' component={isLogged ? (props) => <PermitSingle {...props} /> : () => <Login />} />
          <Route exact path='/permisos-extraordinarios/:id' component={isLogged ? (props) => <PermitSingle {...props} /> : () => <Login />} />
          <Route exact path='/comidas-almuerzos/:id' component={isLogged ? (props) => <FoodSingle {...props} /> : () => <Login />} />
          <Route exact path='/comidas-cenas/:id' component={isLogged ? (props) => <FoodSingle {...props} /> : () => <Login />} />
          <Route exact path='/residentes' component={(isLogged && isLogged === 'residence') ? (props) => (<ListOfResidents properties={routeConfiguration.residents} />) : ((isLogged && isLogged !== 'residence') ? () => <Forbidden /> : () => <Login />)} />
          <Route component={NotFound} />
        </Switch>
      </MainLayout>
    </BrowserRouter>
  )
}
