import React from 'react'
import { Login } from '../pages/Login'
import { RecoveryPassword } from '../pages/RecoveryPassword'
import { StudentFood } from '../pages/StudentFood'
import { ResidenceFood } from '../pages/ResidenceFood'
import { ListOfResidents } from '../pages/ListOfResidents'
import { StudentPermit } from '../pages/StudentPermit'
import { ResidencePermit } from '../pages/ResidencePermit'
import { PermitSingle } from '../pages/PermitSingle'
import { FoodSingle } from '../pages/FoodSingle'
import { NotFound } from '../pages/NotFound'
import { Forbidden } from '../pages/Forbidden'
import { Home } from '../pages/Home'
import { routeConfiguration } from './routesConfiguration'

const serverRoutes = (isLogged) => {
  return [
    {
      path: '/login',
      component: Login,
      exact: true
    },
    {
      path: '/recuperar-contrasena',
      component: RecoveryPassword,
      exact: true
    },
    {
      path: '/',
      component: isLogged ? (props) => (<Home properties={routeConfiguration.dinner} />) : () => (<Login />),
      exact: true
    },
    {
      path: '/comidas-cenas',
      component: (isLogged && (isLogged !== 'residence' && isLogged !== 'kitchen')) ? (props) => (<StudentFood properties={routeConfiguration.dinner} />) : ((isLogged && (isLogged === 'kitchen')) ? (props) => (<ResidenceFood properties={routeConfiguration.dinner} />) : () => (<Login />)),
      exact: true
    },
    {
      path: '/comidas-almuerzos',
      component: (isLogged && isLogged !== 'residence' && isLogged !== 'kitchen') ? (props) => (<StudentFood properties={routeConfiguration.lunch} />) : ((isLogged && (isLogged === 'residence' || isLogged === 'kitchen')) ? (props) => (<ResidenceFood properties={routeConfiguration.lunch} />) : () => (<Login />)),
      exact: true
    },
    {
      path: '/permisos-salidas-nocturnas',
      component: (isLogged && isLogged !== 'residence') ? (props) => (<StudentPermit properties={routeConfiguration.shortPermit} />) : ((isLogged && isLogged === 'residence') ? (props) => (<ResidencePermit properties={routeConfiguration.shortPermit} />) : () => (<Login />)),
      exact: true
    },
    {
      path: '/permisos-extraordinarios',
      component: (isLogged && isLogged !== 'residence') ? (props) => (<StudentPermit properties={routeConfiguration.extraordinary} />) : ((isLogged && isLogged === 'residence') ? (props) => (<ResidencePermit properties={routeConfiguration.extraordinary} />) : () => (<Login />)),
      exact: true
    },
    {
      path: '/permisos-ausencia',
      component: (isLogged && isLogged !== 'residence') ? (props) => (<StudentPermit properties={routeConfiguration.long} />) : ((isLogged && isLogged === 'residence') ? (props) => (<ResidencePermit properties={routeConfiguration.long} />) : () => (<Login />)),
      exact: true
    },
    {
      path: '/permisos-salidas-nocturnas/:id',
      component: isLogged ? (props) => (<PermitSingle {...props} type='short' />) : () => (<Login />),
      exact: true
    },
    {
      path: '/permisos-ausencia/:id',
      component: isLogged ? (props) => <PermitSingle {...props} /> : () => <Login />,
      exact: true
    },
    {
      path: '/permisos-extraordinarios/:id',
      component: isLogged ? (props) => <PermitSingle {...props} /> : () => <Login />,
      exact: true
    },
    {
      path: '/comidas-almuerzos/:id',
      component: isLogged ? (props) => <FoodSingle {...props} /> : () => <Login />,
      exact: true
    },
    {
      path: '/comidas-cenas/:id',
      component: isLogged ? (props) => <FoodSingle {...props} /> : () => <Login />,
      exact: true
    },
    {
      path: 'residentes',
      component: (isLogged && isLogged === 'residence') ? (props) => (<ListOfResidents properties={routeConfiguration.residents} />) : ((isLogged && isLogged !== 'residence') ? () => <Forbidden /> : () => <Login />),
      exact: true
    },
    {
      name: 'NotFound',
      component: NotFound
    }
  ]
}

export default serverRoutes
