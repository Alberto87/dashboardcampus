export const checkIfArrayHasObject = (arr, obj) => {
  let found = false
  for (let i = 0; i < arr.length; i++) {
    if (arr[i].id === obj.id) {
      found = true
      break
    }
  }
  return found
}

export const compareToObject = (obj1, obj2) => {
  let found = false
  for (var key in obj1) {
    if (obj1[key] !== obj2[key]) {
      found = true
      break
    }
  }
  return found
}
