import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { HeaderResidence } from '../components/headers/HeaderResidence'
import { HeaderWidthCalendarSelection } from '../components/HeaderWithCalendarSelection'
import { ResidenceMainContentNavBar } from '../components/ResidenceMainConentNavBar'
import { MainContentLayout } from '../layouts/MainContentLayout'
import { ShortPermitLayout } from '../layouts/ShortPermitLayout'
import { ExtraordinaryPermitLayout } from '../layouts/ExtraordinaryPermitLayout'
import { NoteModal } from '../components/Modals/NoteModal'
import { LongPermitLayout } from '../layouts/LongPermitLayout'

export const ResidencePermitContainer = ({
  properties,
  referenceTonightDate,
  referenceExtraordinaryDate,
  initialTonightPermit = [],
  initialPermit = [],
  residenceProfiles
}) => {
  var moment = require('moment')
  require('moment/locale/es')

  const [isPast, setIsPast] = useState(false)
  const [pastPermit, setPastPermits] = useState([])
  const [futurePermit, setFuturePermits] = useState([])
  const [permistArray, setPermitsArray] = useState([])
  const [openNote, setOpenNote] = useState(false)
  const [note, setNote] = useState('')

  useEffect(() => {
    const referenceDate = (properties.type === 'tonight') ? referenceTonightDate : referenceExtraordinaryDate
    const profile = residenceProfiles.filter(item => item.name === properties.type)
    const limitHour = profile[0].limit_hour
    const now = moment().format('YYYY-MM-DD HH:mm:ss')
    const permitDate = moment(referenceDate).format('YYYY-MM-DD' + ' ' + limitHour)
    const a = moment(now)
    const b = moment(permitDate)
    const diff = b.diff(a, 'minutes', true)
    setIsPast(diff < 0)
  }, [referenceTonightDate])

  useEffect(() => {
    if (properties.type === 'tonight') {
      const pastPermitsArray = (isPast) ? initialTonightPermit : []
      const futurePermitsArray = (isPast) ? [] : initialTonightPermit
      setPastPermits(pastPermitsArray)
      setFuturePermits(futurePermitsArray)
    }
  }, [initialTonightPermit])

  useEffect(() => {
    const array = (properties.type === 'extraordinary') ? initialPermit.filter(item => item.type === 'extraordinary') : initialPermit.filter(item => item.type === 'abscence')
    setPermitsArray(array)
  }, [initialPermit])

  const handleNoteModal = (text) => {
    if (openNote) {
      setOpenNote(false)
      setNote('')
    } else {
      setOpenNote(true)
      setNote(text)
    }
  }

  const tableConfigurationShort = {
    short: ['Residente', 'Día salida', 'Fecha solicitud'],
    extraordinary: ['Residente', 'Estado', 'Día salida', 'Motivo', 'Nota', 'Fecha solicitud', 'Fecha validación'],
    abscence: ['Residente', 'Estado', 'Día salida', 'Día de vuelta', 'Motivo', 'Nota', 'Fecha solicitud', 'Fecha validación']
  }

  return (
    <>
      <HeaderResidence type={properties.type} />
      <div className='main-content'>
        <ResidenceMainContentNavBar />
        <HeaderWidthCalendarSelection properties={properties} />
        <MainContentLayout>
          {
            properties.type === 'tonight' && (
              <ShortPermitLayout
                tableConfiguration={tableConfigurationShort.short}
                types={properties.type}
                future
                past={false}
                pastPermits={pastPermit}
                futurePermits={futurePermit}
              />
            )
          }
          {
            properties.type === 'extraordinary' && (
              <ExtraordinaryPermitLayout
                tableConfiguration={tableConfigurationShort.extraordinary}
                types={properties.type}
                pastPermits={[]}
                futurePermits={permistArray}
                handleNoteModal={handleNoteModal}
                showIcon
              />
            )
          }
          {
            properties.type === 'abscence' && (
              <LongPermitLayout
                tableConfiguration={tableConfigurationShort.abscence}
                types={properties.type}
                pastPermits={[]}
                futurePermits={permistArray}
                handleNoteModal={handleNoteModal}
                showIcon
              />
            )
          }
        </MainContentLayout>
      </div>
      {
        openNote && (
          <NoteModal
            openNote={openNote}
            handleNoteModal={handleNoteModal}
            note={note}
          />
        )
      }
    </>
  )
}

const mapStateToProps = state => {
  return {
    referenceTonightDate: state.referenceTonightDate,
    referenceExtraordinaryDate: state.referenceExtraordinaryDate,
    initialTonightPermit: state.initialTonightPermit,
    initialPermit: state.initialPermit,
    residenceProfiles: state.initialData.infoResidence.residenceProfiles
  }
}

const ConnectedComponent = connect(mapStateToProps, null)(ResidencePermitContainer)
export { ConnectedComponent as ResidencePermit }
