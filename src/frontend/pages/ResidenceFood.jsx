import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { HeaderResidence } from '../components/headers/HeaderResidence'
import { ResidenceMainContentNavBar } from '../components/ResidenceMainConentNavBar'
import { HeaderWidthCalendarSelection } from '../components/HeaderWithCalendarSelection'
import { MainContentLayout } from '../layouts/MainContentLayout'
import { FoodPermitLayout } from '../layouts/FoodPermitLayout'
import { NoteModal } from '../components/Modals/NoteModal'
import { HeaderKitchen } from '../components/headers/HeaderKitchen'
import { HeaderWidthCalendarSelectionKitchen } from '../components/HeaderWithCalendarSelectionKitchen'

export const ResidenceFoodContainer = ({ properties, user, initialFoodPermit = [] }) => {
  const [openNote, setOpenNote] = useState(false)
  const [note, setNote] = useState('')
  const [picnicList, setPicnicList] = useState([])
  const [noPicnicList, setNoPicnicList] = useState([])

  useEffect(() => {
    const typeOfFoodArray = initialFoodPermit.filter(item => item.permitName === properties.type)
    setPicnicList(typeOfFoodArray.filter(item => item.picnic === true))
    setNoPicnicList(typeOfFoodArray.filter(item => item.picnic !== true))
  }, [initialFoodPermit])

  const handleNoteModal = (text) => {
    if (openNote) {
      setOpenNote(false)
      setNote('')
    } else {
      setOpenNote(true)
      setNote(text)
    }
  }
  console.log(properties.type)
  const tableConfiguration = {
    short: ['Residente', 'Tipo de solicitud', 'Día comida', 'Nota', 'Fecha solicitud']
  }
  const titles = ['Picnics solicitados', 'Cenas editadas']
  return (
    <>
      {
        user.role === 'residence' && (
          <HeaderResidence type={properties.type} />
        )
      }
      {
        user.role === 'kitchen' && (
          <HeaderKitchen type={properties.type} />
        )
      }
      <div className='main-content'>
        <ResidenceMainContentNavBar />
        {
          user.role === 'residence' && (
            <HeaderWidthCalendarSelection properties={properties} />
          )
        }
        {
          user.role === 'kitchen' && (
            <HeaderWidthCalendarSelectionKitchen properties={properties} />
          )
        }

        <MainContentLayout>
          <FoodPermitLayout
            tableConfiguration={tableConfiguration}
            pastPermitsList={noPicnicList}
            futurePermitsList={picnicList}
            handleNoteModal={handleNoteModal}
            titles={titles}
            showIcon={false}
          />
        </MainContentLayout>
      </div>
      {
        openNote && (
          <NoteModal
            openNote={openNote}
            handleNoteModal={handleNoteModal}
            note={note}
          />
        )
      }
    </>
  )
}

const mapStateToProps = state => {
  return {
    user: state.user,
    initialFoodPermit: state.initialFoodPermit
  }
}

const ConnectedComponent = connect(mapStateToProps, null)(ResidenceFoodContainer)
export { ConnectedComponent as ResidenceFood }
