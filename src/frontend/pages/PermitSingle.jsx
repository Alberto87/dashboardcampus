import React, { useState, useEffect } from 'react'
import { Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import {
  getPermitById,
  modifyExtraordinaryPermit,
  modifyAbscencePermit,
  removePermit,
  removeAbscencePermit,
  validatePermit,
  rejectPermit,
  getResidenceTonightPermitById,
  getResidencePermitById
} from '../actions'

import { HeaderStudent } from '../components/headers/HeaderStudent'
import { MainContentNavBar } from '../components/MainContentNavBar'
import { PermitSingleLayout } from '../layouts/PermitSingleLayout'
import { PermitSingleNav } from '../components/PermitSingleNav'
import { PermitSingleBody } from '../components/PermitSingleBody'
import { PermitSingleFooter } from '../components/PermitSingleFooter'
import { EditingBar } from '../components/EditingBar'
import { PermitRequestSucceeded } from '../components/PermitRequestSucceeded'
import { ValidatingPermitBar } from '../components/ValidatingPermitBar'
import { HeaderGuardian } from '../components/headers/HeaderGuardian'
import { HeaderResidence } from '../components/headers/HeaderResidence'
import { ResidenceMainContentNavBar } from '../components/ResidenceMainConentNavBar'

const PermitSingleContainer = (
  {
    type,
    user,
    removeAbscencePermit,
    removePermit,
    match,
    history,
    permitDetail = {},
    getPermitById,
    residenceProfiles,
    modifyExtraordinaryPermit,
    modifyAbscencePermit,
    permitRequestSucceeded = false,
    validatePermit,
    rejectPermit,
    getResidenceTonightPermitById,
    getResidencePermitById
  }) => {
  const { id } = match.params
  const hasPermit = true
  const [editing, setEditing] = useState(false)
  const [editable, setEditable] = useState(false)
  const [formToEdit, setFormToEdit] = useState({})
  const [startEditing, setStartEditing] = useState(false)
  const [permit, setPermit] = useState({})
  const [isSaving, setIsSaving] = useState(false)
  const [wantDeleting, setWantDeleting] = useState(false)
  const [isDeleting, setIsDeleting] = useState(false)

  useEffect(() => {
    if (!startEditing) {
      if (user.role === 'residence') {
        if (type === 'short') {
          getResidenceTonightPermitById(id)
        } else {
          getResidencePermitById(id)
        }
      } else {
        getPermitById(id)
      }
    }
    setPermit(permitDetail)
  }, [id, permitDetail, JSON.stringify(permitDetail)])

  useEffect(() => {
    if (permitRequestSucceeded) {
      setEditing(false)
      setIsSaving(false)
      setIsDeleting(false)
    }
  }, [permitRequestSucceeded])

  const handleEditing = () => {
    setEditing(!editing)
  }

  const reciveForm = (form) => {
    setFormToEdit(form)
    setStartEditing(true)
  }

  const disableEditing = (val) => {
    setEditable(val)
  }

  const modifyPermit = () => {
    setPermit(formToEdit)
    sentRequest()
  }

  const sentRequest = () => {
    if (permitDetail.type === 'extraordinary') {
      modifyExtraordinaryPermit(formToEdit)
    } else {
      modifyAbscencePermit(formToEdit)
    }
    setIsSaving(true)
  }

  const deletePermit = () => {
    setWantDeleting(!wantDeleting)
    setEditing(!editing)
  }

  const deletePermitRequest = () => {
    const id = permitDetail.id
    if (permitDetail.type === 'tonight') {
      removePermit({ id }, '/permisos-salidas-nocturnas', permitDetail.type)
    } else if (permitDetail.type === 'extraordinary') {
      removePermit({ id }, '/permisos-extraordinarios', permitDetail.type)
    } else {
      removeAbscencePermit({ id }, '/permisos-ausencia', permitDetail.type)
    }
    setIsDeleting(true)
  }

  const handleRoute = () => {
    const backRoute = (permitDetail.type === 'tonight') ? '/permisos-salidas-nocturnas' : ((permitDetail.type === 'abscence') ? '/permisos-ausencia' : '/permisos-extraordinarios')
    history.push(backRoute)
  }

  const validatePermitRequest = () => {
    const routeRedirection = (permitDetail.type === 'extraordinary') ? '/permisos-extraordinarios' : '/permisos-ausencia'
    validatePermit({ id: id, status: 'validated' }, routeRedirection)
  }

  const rejectPermitRequest = () => {
    const routeRedirection = (permitDetail.type === 'extraordinary') ? '/permisos-extraordinarios' : '/permisos-ausencia'
    rejectPermit({ id: id, status: 'pending-rejected' }, routeRedirection)
  }

  console.log(user.role)
  return hasPermit ? (
    <>
      {
        user.role === 'student' && (
          <HeaderStudent type={permitDetail.type} />
        )
      }
      {
        user.role === 'guardian' && (
          <HeaderGuardian type={permitDetail.type} />
        )
      }
      {
        user.role === 'residence' && (
          <HeaderResidence type={permitDetail.type} />
        )
      }
      {
        editing && (
          <EditingBar
            handleEditing={handleEditing}
            editable={editable}
            modifyPermit={modifyPermit}
            isSaving={isSaving}
            wantDeleting={wantDeleting}
            deletePermitRequest={deletePermitRequest}
            deletePermit={deletePermit}
            isDeleting={isDeleting}
          />
        )
      }
      {
        permitDetail.status === 'pending' && (
          <ValidatingPermitBar validatePermitRequest={validatePermitRequest} rejectPermitRequest={rejectPermitRequest} />
        )
      }
      <div className='main-content'>
        {
          (() => {
            if (user.role === 'student' || user.role === 'guardian') {
              return (
                <MainContentNavBar />
              )
            } else {
              return (
                <ResidenceMainContentNavBar />
              )
            }
          })()
        }
        <PermitRequestSucceeded />
        <PermitSingleLayout>
          <PermitSingleNav
            type={permitDetail.type}
            status={permitDetail.status}
            residenceProfiles={residenceProfiles}
            handleRoute={handleRoute}
            handleEditing={handleEditing}
            editing={editing}
            deletePermit={deletePermit}
            isDeleting={isDeleting}
          />
          <PermitSingleBody
            permitDetail={permitDetail}
            editing={editing}
            disableEditing={disableEditing}
            reciveForm={reciveForm}
            wantDeleting={wantDeleting}
          />
          <PermitSingleFooter type={permitDetail.type} />
        </PermitSingleLayout>
      </div>
    </>
  ) : <Redirect to='/' />
}

const mapDispatchToProps = {
  getPermitById,
  modifyExtraordinaryPermit,
  modifyAbscencePermit,
  removePermit,
  removeAbscencePermit,
  validatePermit,
  rejectPermit,
  getResidenceTonightPermitById,
  getResidencePermitById
}

const mapStateToProps = state => {
  return {
    user: state.user,
    permitDetail: state.permitDetail,
    residenceProfiles: state.initialData.residenceProfiles,
    permitRequestSucceeded: state.permitRequestSucceeded
  }
}

const ConnectedComponent = connect(mapStateToProps, mapDispatchToProps)(PermitSingleContainer)
export { ConnectedComponent as PermitSingle }
