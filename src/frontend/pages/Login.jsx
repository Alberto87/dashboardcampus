import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { loginUser } from '../actions'
import { Redirect } from 'react-router-dom'

import { LoginLayout } from '../layouts/LoginLayout'
require('@babel/polyfill')

export const LoginContainer = ({ user = {}, loginUser, errorLogin = false }) => {
  const [noEmail, setNoEMail] = useState(false)
  const [noPassword, setNoPassword] = useState(false)
  const [error, setError] = useState(false)
  const [form, setForm] = useState({
    email: '',
    password: '',
    device_token: 'jljlkjl'
  })

  useEffect(() => {
    setError(errorLogin)
  }, [errorLogin])

  const handleInput = (event) => {
    setForm({
      ...form,
      [event.target.name]: event.target.value
    })
  }

  const handleSubmit = async (event) => {
    event.preventDefault()
    if (form.email === '' || form.password === '') {
      setNoEMail(form.email === '')
      setNoPassword(form.password === '')
      return
    } else {
      setNoEMail(form.email === '')
      setNoPassword(form.password === '')
    }
    loginUser(form, '/')
  }
  const classEmail = noEmail ? 'form-control border-red' : 'form-control'
  const classPassword = noPassword ? 'form-control border-red' : 'form-control'
  const hasUser = Object.keys(user).length > 0
  return !hasUser ? (
    <LoginLayout>
      <div className='text-center text-muted mb-4'>
        <small>Iniciar sesión</small>
      </div>
      <form action='' onSubmit={handleSubmit}>
        <div className='form-group mb-3'>
          <div className='input-group input-group-merge input-group-alternative'>
            <div className='input-group-prepend'>
              <span className='input-group-text'>
                <i className='fas fa-envelope' />
              </span>
            </div>
            <input
              type='text'
              className={classEmail}
              placeholder='Email'
              name='email'
              onChange={handleInput}
            />
          </div>
        </div>
        <div className='form-group'>
          <div className='input-group input-group-merge input-group-alternative'>
            <div className='input-group-prepend'>
              <span className='input-group-text'>
                <i className='fas fa-unlock-alt' />
              </span>
            </div>
            <input
              type='password'
              className={classPassword}
              placeholder='Contrasña'
              name='password'
              onChange={handleInput}
            />
          </div>
        </div>
        {
          error && (
            <p>Usuario o contraseña erroneo</p>
          )
        }
        <div className='text-center'>
          <button className='btn btn-yellow my-4'>Iniciar sesión</button>
        </div>
      </form>
    </LoginLayout>
  ) : <Redirect to='/' />
}

const mapDispatchToProps = {
  loginUser
}

const mapStateToProps = state => {
  return {
    user: state.user,
    errorLogin: state.errorLogin
  }
}

const ConnectedComponent = connect(mapStateToProps, mapDispatchToProps)(LoginContainer)
export { ConnectedComponent as Login }
