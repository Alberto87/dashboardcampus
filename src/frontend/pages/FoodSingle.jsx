import React, { useState, useEffect } from 'react'
import { Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import { getFoodById, modifyFoodPermit, changePermitDetail, removeFoodPermit, getResidenceFoodById } from '../actions'

import { HeaderStudent } from '../components/headers/HeaderStudent'
import { MainContentNavBar } from '../components/MainContentNavBar'
import { PermitSingleLayout } from '../layouts/PermitSingleLayout'
import { PermitSingleFooter } from '../components/PermitSingleFooter'
import { FoodSingleNav } from '../components/FoodSingleNav'
import { FoodSingleBody } from '../components/FoodSingleBody'
import { EditingBar } from '../components/EditingBar'
import { PermitRequestSucceeded } from '../components/PermitRequestSucceeded'
import { HeaderGuardian } from '../components/headers/HeaderGuardian'
import { HeaderResidence } from '../components/headers/HeaderResidence'
import { ResidenceMainContentNavBar } from '../components/ResidenceMainConentNavBar'

const FoodSingleContainer = ({
  user,
  removeFoodPermit,
  match,
  history,
  permitDetail = {},
  getFoodById,
  residenceProfiles = [],
  modifyFoodPermit,
  changePermitDetail,
  getResidenceFoodById,
  permitRequestSucceeded = false
}) => {
  const { id } = match.params
  const hasPermit = true
  const [editing, setEditing] = useState(false)
  const [editable, setEditable] = useState(false)
  const [formToEdit, setFormToEdit] = useState({})
  const [startEditing, setStartEditing] = useState(false)
  const [permit, setPermit] = useState({})
  const [isSaving, setIsSaving] = useState(false)
  const [wantDeleting, setWantDeleting] = useState(false)
  const [isDeleting, setIsDeleting] = useState(false)

  useEffect(() => {
    console.log(user)
    if (!startEditing) {
      if (user.role === 'residence') {
        getResidenceFoodById(id)
      } else {
        getFoodById(id)
      }
    }
    setPermit(permitDetail)
  }, [id, permitDetail, JSON.stringify(permitDetail)])

  useEffect(() => {
    if (permitRequestSucceeded) {
      setEditing(false)
      setIsSaving(false)
      setIsDeleting(false)
    }
  }, [permitRequestSucceeded])

  const handleRoute = () => {
    const backRoute = (permitDetail.permitName === 'almuerzo') ? '/comidas-almuerzos' : '/comidas-cenas'
    history.push(backRoute)
  }

  const handleEditing = () => {
    setEditing(!editing)
  }

  const disableEditing = (val) => {
    setEditable(val)
  }

  const reciveForm = (form) => {
    setFormToEdit(form)
    setStartEditing(true)
  }

  const modifyPermit = () => {
    setPermit(formToEdit)
    sentRequest()
  }

  const sentRequest = () => {
    modifyFoodPermit(formToEdit)
    changePermitDetail(formToEdit)
    setIsSaving(true)
  }

  const deletePermit = () => {
    setWantDeleting(!wantDeleting)
    setEditing(!editing)
  }

  const deletePermitRequest = () => {
    const id = permitDetail.id
    if (permitDetail.picnic && permitDetail.permitName === 'alumerzo') {
      removeFoodPermit({ id }, '/comidas-almuerzos', 'picnic')
    } else if (permitDetail.picnic && permitDetail.permitName === 'cena') {
      removeFoodPermit({ id }, '/comidas-cenas', 'picnic')
    } else if (permitDetail.permitName === 'cena') {
      removeFoodPermit({ id }, '/comidas-cenas', permitDetail.permitName)
    } else {
      removeFoodPermit({ id }, '/comidas-almuerzos', permitDetail.permitName)
    }
    setIsDeleting(true)
  }
  return hasPermit ? (
    <>
      {
        user.role === 'student' && (
          <HeaderStudent type={permitDetail.permitName} />
        )
      }
      {
        user.role === 'guardian' && (
          <HeaderGuardian type={permitDetail.permitName} />
        )
      }
      {
        user.role === 'residence' && (
          <HeaderResidence type={permitDetail.type} />
        )
      }
      {
        editing && (
          <EditingBar
            handleEditing={handleEditing}
            editable={editable}
            modifyPermit={modifyPermit}
            isSaving={isSaving}
            wantDeleting={wantDeleting}
            deletePermitRequest={deletePermitRequest}
            deletePermit={deletePermit}
            isDeleting={isDeleting}
          />
        )
      }
      <div className='main-content'>

        {
          (() => {
            if (user.role === 'student' || user.role === 'guardian') {
              return (
                <MainContentNavBar />
              )
            } else {
              return (
                <ResidenceMainContentNavBar />
              )
            }
          })()
        }
        <PermitRequestSucceeded />
        <PermitSingleLayout>
          <FoodSingleNav
            type={permitDetail.permitName}
            item={permitDetail}
            residenceProfiles={residenceProfiles}
            handleRoute={handleRoute}
            handleEditing={handleEditing}
            editing={editing}
            deletePermit={deletePermit}
          />
          <FoodSingleBody
            permitDetail={permitDetail}
            editing={editing}
            disableEditing={disableEditing}
            reciveForm={reciveForm}
            wantDeleting={wantDeleting}
          />
          <PermitSingleFooter type={permitDetail.type} />
        </PermitSingleLayout>
      </div>
    </>
  ) : <Redirect to='/' />
}

const mapDispatchToProps = {
  getFoodById,
  modifyFoodPermit,
  changePermitDetail,
  removeFoodPermit,
  getResidenceFoodById
}

const mapStateToProps = state => {
  return {
    user: state.user,
    idFind: state.idFind,
    permitDetail: state.permitDetail,
    residenceProfiles: state.initialData.residenceProfiles,
    permitRequestSucceeded: state.permitRequestSucceeded
  }
}

const ConnectedComponent = connect(mapStateToProps, mapDispatchToProps)(FoodSingleContainer)
export { ConnectedComponent as FoodSingle }
