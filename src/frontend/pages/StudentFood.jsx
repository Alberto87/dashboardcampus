import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { openingPermitFormRequestRequest, removeCheckingPermitRequest } from '../actions'

import { HeaderStudent } from '../components/headers/HeaderStudent'
import { MainContentNavBar } from '../components/MainContentNavBar'
import { MainContentHeader } from '../components/MainContentHeader'
import { MainContentLayout } from '../layouts/MainContentLayout'
import { NewFood } from '../components/NewPermit/NewFood'
import { NewPermit } from '../components/NewPermit/NewPermit'
import { FoodPermitLayout } from '../layouts/FoodPermitLayout'
import { NoteModal } from '../components/Modals/NoteModal'
import { checkIfArrayHasObject } from '../util/function'
import { HeaderGuardian } from '../components/headers/HeaderGuardian'

const StudentFoodContainer = ({
  user,
  properties,
  pastFoodPermitList,
  futureFoodPermitList,
  permitRequestSucceeded = false,
  openingPermitFormRequestRequest,
  removeCheckingPermitRequest
}) => {
  const [openedModal, setOpenedModal] = useState(false)
  const [openNote, setOpenNote] = useState(false)
  const [note, setNote] = useState('')
  const [type, setType] = useState('')
  const [openedPermitModal, setOpenedPermitModal] = useState(false)

  const [pastPermitsList, setPastPermitList] = useState([])
  const [futurePermitsList, setFuturePermitList] = useState([])

  useEffect(() => {
    if (openedModal) {
      if (permitRequestSucceeded) {
        if (permitRequestSucceeded.open) {
          setOpenedModal(false)
        }
      }
    }
  }, [permitRequestSucceeded])

  useEffect(() => {
    setType(properties.type)
  }, [properties])

  useEffect(() => {
    const pastPermits = []
    const futurePermits = []

    pastFoodPermitList.map((permit) => {
      permit.permits.map((item) => {
        if (properties.type === item.permitName) {
          const found = checkIfArrayHasObject(futureFoodPermitList, item)

          if (!found) {
            pastPermits.push(item)
          }
        }
      })
    })

    setPastPermitList(pastPermits)
    futureFoodPermitList.map((permit) => {
      if (properties.type === permit.permitName || properties.type === permit.mealName) {
        futurePermits.push(permit)
      }
    })
    setFuturePermitList(futurePermits)
  }, [pastFoodPermitList, futureFoodPermitList])

  const handleNewPermit = () => {
    openedModal ? openingPermitFormRequestRequest(false) : openingPermitFormRequestRequest(true)
    if (type === 'tonight' || type === 'extraordinary' || type === 'abscence') {
      if (openedPermitModal) {
        setOpenedPermitModal(false)
        removeCheckingPermitRequest()
        setType(properties.type)
        openingPermitFormRequestRequest(false)
      } else {
        setOpenedPermitModal(true)
        openingPermitFormRequestRequest(true)
      }
    } else {
      if (openedModal) {
        setOpenedModal(false)
        setType(properties.type)
        removeCheckingPermitRequest()
        openingPermitFormRequestRequest(false)
      } else {
        setOpenedModal(true)
        openingPermitFormRequestRequest(true)
      }
    }
  }

  const handleNoteModal = (text) => {
    if (openNote) {
      setOpenNote(false)
      setNote('')
    } else {
      setOpenNote(true)
      setNote(text)
    }
  }

  const alternativeOpeningModal = (typeRecived) => {
    setType(typeRecived)
    openingPermitFormRequestRequest(true)
    if (typeRecived !== 'tonight' && typeRecived !== 'extraordinary' && typeRecived !== 'abscence') {
      openedModal ? setOpenedModal(false) : setOpenedModal(true)
    } else {
      openedPermitModal ? setOpenedPermitModal(false) : setOpenedPermitModal(true)
    }
  }

  const tableConfiguration = {
    short: ['Tipo de solicitud', 'Día comida', 'Nota', 'Fecha solicitud']
  }

  const titles = ['Pendientes de consumir', 'Consumidas']

  return (
    <>
      {
        user.role === 'student' && (
          <HeaderStudent type={properties.type} />
        )
      }
      {
        user.role === 'guardian' && (
          <HeaderGuardian type={properties.type} />
        )
      }
      <div className='main-content'>
        <MainContentNavBar alternativeOpeningModal={alternativeOpeningModal} />
        <MainContentHeader properties={properties} handleNewPermit={handleNewPermit} />
        <MainContentLayout>
          <FoodPermitLayout
            tableConfiguration={tableConfiguration}
            pastPermitsList={pastPermitsList}
            futurePermitsList={futurePermitsList}
            handleNoteModal={handleNoteModal}
            titles={titles}
            showIcon
          />
        </MainContentLayout>
      </div>
      {
        openedModal && (
          <NewFood
            openedModal={openedModal}
            handleNewPermit={handleNewPermit}
            typeOfPermit={type}
          />
        )
      }
      {
        openedPermitModal && (
          <NewPermit
            type={type}
            openedModal={openedPermitModal}
            handleNewPermit={handleNewPermit}
          />
        )
      }
      {
        openNote && (
          <NoteModal
            openNote={openNote}
            handleNoteModal={handleNoteModal}
            note={note}
          />
        )
      }
    </>
  )
}

const mapDispatchToProps = {
  openingPermitFormRequestRequest,
  removeCheckingPermitRequest
}

const mapStateToProps = state => {
  return {
    user: state.user,
    pastFoodPermitList: state.initialData.historicalFoodPermits,
    futureFoodPermitList: state.initialData.foodPermit,
    permitRequestSucceeded: state.permitRequestSucceeded
  }
}

const ConnectedComponent = connect(mapStateToProps, mapDispatchToProps)(StudentFoodContainer)
export { ConnectedComponent as StudentFood }
