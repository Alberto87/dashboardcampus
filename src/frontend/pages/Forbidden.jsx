import React from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'

import { HeaderStudent } from '../components/headers/HeaderStudent'
import { HeaderGuardian } from '../components/headers/HeaderGuardian'
import { HeaderResidence } from '../components/headers/HeaderResidence'
import { HeaderKitchen } from '../components/headers/HeaderKitchen'

const ForbiddenContainer = ({ user = {} }) => {
  const routeConfiguration = {
    dinner: { food: true, type: 'cena', breadcrumbs: ['Comidas', 'Comidas', 'Cenas'], button: 'Nueva solicitud de cena' },
    lunch: { food: true, type: 'almuerzo', breadcrumbs: ['Comidas', 'Comidas', 'Almuerzos'], button: 'Nueva solicitud de almuerzo' },
    shortPermit: { food: false, type: 'tonight', breadcrumbs: ['Salidas nocturnas', 'Permisos', 'Salidas nocturnas'], button: 'Nueva salida nocturna' },
    extraordinary: { food: false, type: 'extraordinary', breadcrumbs: ['Permisos extraordinarios', 'Permisos', 'Permisos extraordinarios'], button: 'Nuevo permiso extraordinario' },
    long: { food: false, type: 'abscence', breadcrumbs: ['Permisos de ausencia', 'Permisos', 'Permisos de ausencia'], button: 'Nuevo permiso ausencia' }
  }

  return (
    <>
      {
        user.role === 'student' && (
          <HeaderStudent type='home' />
        )
      }
      {
        user.role === 'guardian' && (
          <HeaderGuardian type='home' />
        )
      }
      {
        user.role === 'residence' && (
          <HeaderResidence type='home' />
        )
      }
      {
        user.role === 'guardian' && (
          <HeaderKitchen type='home' />
        )
      }
      <div className='main-content'>
        <h1>Prohibido</h1>
        <p>Aquí tu no puedes pasar</p>
        <Link to='/'>Vuelve a tu casa</Link>
      </div>
    </>
  )
}

const mapStateToProps = state => {
  return {
    user: state.user
  }
}

const ConnectedComponent = connect(mapStateToProps, null)(ForbiddenContainer)
export { ConnectedComponent as Forbidden }
