import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { recoveryPassword } from '../actions'

import { RecoveryPasswordLayout } from '../layouts/RecoveryPasswordLayout'

export const RecoveryPasswordContainer = ({ recoverySent = {}, recoveryPassword }) => {
  const [petionSent, setPetitionSent] = useState(recoverySent)
  const [sent, setSent] = useState(false)
  const [form, setForm] = useState({
    email: '',
    password: '',
    device_token: 'jljlkjl'
  })

  useEffect(() => {
    setPetitionSent(recoverySent)
  }, [recoverySent])

  const handleInput = (event) => {
    setForm({
      ...form,
      [event.target.name]: event.target.value
    })
  }
  const handleSubmit = async (event) => {
    event.preventDefault()
    recoveryPassword({ email: form.email })
    setSent(true)
  }

  return (
    <RecoveryPasswordLayout>
      <div className='text-center text-muted mb-4'>
        <small>Recuperar contraseña</small>
      </div>
      {
        (() => {
          if (typeof petionSent !== 'boolean' || !petionSent) {
            return (
              <form action='' onSubmit={handleSubmit}>
                <div className='form-group mb-3'>
                  <div className='input-group input-group-merge input-group-alternative'>
                    <div className='input-group-prepend'>
                      <span className='input-group-text'>
                        <i className='fas fa-envelope' />
                      </span>
                    </div>
                    <input
                      type='text'
                      className='form-control'
                      placeholder='Email'
                      name='email'
                      onChange={handleInput}
                    />
                  </div>
                </div>
                {
                  typeof petionSent !== 'boolean' && !petionSent && (
                    <div className='text-center'>
                      <button className='btn btn-yellow my-4'>Parece que este correo no está en nuestra base de datos</button>
                    </div>
                  )
                }
                <div className='text-center'>
                  <button className='btn btn-yellow my-4'>Enviar link de recuperación</button>
                </div>
              </form>
            )
          } else {
            return (
              <>
                <h6>Revisa tu email</h6>
                <p>Te hemos enviado un link a tu correo electrónico a través del cual podrás generar tu nueva contraseña. No olvides revisar tu bandejar de SPAM</p>
                <Link to='/login'>Volver al login</Link>
              </>
            )
          }
        })()
      }

    </RecoveryPasswordLayout>
  )
}

const mapDispatchToProps = {
  recoveryPassword
}

const mapStateToProps = state => {
  return {
    recoverySent: state.recoverySent
  }
}

const ConnectedComponent = connect(mapStateToProps, mapDispatchToProps)(RecoveryPasswordContainer)
export { ConnectedComponent as RecoveryPassword }
