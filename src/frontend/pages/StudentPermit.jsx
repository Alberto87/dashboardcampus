import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { openingPermitFormRequestRequest, removeCheckingPermitRequest } from '../actions'

import { HeaderStudent } from '../components/headers/HeaderStudent'
import { MainContentNavBar } from '../components/MainContentNavBar'
import { MainContentHeader } from '../components/MainContentHeader'
import { MainContentLayout } from '../layouts/MainContentLayout'
import { ShortPermitLayout } from '../layouts/ShortPermitLayout'
import { ExtraordinaryPermitLayout } from '../layouts/ExtraordinaryPermitLayout'
import { NewPermit } from '../components/NewPermit/NewPermit'
import { NewFood } from '../components/NewPermit/NewFood'
import { LongPermitLayout } from '../layouts/LongPermitLayout'
import { NoteModal } from '../components/Modals/NoteModal'
import { HeaderGuardian } from '../components/headers/HeaderGuardian'

const StudentPermitContainer = ({
  properties,
  pastPermits,
  futurePermits,
  permitRequestSucceeded = false,
  openingPermitFormRequestRequest,
  user,
  removeCheckingPermitRequest
}) => {
  const [openedModal, setOpenedModal] = useState(false)
  const [openedFoodModal, setOpenedFoodModal] = useState(false)
  const [openNote, setOpenNote] = useState(false)
  const [note, setNote] = useState('')
  const [type, setType] = useState('')

  useEffect(() => {
    if (openedModal) {
      if (permitRequestSucceeded) {
        if (permitRequestSucceeded.open) {
          setOpenedModal(false)
        }
      }
    }
  }, [permitRequestSucceeded])

  useEffect(() => {
    setType(properties.type)
  }, [properties])

  const handleNewPermit = () => {
    openedModal ? openingPermitFormRequestRequest(false) : openingPermitFormRequestRequest(true)
    if (type === 'almuerzo' || type === 'cena') {
      if (openedFoodModal) {
        setOpenedFoodModal(false)
        setType(properties.type)
        openingPermitFormRequestRequest(false)
        removeCheckingPermitRequest()
      } else {
        setOpenedFoodModal(true)
        openingPermitFormRequestRequest(true)
      }
    } else {
      if (openedModal) {
        setOpenedModal(false)
        setType(properties.type)
        openingPermitFormRequestRequest(false)
        removeCheckingPermitRequest()
      } else {
        setOpenedModal(true)
        openingPermitFormRequestRequest(true)
      }
    }
  }

  const handleNoteModal = (text) => {
    if (openNote) {
      setOpenNote(false)
      setNote('')
    } else {
      setOpenNote(true)
      setNote(text)
    }
  }

  const alternativeOpeningModal = (typeRecived) => {
    setType(typeRecived)
    openingPermitFormRequestRequest(true)
    if (typeRecived !== 'almuerzo' && typeRecived !== 'cena') {
      openedModal ? setOpenedModal(false) : setOpenedModal(true)
    } else {
      openedFoodModal ? setOpenedFoodModal(false) : setOpenedFoodModal(true)
    }
  }

  const tableConfigurationShort = {
    short: ['Día salida', 'Fecha solicitud'],
    extraordinary: ['Estado', 'Día salida', 'Motivo', 'Nota', 'Fecha solicitud', 'Fecha validación'],
    abscence: ['Estado', 'Día salida', 'Día de vuelta', 'Motivo', 'Nota', 'Fecha solicitud', 'Fecha validación']
  }

  return (
    <>
      {
        user.role === 'student' && (
          <HeaderStudent type={properties.type} />
        )
      }
      {
        user.role === 'guardian' && (
          <HeaderGuardian type={properties.type} />
        )
      }
      <div className='main-content'>
        <MainContentNavBar alternativeOpeningModal={alternativeOpeningModal} />
        <MainContentHeader properties={properties} handleNewPermit={handleNewPermit} />
        <MainContentLayout>
          {properties.type === 'tonight' && (
            <ShortPermitLayout
              tableConfiguration={tableConfigurationShort.short}
              types={properties.type}
              pastPermits={pastPermits}
              futurePermits={futurePermits}
              future
              past
            />
          )}
          {properties.type === 'extraordinary' && (
            <ExtraordinaryPermitLayout
              tableConfiguration={tableConfigurationShort.extraordinary}
              types={properties.type}
              pastPermits={pastPermits}
              futurePermits={futurePermits}
              handleNoteModal={handleNoteModal}
              showIcon
            />
          )}
          {properties.type === 'abscence' && (
            <LongPermitLayout
              tableConfiguration={tableConfigurationShort.abscence}
              types={properties.type}
              pastPermits={pastPermits}
              futurePermits={futurePermits}
              handleNoteModal={handleNoteModal}
              showIcon
            />
          )}
        </MainContentLayout>
      </div>
      {
        openedModal && (
          <NewPermit
            type={type}
            openedModal={openedModal}
            handleNewPermit={handleNewPermit}
          />
        )
      }
      {
        openedFoodModal && (
          <NewFood
            openedModal={openedFoodModal}
            handleNewPermit={handleNewPermit}
            typeOfPermit={type}
          />
        )
      }
      {
        openNote && (
          <NoteModal
            openNote={openNote}
            handleNoteModal={handleNoteModal}
            note={note}
          />
        )
      }
    </>
  )
}

const mapDispatchToProps = {
  openingPermitFormRequestRequest,
  removeCheckingPermitRequest
}

const mapStateToProps = state => {
  return {
    user: state.user,
    pastPermits: state.initialData.historicalPermits,
    futurePermits: state.initialData.permits,
    permitRequestSucceeded: state.permitRequestSucceeded
  }
}

const ConnectedComponent = connect(mapStateToProps, mapDispatchToProps)(StudentPermitContainer)
export { ConnectedComponent as StudentPermit }
