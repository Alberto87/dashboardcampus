import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { HeaderResidence } from '../components/headers/HeaderResidence'
import { ResidenceMainContentNavBar } from '../components/ResidenceMainConentNavBar'
import { MainContentHeader } from '../components/MainContentHeader'
import { MainContentLayout } from '../layouts/MainContentLayout'
import { ResidentsListLayout } from '../layouts/ResidentsListLayout'

export const ListOfResidentsContainer = ({ properties, residents }) => {
  const [residenceList, setResidenceList] = useState([])

  useEffect(() => {
    setResidenceList(residents)
  }, [residents])

  const serachResident = (name) => {
    const newArray = []
    residents.map(item => {
      if (item.studentName.toLowerCase().indexOf(name.toLowerCase()) !== -1) {
        newArray.push(item)
      }
    })
    console.log(newArray)
    setResidenceList(newArray)
  }
  return (
    <>
      <HeaderResidence type='residents' />
      <div className='main-content'>
        <ResidenceMainContentNavBar searchBar serachResident={serachResident} />
        <MainContentHeader properties={properties} />
        <MainContentLayout>
          <ResidentsListLayout residents={residenceList} />
        </MainContentLayout>
      </div>
    </>
  )
}

const mapStateToProps = state => {
  return {
    residents: state.initialData.infoResidence.studentList
  }
}

const ConnectedComponent = connect(mapStateToProps, null)(ListOfResidentsContainer)
export { ConnectedComponent as ListOfResidents }
