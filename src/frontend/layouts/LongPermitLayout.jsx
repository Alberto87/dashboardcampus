import React, { useState, useEffect } from 'react'
import { SingleRowLayout } from './SingleRowLayout'
import { RowHeader } from '../components/RowHeader'
import { TableLayout } from './TableLayout'
import { TableThead } from '../components/TableThead'
import { SingleRowLong } from '../components/SingleRowLong'
import { checkIfArrayHasObject } from '../util/function'

export const LongPermitLayout = ({ types, pastPermits, futurePermits, handleNoteModal, tableConfiguration, showIcon }) => {
  const [validatedPermits, setValidatedPermits] = useState([])
  const [pendingPermits, setPendingPermits] = useState([])
  const [notValidatedPermits, setNotValidatedPermits] = useState([])

  useEffect(() => {
    const validatedList = []
    const pendingList = []
    const notValidatedList = []

    pastPermits.map((permit) => {
      permit.permits.map((item) => {
        if (types === item.type) {
          if (item.status === 'validated') {
            const found = checkIfArrayHasObject(futurePermits, item)
            if (!found) {
              validatedList.push(item)
            }
          } else if (item.status === 'pending') {
            const found = checkIfArrayHasObject(futurePermits, item)
            if (!found) {
              pendingList.push(item)
            }
          } else {
            const found = checkIfArrayHasObject(futurePermits, item)
            if (!found) {
              notValidatedList.push(item)
            }
          }
        }
      })
    })

    futurePermits.map((permit) => {
      if (types === permit.type) {
        if (permit.status === 'validated') {
          validatedList.push(permit)
        } else if (permit.status === 'pending') {
          pendingList.push(permit)
        } else {
          notValidatedList.push(permit)
        }
      }
    })

    setValidatedPermits(validatedList)
    setPendingPermits(pendingList)
    setNotValidatedPermits(notValidatedList)
  }, [pastPermits, futurePermits])

  return (
    <>
      <SingleRowLayout>
        <RowHeader
          title='Validados'
          icon='far fa-check-circle mr-2 text-green'
          showDots
          showIcon={showIcon}
        />
        <TableLayout>
          <TableThead list={tableConfiguration} />
          <tbody className='list'>
            {
              validatedPermits.map((permit, index) => {
                return (
                  <SingleRowLong
                    key={index}
                    permit={permit}
                    handleNoteModal={handleNoteModal}
                  />
                )
              })
            }
          </tbody>
        </TableLayout>
      </SingleRowLayout>
      <SingleRowLayout>
        <RowHeader
          title='Pendientes de validar'
          icon='far fa-clock mr-2 text-orange'
          showDots
          showIcon={showIcon}
        />
        <TableLayout>
          <TableThead list={tableConfiguration} />
          <tbody className='list'>
            {
              pendingPermits.map((permit, index) => {
                return (
                  <SingleRowLong
                    key={index}
                    permit={permit}
                    handleNoteModal={handleNoteModal}
                  />
                )
              })
            }
          </tbody>
        </TableLayout>
      </SingleRowLayout>
      <SingleRowLayout>
        <RowHeader
          title='No validados'
          icon='far fa-ban mr-2 text-gray'
          showDots
          showIcon={showIcon}
        />
        <TableLayout>
          <TableThead list={tableConfiguration} />
          <tbody className='list'>
            {
              notValidatedPermits.map((permit, index) => {
                if (permit.status !== 'expired-rejected') {
                  return (
                    <SingleRowLong
                      key={index}
                      permit={permit}
                      handleNoteModal={handleNoteModal}
                    />
                  )
                } else return null
              })
            }
          </tbody>
        </TableLayout>
      </SingleRowLayout>
    </>
  )
}
