import React from 'react'
import { Link } from 'react-router-dom'

import mainLogo from '../assets/img/MainLogoWhite.png'

export const LoginLayout = ({ children }) => {
  return (
    <div className='bg-default'>
      <nav className='navbar navbar-horizontal navbar-transparent navbar-main navbar-expand-lg'>
        <div className='container'>
          <Link to='/' className='navbar-brand'>
            <svg version='1.0' xmlns='http://www.w3.org/2000/svg' width='100px' viewBox='0 0 5000 1770' preserveAspectRatio='xMidYMid meet'>
              <g id='layer101' fill='#ffffff' stroke='none'>
                <path d='M643 1691 c-279 -139 -472 -298 -549 -455 l-39 -80 -3 -550 -3 -551 -26 -27 -27 -28 804 0 804 0 -27 28 -26 27 -3 551 -3 550 -39 80 c-50 102 -178 232 -321 327 -101 66 -356 197 -386 197 -8 0 -78 -31 -156 -69z m280 -135 c219 -111 401 -259 457 -371 l25 -50 3 -502 3 -503 -611 0 -610 0 0 493 0 492 30 64 c58 121 225 260 457 377 58 30 114 54 123 54 9 0 65 -24 123 -54z' />
                <path d='M692 1360 c-96 -34 -197 -106 -236 -169 -28 -44 -54 -123 -58 -176 -3 -39 -2 -40 25 -37 49 6 138 41 189 73 79 51 113 106 149 244 10 39 19 73 19 78 0 12 -31 7 -88 -13z' />
                <path d='M826 1349 c15 -76 56 -188 83 -226 45 -63 142 -117 254 -142 20 -5 39 -5 42 -1 9 15 -17 125 -42 177 -14 32 -45 70 -76 98 -57 50 -172 110 -229 120 l-38 7 6 -33z' />
                <path d='M518 888 l-118 -40 0 -246 c0 -239 -1 -246 -22 -269 l-21 -23 173 0 173 0 -21 23 c-22 23 -22 29 -22 310 0 223 -3 287 -12 286 -7 0 -66 -19 -130 -41z m82 -258 c0 -127 -4 -200 -10 -200 -6 0 -10 73 -10 200 0 127 4 200 10 200 6 0 10 -73 10 -200z m-60 -10 c0 -120 -4 -190 -10 -190 -6 0 -10 70 -10 190 0 120 4 190 10 190 6 0 10 -70 10 -190z m-60 -10 c0 -113 -4 -180 -10 -180 -6 0 -10 64 -10 173 0 153 3 187 17 187 2 0 3 -81 3 -180z' />
                <path d='M940 643 c0 -281 0 -287 -22 -310 l-21 -23 173 0 173 0 -21 23 c-21 23 -22 31 -22 268 l0 245 -122 42 c-68 23 -126 42 -130 42 -5 0 -8 -129 -8 -287z m80 -14 c0 -126 -4 -199 -10 -199 -6 0 -10 75 -10 206 0 134 3 203 10 199 6 -4 10 -83 10 -206z m60 -9 c0 -120 -4 -190 -10 -190 -6 0 -10 70 -10 190 0 120 4 190 10 190 6 0 10 -70 10 -190z m60 -15 c0 -110 -4 -175 -10 -175 -6 0 -10 65 -10 175 0 110 4 175 10 175 6 0 10 -65 10 -175z' />
                <path d='M3365 1604 c-91 -46 -166 -104 -201 -156 l-29 -41 -3 -246 c-2 -137 -7 -250 -13 -255 -20 -21 -5 -26 81 -26 82 0 107 8 80 25 -6 4 -10 85 -10 211 0 159 3 211 14 233 19 35 87 97 132 118 33 16 37 16 72 -2 53 -29 113 -85 129 -122 16 -40 19 -428 3 -438 -27 -17 -2 -25 80 -25 86 0 101 5 81 26 -6 5 -11 117 -13 254 l-3 246 -32 43 c-18 24 -56 62 -85 83 -55 42 -178 108 -200 108 -7 0 -44 -16 -83 -36z' />
                <path d='M2355 1480 c-4 -6 -81 -10 -198 -10 l-192 0 0 -295 0 -295 82 0 c76 0 108 10 83 25 -6 4 -10 88 -10 226 l0 219 118 0 c78 0 122 -4 130 -12 9 -9 12 6 12 70 0 75 -8 99 -25 72z' />
                <path d='M4975 1480 c-4 -6 -86 -10 -213 -10 -160 0 -203 -3 -194 -12 17 -17 17 -539 0 -556 -19 -19 3 -21 215 -24 l197 -3 0 79 c0 62 -3 77 -12 68 -8 -8 -50 -12 -125 -12 l-113 0 0 55 0 55 89 0 c50 0 93 -4 96 -10 16 -26 25 -3 25 65 0 68 -9 91 -25 65 -3 -5 -46 -10 -96 -10 l-89 0 0 60 0 60 118 0 c78 0 122 -4 130 -12 9 -9 12 6 12 70 0 80 -2 92 -15 72z' />
                <path d='M2423 1463 c3 -5 51 -127 106 -273 67 -174 100 -273 96 -287 -5 -23 -4 -23 105 -23 109 0 110 0 105 23 -4 14 28 120 85 285 49 145 90 268 90 273 0 5 -41 9 -91 9 -73 0 -89 -3 -81 -13 8 -9 6 -29 -5 -70 l-16 -57 -98 0 -97 0 -20 59 c-15 43 -17 62 -9 70 8 8 -14 11 -82 11 -51 0 -90 -3 -88 -7z m347 -261 c0 -4 -10 -39 -23 -77 l-23 -69 -27 73 c-15 40 -27 74 -27 77 0 2 23 4 50 4 28 0 50 -4 50 -8z' />
                <path d='M3899 1456 c9 -10 11 -92 9 -295 l-3 -281 115 0 c222 1 323 32 384 118 44 61 54 97 55 182 1 89 -25 156 -84 213 -66 64 -120 77 -317 77 -145 0 -169 -2 -159 -14z m331 -124 c71 -35 101 -151 60 -239 -28 -62 -68 -83 -157 -83 l-73 0 0 170 0 170 68 0 c44 0 79 -6 102 -18z' />
                <path d='M2157 690 c-128 -32 -217 -143 -217 -270 0 -105 67 -209 162 -250 62 -27 159 -30 223 -7 37 13 40 17 43 57 3 38 1 42 -15 35 -120 -51 -196 -44 -266 23 -36 34 -61 119 -52 175 23 138 137 198 280 148 l55 -19 0 39 c0 36 -3 41 -34 54 -46 19 -132 26 -179 15z' />
                <path d='M4275 691 c-83 -15 -157 -78 -172 -148 -4 -21 -8 -117 -8 -213 l0 -175 43 -3 42 -3 0 163 c1 192 8 233 49 270 59 53 160 45 205 -15 19 -26 21 -46 26 -220 l5 -192 43 -3 42 -3 0 193 c0 215 -8 250 -68 300 -50 41 -133 61 -207 49z' />
                <path d='M4728 687 c-64 -18 -78 -32 -78 -78 0 -22 2 -39 4 -39 2 0 27 11 54 25 98 49 202 24 202 -49 0 -42 -28 -68 -102 -96 -118 -44 -151 -82 -145 -169 3 -47 9 -59 40 -87 60 -55 145 -66 233 -29 31 13 34 18 34 54 0 23 -2 41 -5 41 -3 0 -23 -9 -45 -20 -82 -42 -170 -17 -170 48 0 38 21 55 110 91 41 17 88 44 105 61 28 28 30 36 30 99 0 60 -3 73 -25 96 -14 15 -37 33 -52 41 -40 21 -135 26 -190 11z' />
                <path d='M2434 673 c3 -10 50 -130 104 -268 l98 -250 49 0 50 0 92 260 c51 143 93 264 93 268 0 4 -21 7 -46 5 l-47 -3 -26 -75 -26 -75 -97 -3 -96 -3 -30 78 -30 78 -47 3 c-41 3 -46 1 -41 -15z m283 -308 c-17 -52 -34 -91 -38 -87 -6 7 -69 169 -69 178 0 2 31 4 69 4 l69 0 -31 -95z' />
                <path d='M3000 419 l0 -270 27 3 c21 2 51 32 128 122 56 66 105 121 111 122 5 2 56 -53 113 -122 79 -95 109 -124 127 -124 l24 0 0 270 0 270 -45 0 -45 0 0 -177 0 -178 -48 55 c-26 30 -65 75 -88 99 l-40 43 -85 -98 -84 -98 -3 177 -2 177 -45 0 -45 0 0 -271z' />
                <path d='M3650 420 l0 -270 103 0 c126 0 195 18 231 60 64 77 36 198 -55 246 -16 8 -65 17 -109 21 l-80 6 0 103 0 104 -45 0 -45 0 0 -270z m247 -44 c22 -19 28 -32 28 -65 0 -60 -20 -75 -110 -79 l-75 -4 0 86 0 86 64 0 c54 0 70 -4 93 -24z' />
              </g>
            </svg>
          </Link>
        </div>
      </nav>
      <div className='main-content'>
        <div className='header py-7 py-lg-8 pt-lg-9'>
          <div className='container'>
            <div className='header-body text-center mb-7'>
              <div className='row justify-content-center'>
                <div className='col-xl-5 col-lg-6 col-md-8 px-5'>
                  <h1 className='text-white display-3'>Bienvenido de nuevo.</h1>
                  <p className='text-lead text-white'>Inicia sesión para acceder a tu panel de administración personal.</p>
                </div>
              </div>
            </div>
          </div>
          <div className='separator separator-bottom separator-skew zindex-100'>
            <svg x='0' y='0' viewBox='0 0 2560 100' preserveAspectRatio='none' version='1.1' xmlns='http://www.w3.org/2000/svg'>
              <polygon className='fill-default' points='2560 0 2560 100 0 100' />
            </svg>
          </div>
        </div>
        <div className='container mt--8 pb-5'>
          <div className='row justify-content-center'>
            <div className='col-lg-5 col-md-7'>
              <div className='card bg-secondary border-0 mb-0'>
                <div className='card-body px-lg-5 py-lg-5'>
                  {children}
                </div>
              </div>
              <div className='row mt-3'>
                <div className='col-6'>
                  <Link to='/recuperar-contrasena'><small>Recuperar contraseña</small></Link>
                </div>
                <div className='col-6 text-right'>
                  <Link to='/'><small>Registrarse en Campus Laude</small></Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <footer className='footer footer-repositorio py-5 bg-default'>
        <div className='container'>
          <div className='row align-items-center justify-content-lg-between'>
            <div className='col-lg-6'>
              <div className='copyright text-center text-lg-left text-muted'>
                          © 2020 <Link to='/'>Campus Laude</Link>
              </div>
            </div>
            <div className='col-lg-6'>
              <ul className='nav nav-footer justify-content-center justify-content-lg-end'>
                <li className='nav-item'>
                  <Link to='/' className='nav-link'>Politica de privacidad</Link>
                </li>
                <li className='nav-item'>
                  <Link to='/' className='nav-link'>Aviso Legal</Link>
                </li>
                <li className='nav-item'>
                  <Link to='/' className='nav-link'>Contacto</Link>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </footer>
    </div>

  )
}
