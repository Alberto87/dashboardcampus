import React from 'react'

export const NewPermitLayout = ({ children }) => (

  <div className='modal-dialog modal- modal-dialog-centered modal- modal-mediano-2'>
    <div className='modal-content modal-content-2'>
      {children}
    </div>
  </div>

)
