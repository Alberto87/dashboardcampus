import React from 'react'
import { Footer } from '../components/Footer'

export const MainContentLayout = ({ children }) => (
  <div className='container-fluid mt--6'>
    {children}
    <Footer />
  </div>
)
