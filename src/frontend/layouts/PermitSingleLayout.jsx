import React from 'react'
import { Footer } from '../components/Footer'

export const PermitSingleLayout = ({ children }) => (
  <div className='container-fluid'>
    <div className='row justify-content-center'>
      <div className='col-lg-8 card-wrapper'>
        {children}
      </div>
    </div>
    <Footer />
  </div>
)
