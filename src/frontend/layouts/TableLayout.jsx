import React from 'react'

export const TableLayout = ({ children }) => {
  return (
    <div className='table-responsive active'>
      <table className='table align-items-center table-flush table-sm'>
        {children}
      </table>
    </div>
  )
}
