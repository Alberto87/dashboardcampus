import React, { useState, useEffect } from 'react'
import { SingleRowLayout } from './SingleRowLayout'
import { RowHeader } from '../components/RowHeader'
import { TableLayout } from './TableLayout'
import { TableThead } from '../components/TableThead'
import { SingleRowResident } from '../components/SingleRowResident'
import { Pagination } from '../components/Pagination'

export const ResidentsListLayout = ({ residents }) => {
  const [numberToShow, setNumberToShow] = useState(10)
  const [residentsList, setResidentsList] = useState([])
  const [pageNumber, setPageNumber] = useState(1)

  useEffect(() => {
    const residentsArray = []
    const number = residents.length < 10 ? residents.length : numberToShow
    for (let i = 0; i < number; i++) {
      residentsArray.push(residents[i])
    }
    setResidentsList(residentsArray)
  }, [residents])

  useEffect(() => {
    const maxlength = (numberToShow > residents.length) ? residents.length : numberToShow
    const residentsArray = []
    for (let i = 0; i < maxlength; i++) {
      residentsArray.push(residents[i])
    }
    setResidentsList(residentsArray)
  }, [numberToShow])

  const changePage = (page) => {
    const residentsArray = []
    const maxlength = (numberToShow * page > residents.length) ? residents.length : numberToShow * page
    for (let i = numberToShow * (page - 1); i < maxlength; i++) {
      residentsArray.push(residents[i])
    }
    setResidentsList(residentsArray)
    setPageNumber(page)
  }

  const changeNumberToShow = (number) => {
    setNumberToShow(number)
    setPageNumber(1)
  }

  const tableConfiguration = {
    residents: ['Residente', 'Tutor', 'Género', 'Tipo matrícula', 'Control parental']
  }
  return (
    <SingleRowLayout>
      <RowHeader
        title='Matriculados'
        icon='far fa-clock mr-2 text-orange'
      />
      <TableLayout>
        <TableThead list={tableConfiguration.residents} />
        <tbody className='list'>
          {
            residentsList.map((item, index) => {
              return (
                <SingleRowResident resident={item} key={index} />
              )
            })
          }
        </tbody>
        <Pagination
          numberToShow={numberToShow}
          totalLength={residents.length}
          pageNumber={pageNumber}
          changePage={changePage}
          changeNumberToShow={changeNumberToShow}
        />
      </TableLayout>

    </SingleRowLayout>
  )
}
