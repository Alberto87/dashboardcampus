import React, { useState, useEffect } from 'react'
import { SingleRowLayout } from './SingleRowLayout'
import { RowHeader } from '../components/RowHeader'
import { TableLayout } from './TableLayout'
import { TableThead } from '../components/TableThead'
import { SingleRowShort } from '../components/SingleRowShort'
import { checkIfArrayHasObject } from '../util/function'

export const ShortPermitLayout = ({ types, pastPermits, futurePermits, future, past, tableConfiguration = [] }) => {
  const [pastPermitsList, setPastPermits] = useState([])
  const [futurePermitsList, setFuturePermits] = useState([])

  useEffect(() => {
    const pastList = []
    const futureList = []

    pastPermits.map((permit) => {
      permit.permits.map((item) => {
        if (types === item.type) {
          const found = checkIfArrayHasObject(futurePermits, item)
          if (!found) {
            if (item.status === 'validated') {
              pastList.push(item)
            }
          }
        }
      })
    })

    setPastPermits(pastList)

    futurePermits.map((permit) => {
      if (types === permit.type) {
        const found = checkIfArrayHasObject(futureList, permit)
        if (!found) {
          if (permit.status === 'validated') {
            futureList.push(permit)
          }
        }
      }
    })

    setFuturePermits(futureList)
  }, [pastPermits, futurePermits])

  return (
    <>
      {
        future && (
          <SingleRowLayout>
            <RowHeader
              title='Pendientes de consumir'
              icon='far fa-clock mr-2 text-orange'
              showDots
            />
            <TableLayout>
              <TableThead list={tableConfiguration} />
              <tbody className='list'>
                {
                  futurePermitsList.map((permit, index) => {
                    return (
                      <SingleRowShort key={index} permit={permit} />
                    )
                  })
                }
              </tbody>
            </TableLayout>
          </SingleRowLayout>
        )
      }
      {
        past && (
          <SingleRowLayout>
            <RowHeader
              title='Consumidos'
              icon='far fa-check-circle mr-2 text-green'
              showDots
            />
            <TableLayout>
              <TableThead list={tableConfiguration} />
              <tbody className='list'>
                {
                  pastPermitsList.map((permit, index) => {
                    return (
                      <SingleRowShort key={index} permit={permit} />
                    )
                  })
                }
              </tbody>
            </TableLayout>
          </SingleRowLayout>
        )
      }
    </>
  )
}
