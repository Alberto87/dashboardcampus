import React, { useState, useEffect } from 'react'
import { SingleRowLayout } from './SingleRowLayout'
import { RowHeader } from '../components/RowHeader'
import { TableLayout } from './TableLayout'
import { TableThead } from '../components/TableThead'
import { SingleRowFood } from '../components/SingleRowFood'

export const FoodPermitLayout = ({ pastPermitsList, futurePermitsList, handleNoteModal, tableConfiguration, titles, showIcon }) => {
  const [pastPermit, setPastPermit] = useState([])
  const [futurePermit, setFuturePermit] = useState([])

  useEffect(() => {
    setPastPermit(pastPermitsList)
  }, [pastPermitsList])

  useEffect(() => {
    setFuturePermit(futurePermitsList)
  }, [futurePermitsList])
  return (
    <>
      <SingleRowLayout>
        <RowHeader
          title={titles[0]}
          icon='far fa-clock mr-2 text-orange'
          showDots
          showIcon={showIcon}
        />
        <TableLayout>
          <TableThead list={tableConfiguration.short} />
          <tbody className='list'>
            {
              futurePermit.map((item, index) => {
                return (
                  <SingleRowFood
                    item={item}
                    key={index}
                    handleNoteModal={handleNoteModal}
                  />
                )
              })
            }
          </tbody>
        </TableLayout>
      </SingleRowLayout>
      <SingleRowLayout>
        <RowHeader
          title={titles[1]}
          icon='far fa-check-circle mr-2 text-green'
          showDots
          showIcon={showIcon}
        />
        <TableLayout>
          <TableThead list={tableConfiguration.short} />
          <tbody className='list'>
            {
              pastPermit.map((item, index) => {
                return (
                  <SingleRowFood
                    item={item}
                    key={index}
                    handleNoteModal={handleNoteModal}
                  />
                )
              })
            }

          </tbody>
        </TableLayout>
      </SingleRowLayout>
    </>
  )
}
