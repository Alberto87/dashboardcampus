import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import MainLogo from '../assets/img/MainLogo.png'

export const HeaderLayout = (props) => {
  const [opened, setOpened] = useState(false)
  const [showLogo, setShowLogo] = useState(false)
  const handleNav = () => {
    setOpened(true)
    if (!opened) {
      document.body.classList.add('g-sidenav-pinned')
      document.body.classList.remove('g-sidenav-hidden')
      document.body.classList.add('g-sidenav-show')
      setOpened(true)
    } else {
      document.body.classList.remove('g-sidenav-pinned')
      document.body.classList.remove('g-sidenav-show')
      document.body.classList.add('g-sidenav-hidden')
      setOpened(false)
    }
  }
  const handleMouseEnter = () => {
    document.body.classList.remove('g-sidenav-hidden')
    document.body.classList.add('g-sidenav-show')
  }
  const hasClass = (element, className) => {
    return (' ' + element.className + ' ').indexOf(' ' + className + ' ') > -1
  }
  const handleMouseLeave = () => {
    if (!hasClass(document.body, 'g-sidenav-pinned')) {
      document.body.classList.remove('g-sidenav-show')
      document.body.classList.add('g-sidenav-hidden')
    }
  }
  useEffect(() => {
    setShowLogo(true)
  }, [])
  return (
    <nav className='sidenav navbar navbar-vertical fixed-left navbar-expand-xs navbar-light bg-white' onMouseEnter={handleMouseEnter} onMouseLeave={handleMouseLeave}>
      <div className='scroll-wrapper'>
        <div className={opened ? 'scrollbar-inner scroll-content' : 'scrollbar-inner scroll-content scroll-scrollx_visible'}>
          <div className='sidenav-header d-flex align-items-center'>
            {
              typeof window !== 'undefined' && showLogo && (
                <Link className='navbar-brand' to='/'>
                  <img src={MainLogo} className='navbar-brand-img' alt='CampusLaude Logo' />
                </Link>
              )
            }
            <div className='ml-auto'>
              <div className={!opened ? 'sidenav-toggler d-none d-xl-block' : 'sidenav-toggler d-none d-xl-block active'} onClick={handleNav}>
                <div className='sidenav-toggler-inner'>
                  <div className='sidenav-toggler-line' />
                  <div className='sidenav-toggler-line' />
                  <div className='sidenav-toggler-line' />
                </div>
              </div>
            </div>
          </div>
          <div className='navbar-inner'>
            <div className='collapse navbar-collapse' id='sidenav-collapse-main'>
              {props.children}
            </div>
          </div>
        </div>
      </div>
    </nav>
  )
}
