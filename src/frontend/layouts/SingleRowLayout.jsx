import React from 'react'

export const SingleRowLayout = ({ children }) => (
  <div className='row'>
    <div className='col'>
      <div className='card'>
        {children}
      </div>
    </div>
  </div>
)
