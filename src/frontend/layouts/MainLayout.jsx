import React from 'react'
import '../assets/vendor/@fortawesome/fontawesome-free/css/all.min.css'
import '../assets/vendor/fontawesome/css/all.css'
import '../assets/vendor/fontawesome/js/all.js'
import '../assets/styles/App.scss'

export const MainLayout = (props) => {
  return (
    <div>
      {props.children}
    </div>
  )
}
