import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'

export const PermitSingleNavContainer = ({ type, status, handleRoute, handleEditing, editing, deletePermit, user, permitDetail = {} }) => {
  const [avatar, setAvatar] = useState({})

  useEffect(() => {
    if (user.role === 'residence') {
      setAvatar(permitDetail.studentAvatar)
    }
  }, [permitDetail])
  return (
    <div className='d-flex justify-content-between'>
      <div>
        <div className='mt-4 mb-1 d-flex align-items-center' onClick={handleRoute}>
          <span href='enlace-tipo-1 d-inline-block'>
            <i className='far fa-angle-left mr-2' />
          </span>
          {
            user.role !== 'residence' && (
              <span className='enlace-tipo-1 d-inline-block' onClick={handleRoute}>
                {
                  type === 'tonight' && <span className='m-0'>Salidas nocturnas</span>
                }
                {
                  type === 'abscence' && <span className='m-0'>Permisos de ausencia</span>
                }
                {
                  type === 'extraordinary' && <span className='m-0'>Permisos extraordinarios</span>
                }
              </span>
            )
          }
          {
            user.role === 'residence' && (
              <>
                <span className='avatar avatar-sm rounded-circle mr-2 ml-1 enlace-tipo-1'>
                  {
                    typeof avatar !== 'undefined' && (
                      <img src={avatar.url} alt='' />
                    )
                  }
                </span>
                <span className='enlace-tipo-1 d-inline-block'>
                  <span>{permitDetail.studentName}</span>
                </span>
              </>
            )
          }
        </div>
        {
          type === 'tonight' && <h1 className='mb-4'>Salida nocturna</h1>
        }
        {
          type === 'abscence' && <h1 className='mb-4'>Permiso de ausencia</h1>
        }
        {
          type === 'extraordinary' && <h1 className='mb-4'>Permiso extraordinario</h1>
        }
      </div>
      {
        status === 'pending' && !editing && user.role === 'student' && (
          <div className='d-flex align-items-center'>
            <span className='btn-icon btn btn-yellow text-white text-uppercase bg-yellow' onClick={deletePermit}>
              <i className='far fa-pencil mr-2' />
                  Borrar permiso
            </span>
            <span className='btn-icon btn btn-yellow text-white text-uppercase bg-yellow' onClick={handleEditing}>
              <i className='far fa-pencil mr-2' />
                  Editar permiso
            </span>
          </div>
        )
      }
      {
        status === 'validated' && !editing && user.role === 'student' && (
          <div className='d-flex align-items-center'>
            <span className='btn-icon btn btn-yellow text-white text-uppercase bg-yellow' onClick={deletePermit}>
              <i className='far fa-pencil mr-2' />
                  Borrar permiso
            </span>
          </div>
        )
      }
    </div>
  )
}

const mapStateToProps = state => {
  return {
    user: state.user,
    permitDetail: state.permitDetail
  }
}

const ConnectedComponent = connect(mapStateToProps, null)(PermitSingleNavContainer)
export { ConnectedComponent as PermitSingleNav }
