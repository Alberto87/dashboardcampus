import React, { useState, useEffect } from 'react'
import { compareToObject } from '../../util/function'

export const NotEditableExtraordinaryForm = ({ permitDetail }) => {
  var moment = require('moment')
  require('moment/locale/es')
  const [permit, setPermit] = useState({})

  useEffect(() => {
    if (compareToObject(permit, permitDetail)) {
      setPermit(permitDetail)
    } else {
      setPermit(permitDetail)
    }
  }, [JSON.stringify(permitDetail)])
  return (
    <div className='card-body'>
      <div className='row py-3 align-items-center'>
        <div className='col-sm-3'>
          <small className='text-uppercase text-muted font-weight-bold'>Estado</small>
        </div>
        <div className='col-sm-9'>
          {permit.status === 'pending' && (
            <p className='mb-0 '>
              <span className='text-orange'>●</span> Pendiente
            </p>
          )}
          {permit.status === 'validated' && (
            <p className='mb-0 '>
              <span className='text-green'>●</span> Validado
            </p>
          )}
          {permit.status === 'validated-rejected' && (
            <p className='mb-0 '>
              <span className='text-gray'>●</span> Cancelado
            </p>
          )}
          {permit.status === 'pending-rejected' && (
            <p className='mb-0 '>
              <span className='text-red'>●</span> Rechazado
            </p>
          )}
        </div>
      </div>
      <div className='row py-3 align-items-center'>
        <div className='col-sm-3'>
          <small className='text-uppercase text-muted font-weight-bold'>Día de salida</small>
        </div>
        <div className='col-sm-9'>
          <p className='mb-0'>
            <i className='far fa-calendar-day mr-1 text-muted' />
            {moment(permit.dayStart).format('ddd, DD MMM')}
          </p>
        </div>
      </div>
      <div className='row py-3 align-items-center'>
        <div className='col-sm-3'>
          <small className='text-uppercase text-muted font-weight-bold'>Motivo</small>
        </div>
        <div className='col-sm-9'>
          <p className='mb-0'>
            {permit.reason === 'pernoctar' && 'Pernoctar'}
            {permit.reason === 'other' && 'Llego tarde'}
          </p>
        </div>
      </div>
      {
        permit.comment !== '' && (
          <div className='row py-3 align-items-center'>
            <div className='col-sm-3'>
              <small className='text-uppercase text-muted font-weight-bold'>Nota</small>
            </div>
            <div className='col-sm-9'>
              <p className='mb-0'>
                {permit.comment}
              </p>
            </div>
          </div>
        )
      }
    </div>
  )
}
