import React, { useState, useEffect } from 'react'
import { SingleRowPicnicColumn } from '../SingleRowPicnicColumn'
import { compareToObject } from '../../util/function'

export const NotEditaBleFoodForm = ({ permitDetail, badgeClass, badgeText }) => {
  var moment = require('moment')
  require('moment/locale/es')
  const [permit, setPermit] = useState({})

  useEffect(() => {
    if (compareToObject(permit, permitDetail)) {
      setPermit(permitDetail)
    } else {
      setPermit(permitDetail)
    }
  }, [JSON.stringify(permitDetail)])

  return (
    <div className='card-body'>
      <div className='row py-3 align-items-center'>
        <div className='col-sm-3'>
          <small className='text-uppercase text-muted font-weight-bold'>Tipo de solicitud</small>
        </div>
        <div className='col-sm-9'>
          {
            permit.picnic && (
              <SingleRowPicnicColumn item={permit} />
            )
          }
          {
            !permit.picnic && (
              <>
                {
                  permit.eatHere && permit.whenEat !== 'onTime' && (
                    <div className='mb-1 d-inline-block'>
                      <span className={badgeClass}>{badgeText}</span>
                    </div>
                  )

                }
                {
                  permit.eatHere && permit.whenEat === 'onTime' && (
                    <span className='text-muted'>A tiempo</span>
                  )
                }
                {
                  !permit.eatHere && (<span className='badge badge-md badge-warning'>Como fuera</span>)
                }
              </>
            )
          }
        </div>
      </div>
      <div className='row py-3 align-items-center'>
        <div className='col-sm-3'>
          <small className='text-uppercase text-muted font-weight-bold'>Día de comidad</small>
        </div>
        <div className='col-sm-9'>
          <p className='mb-0'>
            <i className='far fa-calendar-day mr-1 text-muted' />
            {moment(permit.orderedDate).format('ddd, DD MMM')}
          </p>
        </div>
      </div>
      {
        permit.note !== null && permit.note !== '' && (
          <div className='row py-3 align-items-center'>
            <div className='col-sm-3'>
              <small className='text-uppercase text-muted font-weight-bold'>Nota</small>
            </div>
            <div className='col-sm-9'>
              <p className='mb-0'>
                {permit.note}
              </p>
            </div>
          </div>
        )
      }
    </div>
  )
}
