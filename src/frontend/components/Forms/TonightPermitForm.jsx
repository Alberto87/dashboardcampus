import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { DatePicker } from '@material-ui/pickers'
import { addTonightPermit, checkMatchingPermit } from '../../actions'
import { MatchingPermitsBlock } from '../MatchingPermitsBlock'

export const TonightPermitFormContainer = ({ addTonightPermit, checkMatchingPermit, matchingPermit = [], residenceProfiles = [] }) => {
  var moment = require('moment')
  require('moment/locale/es')
  const [selectedDate, setSetlectedDate] = useState(moment().format('YYYY-MM-DD'))
  const [match, setMatch] = useState(false)
  const [matchSamePermit, setMatchSamePermit] = useState(false)
  const [matchingPermitArray, setMatchingPermitArray] = useState([])
  const [minDate, setMinDate] = useState(moment().format('YYYY-MM-DD'))

  useEffect(() => {
    setMatchingPermitArray(matchingPermit)
    if ((matchingPermit.length > 0)) {
      if ((matchingPermit.length === 1)) {
        if (matchingPermit[0].type === 'tonight') {
          setMatchSamePermit(true)
          setMatch(false)
        } else {
          setMatch(true)
          setMatchSamePermit(false)
        }
      } else {
        setMatch(true)
      }
    } else {
      setMatch(false)
      setMatchSamePermit(false)
    }
  }, [matchingPermit.length, matchingPermit[0]])

  useEffect(() => {
    const selectedProfile = residenceProfiles.find(item => item.name === 'tonight')
    const limitHour = selectedProfile.residence_max_hour
    const limitDate = moment().format('YYYY-MM-DD' + ' ' + limitHour)
    const now = moment().format('YYYY-MM-DD HH:mm')
    const a = moment(now)
    const b = moment(limitDate)
    const diff = b.diff(a, 'minutes', true)
    if (diff < 0) {
      setMinDate(moment().add(1, 'd').format('YYYY-MM-DD'))
      setSetlectedDate(moment().add(1, 'd').format('YYYY-MM-DD'))
      checkMatchingPermit({ dayStart: moment().add(1, 'd').format('YYYY-MM-DD HH:mm:ss') })
    }
  }, [])

  useEffect(() => {
    const currentTime = moment().format('HH:mm:ss')
    const dateStart = moment(selectedDate + ' ' + currentTime).format('YYYY-MM-DD HH:mm:ss')
    checkMatchingPermit({ dayStart: dateStart })
  }, [])

  const handleChangeDate = (date) => {
    const setDate = moment(date).format('YYYY-MM-DD')
    const currentTime = moment().format('HH:mm:ss')
    const dayStart = moment(setDate + ' ' + currentTime).format('YYYY-MM-DD HH:mm:ss')
    console.log(dayStart)
    checkMatchingPermit({ dayStart })
    setSetlectedDate(setDate)
  }

  const handleSubmit = (event) => {
    event.preventDefault()
    const currentTime = moment().format('HH:mm:ss')
    const dateToSend = moment(selectedDate + ' ' + currentTime).format('YYYY-MM-DD HH:mm:ss')
    addTonightPermit({ dateToSend: dateToSend })
  }

  return (
    <form action='' onSubmit={handleSubmit}>
      <div className='form-group row'>
        <label htmlFor='example-date-input' className='col-md-3 col-form-label form-control-label'>Día de salida*</label>
        <div className='col-md-9'>
          <div className='input-group-prepend'>
            <span className='input-group-text'>
              <i className='far fa-calendar-day text-muted' />
            </span>
            <DatePicker
              minDate={minDate}
              value={selectedDate}
              disableToolbar
              variant='inline'
              disablePast
              onChange={(date) => handleChangeDate(date)}
            />
          </div>
          {
            matchSamePermit && (<p>Ya hay una notificación para este día</p>)
          }
          {
            match && (
              <MatchingPermitsBlock matchingPermitArray={matchingPermitArray} />
            )
          }
        </div>
      </div>
      {
        !match && !matchSamePermit && (
          <div className='row d-flex justify-content-between'>
            <div className='col' />
            <div className='col d-flex justify-content-end'>
              <button className='btn-icon btn btn-yellow text-white text-uppercase desactivo bg-yellow'>Añadir permiso</button>
            </div>
          </div>
        )
      }
    </form>
  )
}

const mapDispatchToProps = {
  addTonightPermit,
  checkMatchingPermit
}

const mapStateToProps = state => {
  return {
    residenceProfiles: state.initialData.residenceProfiles,
    matchingPermit: state.matchingPermit
  }
}

const ConnectedComponent = connect(mapStateToProps, mapDispatchToProps)(TonightPermitFormContainer)
export { ConnectedComponent as TonightPermitForm }
