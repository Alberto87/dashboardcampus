import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { DatePicker } from '@material-ui/pickers'
import { addAbscencePermit, checkAbsenceMatchingPermit } from '../../actions'
import { MatchingPermitsBlock } from '../MatchingPermitsBlock'

export const AbscencePermitFormContainer = ({ addAbscencePermit, checkAbsenceMatchingPermit, matchingPermit = [], residenceProfiles = [] }) => {
  var moment = require('moment')
  require('moment/locale/es')
  const [selectedDate, setSetlectedDate] = useState(moment().format('YYYY-MM-DD'))
  const [minEndDate, setMinEndDate] = useState(moment().add(2, 'd').format('YYYY-MM-DD'))
  const [greenLight, setGreenLight] = useState(false)
  const [reason, setReason] = useState(false)
  const [note, setNote] = useState('')
  const [match, setMatch] = useState(false)
  const [matchingPermitArray, setMatchingPermitArray] = useState([])
  const [minDate, setMinDate] = useState(moment().format('YYYY-MM-DD'))

  useEffect(() => {
    setMatchingPermitArray(matchingPermit)
    setMatch((matchingPermit.length > 0))
  }, [matchingPermit.length, matchingPermit[0]])

  useEffect(() => {
    const selectedProfile = residenceProfiles.find(item => item.name === 'abscence')
    const limitHour = selectedProfile.residence_max_hour
    const limitDate = moment().format('YYYY-MM-DD' + ' ' + limitHour)
    const now = moment().format('YYYY-MM-DD HH:mm')
    const a = moment(now)
    const b = moment(limitDate)
    const diff = b.diff(a, 'minutes', true)
    if (diff < 0) {
      setMinDate(moment().add(1, 'd').format('YYYY-MM-DD'))
      setSetlectedDate(moment().add(1, 'd').format('YYYY-MM-DD'))
      setMinEndDate(moment(minEndDate).add(1, 'd').format('YYYY-MM-DD'))
      checkAbsenceMatchingPermit({ dayStart: moment().add(1, 'd').format('YYYY-MM-DD HH:mm:ss'), dayEnd: moment(minEndDate).add(1, 'd').format('YYYY-MM-DD HH:mm:ss') })
    }
  }, [])

  useEffect(() => {
    const currentTime = moment().format('HH:mm:ss')
    const dateStart = moment(selectedDate + ' ' + currentTime).format('YYYY-MM-DD HH:mm:ss')
    const dayEnd = moment(minEndDate + ' ' + currentTime).format('YYYY-MM-DD HH:mm:ss')
    checkAbsenceMatchingPermit({ dayStart: dateStart, dayEnd: dayEnd })
  }, [])

  const handleChangeStartDate = (date) => {
    const setDate = moment(date).format('YYYY-MM-DD')
    const currentTime = moment().format('HH:mm:ss')
    const dateStart = moment(setDate + ' ' + currentTime).format('YYYY-MM-DD HH:mm:ss')

    const a = moment(setDate)
    const b = moment(moment(minEndDate).format('YYYY-MM-DD'))
    const diff = b.diff(a, 'days', true)

    if (diff <= 1) {
      const addDate = moment(date).add(2, 'd').format('YYYY-MM-DD')
      const dayEnd = moment(addDate + ' ' + currentTime).format('YYYY-MM-DD HH:mm:ss')
      setMinEndDate(addDate)
      checkAbsenceMatchingPermit({ dayStart: dateStart, dayEnd: dayEnd })
    } else {
      const dayEnd = moment(minEndDate + ' ' + currentTime).format('YYYY-MM-DD HH:mm:ss')
      checkAbsenceMatchingPermit({ dayStart: dateStart, dayEnd: dayEnd })
    }

    setSetlectedDate(setDate)
  }

  const handleChangeEndDate = (date) => {
    const setDate = moment(date).format('YYYY-MM-DD')
    const currentTime = moment().format('HH:mm:ss')
    const dateStart = moment(selectedDate + ' ' + currentTime).format('YYYY-MM-DD HH:mm:ss')
    const dayEnd = moment(setDate + ' ' + currentTime).format('YYYY-MM-DD HH:mm:ss')
    checkAbsenceMatchingPermit({ dayStart: dateStart, dayEnd: dayEnd })
    setMinEndDate(setDate)
  }

  const handleReasonInput = (event) => {
    setGreenLight((event.target.value !== 'none'))
    setReason(event.target.value)
  }

  const handleNoteInput = (event) => {
    setNote(event.target.value)
  }

  const handleSubmit = (event) => {
    event.preventDefault()
    const currentTime = moment().format('HH:mm:ss')
    const dateStart = moment(selectedDate + ' ' + currentTime).format('YYYY-MM-DD HH:mm:ss')
    const dayEnd = moment(minEndDate + ' ' + currentTime).format('YYYY-MM-DD HH:mm:ss')
    addAbscencePermit({ dayStart: dateStart, dayEnd: dayEnd, reason: reason, note: note })
  }

  return (
    <form action='' onSubmit={handleSubmit}>
      <div className='form-group row'>
        <label htmlFor='example-date-input' className='col-md-3 col-form-label form-control-label'>Día de salida*</label>
        <div className='col-md-9'>
          <div className='input-group-prepend'>
            <span className='input-group-text'>
              <i className='far fa-calendar-day text-muted' />
            </span>
            <DatePicker
              minDate={minDate}
              value={selectedDate}
              disableToolbar
              variant='inline'
              disablePast
              onChange={(date) => handleChangeStartDate(date)}
            />
          </div>

        </div>
      </div>
      <div className='form-group row'>
        <label htmlFor='example-date-input' className='col-md-3 col-form-label form-control-label'>Día de vuelta*</label>
        <div className='col-md-9'>
          <div className='input-group-prepend'>
            <span className='input-group-text'>
              <i className='far fa-calendar-day text-muted' />
            </span>
            <DatePicker
              minDate={moment(selectedDate).add(2, 'd').format('YYYY-MM-DD')}
              value={minEndDate}
              disableToolbar
              variant='inline'
              disablePast
              onChange={(date) => handleChangeEndDate(date)}
            />
          </div>
          {
            match && (
              <MatchingPermitsBlock matchingPermitArray={matchingPermitArray} />
            )
          }
        </div>
      </div>

      <div className='form-group row'>
        <label htmlFor='' className='col-md-3 example-text-input'>Motivo *</label>
        <div className='col-md-9'>
          <select name='' id='' className='form-control' onChange={handleReasonInput} disabled={match}>
            <option value='none'> -- Selecciona una opción -- </option>
            <option value='academic'>Académico</option>
            <option value='fun'>Visita familiar</option>
            <option value='other'>Otro</option>
          </select>
        </div>
      </div>
      <div className='form-group row'>
        <label htmlFor='' className='col-md-3 col-form-label form-control-label py-0'>Nota</label>
        <div className='col-md-9'>
          <textarea name='' id='' className='form-control' rows='3' onChange={handleNoteInput} disabled={match} />
        </div>
      </div>
      {
        greenLight && (
          <div className='row d-flex justify-content-between'>
            <div className='col' />
            <div className='col d-flex justify-content-end'>
              <button className='btn-icon btn btn-yellow text-white text-uppercase desactivo bg-yellow'>Añadir permiso</button>
            </div>
          </div>
        )
      }
    </form>
  )
}

const mapDispatchToProps = {
  addAbscencePermit,
  checkAbsenceMatchingPermit
}

const mapStateToProps = state => {
  return {
    residenceProfiles: state.initialData.residenceProfiles,
    matchingPermit: state.matchingPermit
  }
}

const ConnectedComponent = connect(mapStateToProps, mapDispatchToProps)(AbscencePermitFormContainer)
export { ConnectedComponent as AbscencePermitForm }
