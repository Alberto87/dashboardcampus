import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { modifyFoodPermit, checkFoodMatchingPermit } from '../../actions'
import { MuiPickersUtilsProvider, DatePicker } from '@material-ui/pickers'
import MomentUtils from '@date-io/moment'
import { compareToObject } from '../../util/function'

export const EditableFoodFormContainer = ({ permitDetail, matchingFoodPermit = {}, disableEditing, checkFoodMatchingPermit, reciveForm }) => {
  var moment = require('moment')
  require('moment/locale/es')
  const [form, setForm] = useState(Object.assign({}, permitDetail))
  const [selectedDate, setSelectedDate] = useState(moment(permitDetail.orderedDate).format('YYYY-MM-DD'))
  const [matchingPermit, setMatchingPermit] = useState(false)

  useEffect(() => {
    if (!(Object.keys(matchingFoodPermit).length === 0)) {
      if (matchingFoodPermit.orderedDate !== permitDetail.orderedDate) {
        disableEditing(false)
        setMatchingPermit(true)
      } else {
        disableEditing(false)
        setMatchingPermit(false)
      }
    } else {
      setMatchingPermit(false)
    }
  }, [matchingFoodPermit, JSON.stringify(matchingFoodPermit)])

  const handleChangeDate = (date) => {
    checkMatchingPermits(date)
    const setDate = moment(date).format('YYYY-MM-DD HH:mm:ss')
    const partialForm = Object.assign({}, form)
    partialForm.orderedDate = setDate
    const selected = moment(date).format('YYYY-MM-DD')

    setForm(partialForm)
    setSelectedDate(selected)
    if (!matchingPermit) {
      disableEditing(compareToObject(partialForm, permitDetail))
    }
    reciveForm(partialForm)
  }

  const checkMatchingPermits = (selectedDate) => {
    const dateToCheck = moment(selectedDate).format('YYYY-MM-DD HH:mm:ss')
    const dateInfo = {
      dateToSend: dateToCheck,
      name: form.permitName
    }
    checkFoodMatchingPermit(dateInfo)
  }

  const handleWhereInput = (event) => {
    const partialForm = Object.assign({}, form)
    if (event.target.value === 'true') {
      partialForm.eatHere = true
      partialForm.picnic = false
    } else {
      partialForm.eatHere = false
    }
    setForm(partialForm)
    disableEditing(compareToObject(partialForm, permitDetail))
    reciveForm(partialForm)
  }

  const handleWheneInput = (event) => {
    const partialForm = Object.assign({}, form)
    partialForm.whenEat = event.target.value
    setForm(partialForm)
    if (event.target.value !== 'onTime') {
      disableEditing(compareToObject(partialForm, permitDetail))
    } else {
      disableEditing(false)
      if (form.note !== '') {
        disableEditing(compareToObject(partialForm, permitDetail))
      } else {
        disableEditing(false)
      }
    }
    reciveForm(partialForm)
  }

  const handleNoteInput = (event) => {
    const partialForm = Object.assign({}, form)
    partialForm.note = event.target.value
    setForm(partialForm)
    if (event.target.value === '' && form.eatHere && form.whenEat === 'onTime') {
      disableEditing(false)
    } else {
      disableEditing(compareToObject(partialForm, permitDetail))
    }
    reciveForm(partialForm)
  }

  const handlePicnicInput = (event) => {
    const partialForm = Object.assign({}, form)
    partialForm.picnic = (event.target.value === 'true')
    setForm(partialForm)
    disableEditing(compareToObject(partialForm, permitDetail))
    reciveForm(partialForm)
  }

  const handleInputPicnicTime = (event) => {
    const picnicTime = !(event.target.value === '') ? moment(selectedDate + ' ' + event.target.value + ':00').format('YYYY-MM-DD HH:mm:ss') : null
    const partialForm = Object.assign({}, form)
    partialForm.picnic_date = picnicTime
    setForm(partialForm)
    disableEditing(compareToObject(partialForm, permitDetail))
    reciveForm(partialForm)
  }

  return (
    <MuiPickersUtilsProvider utils={MomentUtils}>
      <div className='card-body'>
        <div className='row py-3 align-items-center'>
          <div className='col-sm-3'>
            <small className='text-uppercase text-muted font-weight-bold'>Día de comida </small>
          </div>
          <div className='col-sm-9'>
            <div className='input-group-prepend'>
              <span className='input-group-text'>
                <i className='far fa-calendar-day text-muted' />
              </span>
              <DatePicker
                value={selectedDate}
                disableToolbar
                variant='inline'
                disablePast
                onChange={(date) => handleChangeDate(date)}
              />
            </div>
            {
              matchingPermit && (
                <p>Hay un edicción de comida selecionada para este día</p>
              )
            }
          </div>
          <div className='col-sm-3'>
            <small className='text-uppercase text-muted font-weight-bold'>¿Cenas en el alojamiento?</small>
          </div>
          <div className='col-md-9'>
            <select name='' id='' className='form-control' onChange={handleWhereInput}>
              <option value='true' selected={permitDetail.eatHere}>Sí</option>
              <option value='false' selected={!permitDetail.eatHere}>No</option>
            </select>
          </div>
          {
            form.eatHere && (
              <>
                <div className='col-sm-3'>
                  <small className='text-uppercase text-muted font-weight-bold'>¿A qué hora cenarás? *</small>
                </div>
                <div className='col-md-9'>
                  <select name='' id='' className='form-control' onChange={handleWheneInput}>
                    <option value='before' selected={permitDetail.whenEat === 'before'}>30 minutos antes</option>
                    <option value='onTime' selected={permitDetail.whenEat === 'onTime'}>A tiempo</option>
                    <option value='after' selected={permitDetail.whenEat === 'after'}>30 minutos después</option>
                  </select>
                </div>
              </>
            )
          }
          {
            !form.eatHere && (
              <>
                <div className='col-sm-3'>
                  <small className='text-uppercase text-muted font-weight-bold'>¿Quieres un picnic?</small>
                </div>
                <div className='col-md-9'>
                  <select name='' id='' className='form-control' onChange={handlePicnicInput}>
                    <option value='true' selected={permitDetail.picnic}>Sí</option>
                    <option value='false' selected={!permitDetail.picnic}>No</option>
                  </select>
                </div>
              </>
            )
          }
          {
            form.picnic && (
              <>
                <div className='col-sm-3'>
                  <small className='text-uppercase text-muted font-weight-bold'>Hora de recogida</small>
                </div>
                <div className='col-sm-9'>
                  <div className='input-group-prepend'>
                    <span className='input-group-text'>
                      <i className='far fa-calendar-day text-muted' />
                    </span>
                    <input className='form-control timepicker' placeholder='Selecciona un día' type='time' defaultValue={moment(permitDetail.picnic_date).format('HH:mm')} onChange={handleInputPicnicTime} />
                  </div>
                </div>
              </>
            )
          }
          <div className='col-sm-3'>
            <small className='text-uppercase text-muted font-weight-bold'>Nota</small>
          </div>
          <div className='col-md-9'>
            <textarea name='' id='' className='form-control' rows='3' defaultValue={permitDetail.note} onChange={handleNoteInput} />
          </div>
        </div>
      </div>
    </MuiPickersUtilsProvider>
  )
}

const mapDispatchToProps = {
  checkFoodMatchingPermit,
  modifyFoodPermit
}

const mapStateToProps = state => {
  return {
    matchingFoodPermit: state.matchingFoodPermit
  }
}

const ConnectedComponent = connect(mapStateToProps, mapDispatchToProps)(EditableFoodFormContainer)
export { ConnectedComponent as EditableFoodForm }
