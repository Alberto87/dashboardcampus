import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { MuiPickersUtilsProvider, DatePicker } from '@material-ui/pickers'
import MomentUtils from '@date-io/moment'
import { checkAbsenceMatchingPermit } from '../../actions'
import { compareToObject } from '../../util/function'
import { MatchingPermitsBlock } from '../MatchingPermitsBlock'

export const EditableAbscenceFormContainer = ({ permitDetail, matchingPermit = [], disableEditing, reciveForm, checkAbsenceMatchingPermit }) => {
  var moment = require('moment')
  require('moment/locale/es')

  const [form, setForm] = useState(Object.assign({}, permitDetail))
  const [selectedDate, setSelectedDate] = useState(moment(permitDetail.dayStart).format('YYYY-MM-DD'))
  const [minEndDate, setMinEndDate] = useState(moment(permitDetail.dayEnd).format('YYYY-MM-DD'))
  const [matching, setMatchingPermit] = useState(false)
  const [matchingPermitArray, setMatchingPermitArray] = useState([])

  useEffect(() => {
    if (matchingPermit.length > 0) {
      const matchingPermitFiltered = matchingPermit.filter(items => items.id !== permitDetail.id)
      setMatchingPermitArray(matchingPermitFiltered)
      setMatchingPermit(matchingPermitFiltered.length > 0)
      if (matchingPermitFiltered.length > 0) {
        disableEditing(false)
      }
    } else {
      setMatchingPermit(false)
    }
  }, [matchingPermit, JSON.stringify(matchingPermit)])

  const handleChangeDate = (date) => {
    const dayStart = moment(date).format('YYYY-MM-DD HH:mm:ss')
    const setDate = moment(date).format('YYYY-MM-DD')

    const partialForm = Object.assign({}, form)
    partialForm.dayStart = dayStart
    const currentTime = moment(permitDetail.dayStart).format('HH:mm:ss')
    const dateStart = moment(setDate + ' ' + currentTime).format('YYYY-MM-DD HH:mm:ss')
    const endCurrentTime = moment(permitDetail.dayEnd).format('HH:mm:ss')
    const a = moment(setDate)
    const b = moment(moment(minEndDate).format('YYYY-MM-DD'))
    const diff = b.diff(a, 'days', true)

    if (diff <= 1) {
      const addDate = moment(date).add(2, 'd').format('YYYY-MM-DD')

      const dayEnd = moment(addDate + ' ' + endCurrentTime).format('YYYY-MM-DD HH:mm:ss')
      partialForm.dayEnd = dayEnd
      setMinEndDate(addDate)
      checkAbsenceMatchingPermit({ dayStart: dateStart, dayEnd: dayEnd })
    } else {
      const dayEnd = moment(minEndDate + ' ' + currentTime).format('YYYY-MM-DD HH:mm:ss')
      checkAbsenceMatchingPermit({ dayStart: dateStart, dayEnd: dayEnd })
    }

    setForm(partialForm)
    setSelectedDate(setDate)
    if (!matching) {
      disableEditing(compareToObject(partialForm, permitDetail))
    }
    reciveForm(partialForm)
  }

  const handleChangeEndDate = (date) => {
    const setDate = moment(date).format('YYYY-MM-DD')
    const currentTime = moment(permitDetail.dayStart).format('HH:mm:ss')
    const endCurrentTime = moment(permitDetail.dayStart).format('HH:mm:ss')
    const dateStart = moment(selectedDate + ' ' + currentTime).format('YYYY-MM-DD HH:mm:ss')
    const dayEnd = moment(setDate + ' ' + endCurrentTime).format('YYYY-MM-DD HH:mm:ss')
    checkAbsenceMatchingPermit({ dayStart: dateStart, dayEnd: dayEnd })
    setMinEndDate(setDate)
    const partialForm = Object.assign({}, form)
    partialForm.dayEnd = dayEnd
    setForm(partialForm)
    if (!matching) {
      console.log('estoy checkeando')
      disableEditing(compareToObject(partialForm, permitDetail))
    }
    reciveForm(partialForm)
  }

  const handleReasonInput = (event) => {
    const partialForm = Object.assign({}, form)
    partialForm.reason = event.target.value
    setForm(partialForm)
    disableEditing(compareToObject(partialForm, permitDetail))
    reciveForm(partialForm)
  }

  const hanleNoteInput = (event) => {
    const partialForm = Object.assign({}, form)
    partialForm.comment = event.target.value
    setForm(partialForm)
    disableEditing(compareToObject(partialForm, permitDetail))
    reciveForm(partialForm)
  }

  return (
    <MuiPickersUtilsProvider utils={MomentUtils}>
      <div className='card-body'>
        <div className='row py-3 align-items-center'>
          <div className='col-sm-3'>
            <small className='text-uppercase text-muted font-weight-bold'>Día de salida*</small>
          </div>
          <div className='col-sm-9'>
            <div className='input-group-prepend'>
              <span className='input-group-text'>
                <i className='far fa-calendar-day text-muted' />
              </span>
              <DatePicker
                value={selectedDate}
                disableToolbar
                variant='inline'
                disablePast
                onChange={(date) => handleChangeDate(date)}
              />
            </div>
          </div>
          <div className='col-sm-3'>
            <small className='text-uppercase text-muted font-weight-bold'>Día de vuelta*</small>
          </div>
          <div className='col-sm-9'>
            <div className='input-group-prepend'>
              <span className='input-group-text'>
                <i className='far fa-calendar-day text-muted' />
              </span>
              <DatePicker
                minDate={moment(selectedDate).add(2, 'd').format('YYYY-MM-DD')}
                value={minEndDate}
                disableToolbar
                variant='inline'
                disablePast
                onChange={(date) => handleChangeEndDate(date)}
              />
            </div>
            {
              matching && (
                <MatchingPermitsBlock matchingPermitArray={matchingPermitArray} />
              )
            }
          </div>
          <div className='col-sm-3'>
            <small className='text-uppercase text-muted font-weight-bold'>Motivo</small>
          </div>
          <div className='col-md-9'>
            <select name='' id='' className='form-control' onChange={handleReasonInput} defaultValue={permitDetail.reason}>
              <option value='academic'>Académico</option>
              <option value='fun'>Visita familiar</option>
              <option value='other'>Otro</option>
            </select>
          </div>
          <div className='col-sm-3'>
            <small className='text-uppercase text-muted font-weight-bold'>Nota</small>
          </div>
          <div className='col-md-9'>
            <textarea name='' id='' className='form-control' rows='3' defaultValue={permitDetail.note} onChange={hanleNoteInput} />
          </div>
        </div>
      </div>
    </MuiPickersUtilsProvider>
  )
}

const mapDispatchToProps = {
  checkAbsenceMatchingPermit
}

const mapStateToProps = state => {
  return {
    matchingPermit: state.matchingPermit
  }
}

const ConnectedComponent = connect(mapStateToProps, mapDispatchToProps)(EditableAbscenceFormContainer)
export { ConnectedComponent as EditableAbscenceForm }
