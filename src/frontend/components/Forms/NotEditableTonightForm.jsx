import React, { useState, useEffect } from 'react'
import { compareToObject } from '../../util/function'

export const NotEditableTonightForm = ({ permitDetail }) => {
  var moment = require('moment')
  require('moment/locale/es')
  const [permit, setPermit] = useState({})

  useEffect(() => {
    if (compareToObject(permit, permitDetail)) {
      setPermit(permitDetail)
    } else {
      setPermit(permitDetail)
    }
  }, [JSON.stringify(permitDetail)])
  return (
    <div className='card-body'>
      <div className='row py-3 align-items-center'>
        <div className='col-sm-3'>
          <small className='text-uppercase text-muted font-weight-bold'>Día de salida</small>
        </div>
        <div className='col-sm-9'>
          <p className='mb-0'>
            <i className='far fa-calendar-day mr-1 text-muted' />
            {moment(permit.dayStart).format('ddd, DD MMM')}
          </p>
        </div>
      </div>
    </div>
  )
}
