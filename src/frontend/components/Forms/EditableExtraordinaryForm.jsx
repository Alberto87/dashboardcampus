import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { MuiPickersUtilsProvider, DatePicker } from '@material-ui/pickers'
import MomentUtils from '@date-io/moment'
import { checkMatchingPermit } from '../../actions'
import { compareToObject } from '../../util/function'
import { MatchingPermitsBlock } from '../MatchingPermitsBlock'

export const EditableExtraordinaryFormContainer = ({ permitDetail, matchingPermit = [], disableEditing, reciveForm, checkMatchingPermit }) => {
  var moment = require('moment')
  require('moment/locale/es')

  const [form, setForm] = useState(Object.assign({}, permitDetail))
  const [selectedDate, setSelectedDate] = useState(moment(permitDetail.dayStart).format('YYYY-MM-DD'))
  const [matching, setMatchingPermit] = useState(false)
  const [matchingPermitArray, setMatchingPermitArray] = useState([])

  useEffect(() => {
    if (matchingPermit.length > 0) {
      if (!(Object.keys(matchingPermit[0]).length === 0)) {
        if (moment(matchingPermit[0].dayStart).format('YYYY-MM-DD') !== moment(permitDetail.dayStart).format('YYYY-MM-DD')) {
          disableEditing(false)
          setMatchingPermit(true)
          setMatchingPermitArray(matchingPermit)
        } else {
          console.log('secondo caso')
          disableEditing(false)
          setMatchingPermit(false)
        }
      } else {
        setMatchingPermit(false)
      }
    } else {
      setMatchingPermit(false)
    }
  }, [matchingPermit, JSON.stringify(matchingPermit)])

  const handleChangeDate = (date) => {
    const dayStart = moment(date).format('YYYY-MM-DD HH:mm:ss')
    checkMatchingPermit({ dayStart })
    const partialForm = Object.assign({}, form)
    partialForm.dayStart = dayStart
    const selected = moment(date).format('YYYY-MM-DD')

    setForm(partialForm)
    setSelectedDate(selected)
    if (!matching) {
      disableEditing(compareToObject(partialForm, permitDetail))
    }
    reciveForm(partialForm)
  }

  const handleReasonInput = (event) => {
    const partialForm = Object.assign({}, form)
    partialForm.reason = event.target.value
    setForm(partialForm)
    disableEditing(compareToObject(partialForm, permitDetail))
    reciveForm(partialForm)
  }

  const hanleNoteInput = (event) => {
    const partialForm = Object.assign({}, form)
    partialForm.comment = event.target.value
    setForm(partialForm)
    disableEditing(compareToObject(partialForm, permitDetail))
    reciveForm(partialForm)
  }

  return (
    <MuiPickersUtilsProvider utils={MomentUtils}>
      <div className='card-body'>
        <div className='row py-3 align-items-center'>
          <div className='col-sm-3'>
            <small className='text-uppercase text-muted font-weight-bold'>Día de salida*</small>
          </div>
          <div className='col-sm-9'>
            <div className='input-group-prepend'>
              <span className='input-group-text'>
                <i className='far fa-calendar-day text-muted' />
              </span>
              <DatePicker
                value={selectedDate}
                disableToolbar
                variant='inline'
                disablePast
                onChange={(date) => handleChangeDate(date)}
              />
            </div>
            {
              matching && (
                <MatchingPermitsBlock matchingPermitArray={matchingPermitArray} />
              )
            }
          </div>
          <div className='col-sm-3'>
            <small className='text-uppercase text-muted font-weight-bold'>Motivo</small>
          </div>
          <div className='col-md-9'>
            <select name='' id='' className='form-control' onChange={handleReasonInput}>
              <option value='other' selected={permitDetail.reason === 'other'}>Llego tarde</option>
              <option value='pernoctar' selected={permitDetail.reason === 'pernoctar'}>Duermo fuera</option>
            </select>
          </div>
          <div className='col-sm-3'>
            <small className='text-uppercase text-muted font-weight-bold'>Nota</small>
          </div>
          <div className='col-md-9'>
            <textarea name='' id='' className='form-control' rows='3' defaultValue={permitDetail.note} onChange={hanleNoteInput} />
          </div>
        </div>
      </div>
    </MuiPickersUtilsProvider>
  )
}

const mapDispatchToProps = {
  checkMatchingPermit
}

const mapStateToProps = state => {
  return {
    matchingPermit: state.matchingPermit
  }
}

const ConnectedComponent = connect(mapStateToProps, mapDispatchToProps)(EditableExtraordinaryFormContainer)
export { ConnectedComponent as EditableExtraordinaryForm }
