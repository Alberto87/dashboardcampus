import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { DatePicker } from '@material-ui/pickers'
import { addExtraordinaryPermit, checkMatchingPermit } from '../../actions'
import { MatchingPermitsBlock } from '../MatchingPermitsBlock'

export const ExtraordinaryPermitFormContainer = ({ addExtraordinaryPermit, checkMatchingPermit, matchingPermit = [], residenceProfiles = [] }) => {
  var moment = require('moment')
  require('moment/locale/es')
  const [selectedDate, setSetlectedDate] = useState(moment().format('YYYY-MM-DD'))
  const [greenLight, setGreenLight] = useState(false)
  const [reason, setReason] = useState(false)
  const [note, setNote] = useState('')
  const [match, setMatch] = useState(false)
  const [matchingPermitArray, setMatchingPermitArray] = useState([])
  const [minDate, setMinDate] = useState(moment().format('YYYY-MM-DD'))

  useEffect(() => {
    setMatchingPermitArray(matchingPermit)
    setMatch((matchingPermit.length > 0))
  }, [matchingPermit.length, matchingPermit[0]])

  useEffect(() => {
    const selectedProfile = residenceProfiles.find(item => item.name === 'extraordinary')
    const limitHour = selectedProfile.residence_max_hour
    const limitDate = moment().format('YYYY-MM-DD' + ' ' + limitHour)
    const now = moment().format('YYYY-MM-DD HH:mm')
    const a = moment(now)
    const b = moment(limitDate)
    const diff = b.diff(a, 'minutes', true)
    if (diff < 0) {
      setMinDate(moment().add(1, 'd').format('YYYY-MM-DD'))
      setSetlectedDate(moment().add(1, 'd').format('YYYY-MM-DD'))
      checkMatchingPermit({ dayStart: moment().add(1, 'd').format('YYYY-MM-DD HH:mm:ss') })
    }
  }, [])

  useEffect(() => {
    const currentTime = moment().format('HH:mm:ss')
    const dateStart = moment(selectedDate + ' ' + currentTime).format('YYYY-MM-DD HH:mm:ss')
    checkMatchingPermit({ dayStart: dateStart })
  }, [])

  const handleChangeDate = (date) => {
    const setDate = moment(date).format('YYYY-MM-DD')
    const currentTime = moment().format('HH:mm:ss')
    const dayStart = moment(setDate + ' ' + currentTime).format('YYYY-MM-DD HH:mm:ss')
    checkMatchingPermit({ dayStart })

    setSetlectedDate(setDate)
  }

  const handleReasonInput = (event) => {
    setGreenLight((event.target.value !== 'none'))
    setReason(event.target.value)
  }

  const handleNoteInput = (event) => {
    setNote(event.target.value)
  }

  const handleSubmit = (event) => {
    event.preventDefault()
    const currentTime = moment().format('HH:mm:ss')
    const dateToSend = moment(selectedDate + ' ' + currentTime).format('YYYY-MM-DD HH:mm:ss')
    addExtraordinaryPermit({ dateToSend: dateToSend, reason: reason, note: note })
  }

  return (
    <form action='' onSubmit={handleSubmit}>
      <div className='form-group row'>
        <label htmlFor='example-date-input' className='col-md-3 col-form-label form-control-label'>Día de salida*</label>
        <div className='col-md-9'>
          <div className='input-group-prepend'>
            <span className='input-group-text'>
              <i className='far fa-calendar-day text-muted' />
            </span>
            <DatePicker
              minDate={minDate}
              value={selectedDate}
              disableToolbar
              variant='inline'
              disablePast
              onChange={(date) => handleChangeDate(date)}
            />
          </div>
          {
            match && (
              <MatchingPermitsBlock matchingPermitArray={matchingPermitArray} />
            )
          }
        </div>
      </div>
      <div className='form-group row'>
        <label htmlFor='' className='col-md-3 example-text-input'>Motivo *</label>
        <div className='col-md-9'>
          <select name='' id='' className='form-control' onChange={handleReasonInput} disabled={match}>
            <option value='none'> -- Selecciona una opción -- </option>
            <option value='other'>Llego tarde</option>
            <option value='pernoctar'>Duermo fuera</option>
          </select>
        </div>
      </div>
      <div className='form-group row'>
        <label htmlFor='' className='col-md-3 col-form-label form-control-label py-0'>Nota</label>
        <div className='col-md-9'>
          <textarea name='' id='' className='form-control' rows='3' onChange={handleNoteInput} disabled={match} />
        </div>
      </div>
      {
        greenLight && (
          <div className='row d-flex justify-content-between'>
            <div className='col' />
            <div className='col d-flex justify-content-end'>
              <button className='btn-icon btn btn-yellow text-white text-uppercase desactivo bg-yellow'>Añadir permiso</button>
            </div>
          </div>
        )
      }
    </form>
  )
}

const mapDispatchToProps = {
  addExtraordinaryPermit,
  checkMatchingPermit
}

const mapStateToProps = state => {
  return {
    residenceProfiles: state.initialData.residenceProfiles,
    matchingPermit: state.matchingPermit
  }
}

const ConnectedComponent = connect(mapStateToProps, mapDispatchToProps)(ExtraordinaryPermitFormContainer)
export { ConnectedComponent as ExtraordinaryPermitForm }
