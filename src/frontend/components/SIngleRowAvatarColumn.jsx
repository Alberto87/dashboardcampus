import React from 'react'

export const SingleRowAvatarColumn = ({ permit }) => {
  return (
    <td className='residentes'>
      <div className='d-flex media align-items-center'>
        <span className='avatar avatar-sm rounded-circle mr-3'>
          <img src={permit.studentAvatar.url} alt='' />
        </span>
        <div className='media-body'>
          <span className='name mb-0 text-sm'>
            <span>{permit.studentName}</span>
          </span>
        </div>
      </div>
    </td>
  )
}
