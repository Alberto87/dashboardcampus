import React from 'react'

export const EditingBar = ({ handleEditing, editable, modifyPermit, isSaving, wantDeleting, deletePermitRequest, deletePermit, isDeleting }) => {
  return (
    <div className='barra-footer-fija '>
      <div className='izquierda'>
        {
          !wantDeleting && (
            <span className='text-sm text-muted mr-2'>
              <i className='fal fa-check mr-2' />
              {
                editable && !wantDeleting ? '¡Todo listo para guardar!' : 'Puedes modificar tu permiso'
              }

            </span>
          )
        }
        {
          wantDeleting && (
            <span className='text-sm text-muted mr-2'>
              <i className='fal fa-check mr-2' />
                ¿Quieres borrar definifavemte el permiso?
            </span>
          )
        }
      </div>
      <div className='derecha'>
        {
          !isSaving && !wantDeleting && (<span className='btn-icon btn btn-outline-light' onClick={handleEditing}>Descartar</span>)
        }

        {
          editable && !isSaving && !wantDeleting && (<span className='btn-icon btn btn-yellow text-white bg-yellow' onClick={modifyPermit}>GUARDAR</span>)
        }

        {
          isSaving && !wantDeleting && (<h6>Tu permiso se esta modificando</h6>)
        }
        {
          isDeleting && (<h6>Tu permiso se esta borrando</h6>)
        }
        {
          wantDeleting && !isDeleting && (
            <>
              <span className='btn-icon btn btn-outline-light' onClick={deletePermit}>NO</span>
              <span className='btn-icon btn btn-yellow text-white bg-yellow' onClick={deletePermitRequest}>SÍ</span>
            </>
          )
        }

      </div>
    </div>
  )
}
