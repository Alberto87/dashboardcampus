import React from 'react'

export const SingleRowPermitReason = ({ permit = {} }) => {
  if (permit.type === 'extraordinary') {
    if (permit.reason === 'other') {
      return (
        <td className='motivo'>
          <span>Llego tarde</span>
        </td>
      )
    } else {
      return (
        <td className='motivo'>
          <span>Pernoctar</span>
        </td>
      )
    }
  } else {
    return (
      <td className='motivo'>
        {
          permit.reason === 'academic' && (<span>Académico</span>)
        }
        {
          permit.reason === 'fun' && (<span>Visita familiar</span>)
        }
        {
          permit.reason === 'other' && (<span>Otros motivos</span>)
        }
      </td>
    )
  }
}
