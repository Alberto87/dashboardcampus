import React from 'react'

export const SingleRowPicnicColumn = ({ item }) => (
  <td className='estado2'>
    <a href='' className='mb-1 d-inline-block'>
      <span className='badge badge-md badge-info'>Picnic solicitado</span>
    </a>
    <span className='estado text-muted d-flex align-items-center'>
      {
        !item.prepared && !item.delivered && (
          <>
            <i className='far fa-clock text-orange mr-1' />
            <small>Pendiente</small>
          </>
        )
      }
      {
        item.prepared && !item.delivered && (
          <>
            <i className='far fa-check mr-1 text-green' />
            <small>Preparado</small>
          </>
        )
      }
      {
        item.prepared && item.delivered && (
          <>
            <i className='far fa-check-double mr-1 text-green' />
            <small>Recogido</small>
          </>
        )
      }
    </span>
  </td>
)
