import React from 'react'

export const RowHeader = ({ title, icon, showDots, showIcon }) => {
  return (
    <div className='card-header border-0 col-12 m-0 row'>

      <div className='col-6 m-0 p-0'>
        <h3 className='mb-0'>
          {
            showIcon && (
              <i className={icon} />
            )
          }
          {title}
        </h3>
      </div>
      <div className='col-6 m-0 p-0 text-right'>
        {
          showDots && (
            <div className='dropdow'>
              <a href='' className='btn btn-sm btn-icon-only mr-0'>
                <i className='fas fa-ellipsis-v' />
              </a>
            </div>
          )
        }
      </div>
    </div>
  )
}
