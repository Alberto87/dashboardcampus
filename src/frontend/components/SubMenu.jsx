import React from 'react'

export const SubMenu = ({ show, alternativeOpeningModal }) => {
  return (
    <div className={show ? 'dropdown-menu dropdown-menu-lg dropdown-menu-dark bg-default dropdown-menu-right dropdown-custom show' : 'dropdown-menu dropdown-menu-lg dropdown-menu-dark bg-default dropdown-menu-right dropdown-custom'}>
      <div className='row shortcuts px-4'>
        <span className='col-4 shortcut-item enlace-tipo-2' onClick={() => alternativeOpeningModal('tonight')}>
          <span className='shortcut-media avatar rounded-circle'>
            <i className='fad fa-stopwatch text-white' />
          </span>
          <small>Salida nocturna</small>
        </span>
        <span className='col-4 shortcut-item enlace-tipo-2' onClick={() => alternativeOpeningModal('extraordinary')}>
          <span className='shortcut-media avatar rounded-circle'>
            <i className='fad fa-moon-stars text-white' />
          </span>
          <small>Permiso extraordin.</small>
        </span>
        <span className='col-4 shortcut-item enlace-tipo-2' onClick={() => alternativeOpeningModal('abscence')}>
          <span className='shortcut-media avatar rounded-circle'>
            <i className='fad fa-suitcase-rolling text-white' />
          </span>
          <small>Permiso ausencia.</small>
        </span>
        <span className='col-4 shortcut-item enlace-tipo-2' onClick={() => alternativeOpeningModal('almuerzo')}>
          <span className='shortcut-media avatar rounded-circle'>
            <i className='fad fa-utensils-alt text-white' />
          </span>
          <small>Editar almuerzo</small>
        </span>
        <span className='col-4 shortcut-item enlace-tipo-2' onClick={() => alternativeOpeningModal('cena')}>
          <span className='shortcut-media avatar rounded-circle'>
            <i className='fad fa-pizza-slice text-white' />
          </span>
          <small>Editar cena</small>
        </span>
      </div>
    </div>
  )
}
