import React, { useState } from 'react'
import { connect } from 'react-redux'
import { removeMatchingAbscencePermit, removeMetchingPermit } from '../actions'

export const MatchingPermitsBlockContainer = ({ matchingPermitArray, removeMatchingAbscencePermit, removeMetchingPermit }) => {
  var moment = require('moment')
  require('moment/locale/es')

  const [indexSelected, setIndexSelected] = useState(false)

  const handleIndex = (index) => {
    setIndexSelected(index)
  }
  const removeIndex = () => {
    setIndexSelected(false)
  }
  const deletePermit = (id, type) => {
    if (type === 'abscence') {
      removeMatchingAbscencePermit({ id: id })
    } else {
      removeMetchingPermit({ id: id })
    }
    setIndexSelected(false)
  }
  return (
    <div className='aviso mt-4'>
      <div className='card card-alert mb-1'>
        <div className='card-header'>
          <h4>
            <i className='far fa-exclamation-triangle mr-1' />
                        Eliminar coincidencias
          </h4>
          <p className='mb-3 text-sm'>Ya hay permisos solicitados para esa fecha. Debes <span>eliminar</span> los mismos si deseas pedir este nuevo:</p>
          {
            matchingPermitArray.map((item, index) => {
              if (index !== indexSelected) {
                console.log(item)
                return (
                  <div className='d-flex align-items-end justify-content-between mb-3' key={index}>
                    <div className='d-flex align-items-center justify-content-between'>
                      <span className='badge badge-dot d-flex'>
                        {
                          item.status === 'pending' ? <i className='bg-orange' /> : <i className='bg-green' />
                        }
                        <div className='d-flex align-items-start'>
                          <span className='status mb-1 text-dark'>{item.type === 'extraordinary' ? 'Perm. extraordinario' : ((item.type === 'tonight') ? 'Salida noctura' : 'Perm de ausencia')}</span>
                          {
                            item.type !== 'abscence' && (
                              <small className='text-muted mt-1'>{moment(item.dayStart).format('DD MMM')}</small>
                            )
                          }
                          {
                            item.type === 'abscence' && (
                              <small className='text-muted mt-1'>{moment(item.dayStart).format('DD MMM')}-{moment(item.dayEnd).format('DD MMM')}</small>
                            )
                          }
                        </div>
                      </span>
                      {
                        item.status === 'pending' && (
                          <button className='btn-icon btn btn-sm btn-yellow px-3 py-2 text-white bg-yellow' onClick={() => handleIndex(index)}>
                            <i className='fas fa-ban mr-2' />
                                              Borrar
                          </button>
                        )
                      }
                      {
                        item.status !== 'pending' && (
                          <button className='btn-icon btn btn-sm btn-yellow px-3 py-2 text-white bg-yellow' onClick={() => handleIndex(index)}>
                            <i className='fas fa-ban mr-2' />
                                              Cancelar
                          </button>
                        )
                      }
                    </div>
                  </div>
                )
              } else {
                return (
                  <div className='d-flex align-items-end justify-content-between mb-3' key={index}>
                    <div className='d-flex align-items-center justify-content-between'>
                      <small>¿Estás seguro?</small>
                      <div>
                        <span className='btn-icon btn btn-sm btn-secondary px-3 py-2' onClick={removeIndex}>No</span>
                        <span className='btn-icon btn btn-sm btn-yellow px-3 py-2' onClick={() => deletePermit(item.id, item.type)}>Si</span>
                      </div>
                    </div>
                  </div>
                )
              }
            })
          }
        </div>
      </div>
    </div>
  )
}

const mapDispatchToProps = {
  removeMatchingAbscencePermit,
  removeMetchingPermit
}

const ConnectedComponent = connect(null, mapDispatchToProps)(MatchingPermitsBlockContainer)
export { ConnectedComponent as MatchingPermitsBlock }
