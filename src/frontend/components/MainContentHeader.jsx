import React from 'react'
import { connect } from 'react-redux'

import { PermitRequestSucceeded } from './PermitRequestSucceeded.jsx'

export const MainContentHeaderContainer = ({ properties, handleNewPermit, user }) => {
  return (
    <>
      <div className='header bg-primary pb-6'>
        <div className='container-fluid'>
          <div className='header-body'>
            <div className='row align-items-center py-4'>
              <div className='col-lg-6 col-7'>
                <h6 className='h2 text-white d-inline-block mb-0 mr-2'>{properties.breadcrumbs[0]}</h6>
                <nav className='d-none d-md-inline-block ml-md-2'>
                  <ol className='breadcrumb breadcrumb-links breadcrumb-dark'>
                    <li className='breadcrumb-item'>
                      <a href=''>
                        <i className='fas fa-home' />
                      </a>
                    </li>
                    <li className='breadcrumb-item'>
                      {properties.breadcrumbs[1]}
                    </li>
                    {
                      user.role !== 'residence' && (
                        <li className='breadcrumb-item active'>{properties.breadcrumbs[2]}</li>
                      )
                    }
                  </ol>
                </nav>
              </div>
              {
                user.role === 'student' && (
                  <div className='col-lg-6 col-5 text-right'>
                    <span className='btn btn-sm btn-secondary py-2 px-3' onClick={() => { handleNewPermit(properties.type) }}>
                      <i className='far fa-plus mr-2' />
                      {properties.button}

                    </span>
                  </div>
                )
              }
            </div>
          </div>
        </div>
        <PermitRequestSucceeded />
      </div>
    </>
  )
}

const mapStateToProps = state => {
  return {
    user: state.user
  }
}

const ConnectedComponent = connect(mapStateToProps, null)(MainContentHeaderContainer)
export { ConnectedComponent as MainContentHeader }
