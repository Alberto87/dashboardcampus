import React from 'react'

export const TableThead = ({ list }) => {
  return (
    <thead className='thead-light'>
      <tr>
        {
          list.map((item, index) => {
            return (
              <th className='sort' data-sort={item} key={index}>{item}</th>
            )
          })
        }
      </tr>
    </thead>
  )
}
