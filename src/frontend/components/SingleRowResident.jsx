import React from 'react'

export const SingleRowResident = ({ resident = {} }) => {
  return (
    <tr>
      <td>
        <div className='d-flex media align-items-center'>
          <span className='avatar avatar-sm rounded-circle mr-3'>
            <img src={resident.avatar.url} alt='' />
          </span>
          <div className='media-body'>
            <span className='name mb-0 text-sm'>
              <span className='enlace-tipo-2'>{resident.studentName}</span>
            </span>
          </div>
        </div>
      </td>
      <td className=''>
        <span>Álvaro González Benítez</span>
      </td>
      <td>
        <span>{resident.sex === 'male' ? 'Masculino' : 'Femenino'}</span>
      </td>
      <td>
        <span className='badge badge-md badge-info'>Acceso libre</span>
      </td>
      <td>Permisos, Comidas</td>
      <td className='acciones-2'>
        <div className='dropdown'>
          <button className='accion accion2 btn btn-secondary btn-sm scale-in-center text-light'>
            <i className='fas fa-ellipsis-v' />
          </button>
          <div className='dropdown-menu dropdown-menu-right dropdown-menu-arrow'>
            <span className='dropdown-item'>Ira a residente</span>
          </div>
        </div>
      </td>
    </tr>
  )
}
