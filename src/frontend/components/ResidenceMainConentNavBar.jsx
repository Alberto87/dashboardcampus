import React, { useState } from 'react'
import { connect } from 'react-redux'
import { logoutUser } from '../actions'

export const ResidenceMainContentNavBarContainer = ({ user, logoutUser, infoResidence = {}, searchBar, serachResident }) => {
  const [opened, setOpened] = useState(false)

  const handleDropDown = (e) => {
    e.preventDefault()
    opened ? setOpened(false) : setOpened(true)
  }

  const handleLogoutRquest = (e) => {
    e.preventDefault()
    logoutUser({}, '/login')
  }

  const handleInputChange = (event) => {
    serachResident(event.target.value)
  }

  const name = user.role === 'residence' ? infoResidence.name.split(' ')[0] + ' ' + infoResidence.name.split(' ')[0] : 'cocina'
  return (
    <nav className='navbar navbar-top navbar-expand navbar-dark bg-primary border-bottom'>
      <div className='container-fluid'>
        <div className='collapse navbar-collapse' id='navbarSupportedContent'>
          {
            searchBar && (
              <div className='navbar-search navbar-search-light form-inline mr-sm-3'>
                <div className='form-group mb-0'>
                  <div className='input-group input-group-alternative input-group-merge'>
                    <div className='input-group-prepend'>
                      <span className='input-group-text'>
                        <i className='fas fa-search' />
                      </span>
                    </div>
                    <input type='text' className='form-control' onChange={handleInputChange} />
                  </div>
                </div>
              </div>
            )
          }
          <ul className='navbar-nav align-items-center ml-md-auto'>
            <li className='nav-item d-xl-none'>
              <div className='pr-3 sidenav-toggler sidenav-toggler-dark active'>
                <div className='sidenav-toggler-inner'>
                  <i className='sidenav-toggler-line' />
                  <i className='sidenav-toggler-line' />
                  <i className='sidenav-toggler-line' />
                </div>
              </div>
            </li>
            <li className='nav-item d-sm-none'>
              <a href='' className='nav-link'>
                <i className='ni ni-zoom-split-in' />
              </a>
            </li>
            <li className='nav-item dropdown'>
              <span className='nav-link d-flex align-items-center' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                <i className='fas fa-bell mr-1' />
              </span>
            </li>
          </ul>
          <ul className='navbar-nav align-items-center ml-auto ml-md-0'>
            <li className='nav-item dropdown'>
              <a href='' className='nav-link pr-0' onClick={handleDropDown}>
                <div className='d-flex align-items-center'>
                  <span className='avatar avatar-sm rounded-circle'>
                    {
                      user.role === 'residence' && (
                        <img src={infoResidence.avatar.url} alt='' />
                      )
                    }
                  </span>
                  <div className='media-body ml-2 d-none d-lg-block'>
                    <span className='mb-0 text-sm font-weight-bold'>{name}</span>
                  </div>
                </div>
              </a>
              <div className={opened ? 'dropdown-menu dropdown-menu-right show' : 'dropdown-menu dropdown-menu-right'}>
                <div className='dropdown-header noti-title'>
                  <h6 className='text-overflow m-0'>¡Bienvenid@!</h6>
                </div>
                <div className='dropdown-divider' />
                <a href='' className='dropdown-item' onClick={handleLogoutRquest}>
                  <i className='far fa-sign-out' />
                  <span>Cerrar sesión</span>
                </a>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

const mapDispatchToProps = {
  logoutUser
}

const mapStateToProps = state => {
  return {
    user: state.user,
    infoResidence: state.user.role === 'residence' ? state.initialData.infoResidence : {}
  }
}

const ConnectedComponent = connect(mapStateToProps, mapDispatchToProps)(ResidenceMainContentNavBarContainer)
export { ConnectedComponent as ResidenceMainContentNavBar }
