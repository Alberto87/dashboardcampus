import React from 'react'

export const SingleRowPermitStatusColumn = ({ permit = {} }) => {
  return (
    <td>
      {
        permit.status === 'validated' && (
          <span className='badge badge-dot mr-4 d-flex align-items-center justify-content-start'>
            <i className='bg-green' />
            <span className='status mr-1'>Validado</span>
          </span>
        )
      }
      {
        permit.status === 'pending' && (
          <span className='badge badge-dot mr-4 d-flex align-items-center justify-content-start'>
            <i className='bg-orange' />
            <span className='status mr-1'>Pendiente</span>
          </span>
        )
      }
      {
        permit.status === 'pending-rejected' && (
          <span className='badge badge-dot mr-4 d-flex align-items-center justify-content-start'>
            <i className='bg-red' />
            <span className='status mr-1'>Rechazado</span>
          </span>
        )
      }
      {
        permit.status === 'validated-rejected' && (
          <span className='badge badge-dot mr-4 d-flex align-items-center justify-content-start'>
            <i className='bg-gray' />
            <span className='status mr-1'>Cancelado</span>
          </span>
        )
      }
    </td>
  )
}
