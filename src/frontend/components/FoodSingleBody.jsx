import React, { useState, useEffect } from 'react'
import { NotEditaBleFoodForm } from './Forms/NotEditableFoodForm'
import { EditableFoodForm } from './Forms/EditableFoodForm'

export const FoodSingleBody = ({ permitDetail, editing, disableEditing, reciveForm, wantDeleting }) => {
  var moment = require('moment')
  require('moment/locale/es')
  const [permit, setPermit] = useState({})

  useEffect(() => {
    setPermit(permitDetail)
  }, [JSON.stringify(permitDetail)])

  const badgeClass = (permitDetail.whenEat === 'after') ? 'badge badge-md badge-morada' : 'badge badge-md badge-azul'
  const badgeText = (permitDetail.whenEat === 'after') ? '30 Min después' : '30 Min antes'

  return (
    <div className='card'>
      <div className='card-header'>
        <h3 className='mb-0'>Datos del permiso</h3>
      </div>
      {
        !editing && (
          <NotEditaBleFoodForm
            permitDetail={permit}
            badgeClass={badgeClass}
            badgeText={badgeText}
          />
        )
      }
      {
        wantDeleting && (
          <NotEditaBleFoodForm
            permitDetail={permit}
            badgeClass={badgeClass}
            badgeText={badgeText}
          />
        )
      }
      {
        editing && !wantDeleting && (
          <EditableFoodForm permitDetail={permit} disableEditing={disableEditing} reciveForm={reciveForm} />
        )
      }
    </div>
  )
}
