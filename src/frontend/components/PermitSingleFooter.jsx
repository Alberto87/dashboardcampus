import React from 'react'

export const PermitSingleFooter = ({ type }) => (
  <div className='card card-gray'>
    <div className='card-body'>
      <div className='timeline timeline-one-side' data-timeline-axis-style='dashed' data-timeline-content='axis'>
        <div className='timeline-block'>
          <span className='timeline-step badge-gray-2'>
            <i className='far fa-plus text-muted' />
          </span>
          <div className='timeline-content'>
            <p className='mb-0 font-weight-400 text-muted'>Solicitud del permiso</p>
            <small className='text-muted font-weight-300'>Viernes, 1 de mayo a las 14:02h</small>
          </div>
        </div>
        {
          type !== 'tonight' && (
            <>
              <div className='timeline-block'>
                <span className='timeline-step badge-gray-2'>
                  <i className='far fa-pen text-muted' />
                </span>
                <div className='timeline-content'>
                  <p className='mb-0 font-weight-400 text-muted'>Edición del permiso</p>
                  <small className='text-muted font-weight-300'>Viernes, 1 de mayo a las 14:02h</small>
                </div>
              </div>
              <div className='timeline-block'>
                <span className='timeline-step badge-gray-2'>
                  <i className='far fa-check text-muted' />
                </span>
                <div className='timeline-content'>
                  <p className='mb-0 font-weight-400 text-muted'>Validación del permiso</p>
                  <small className='text-muted font-weight-300'>Viernes, 1 de mayo a las 14:02h</small>
                </div>
              </div>
            </>
          )
        }
      </div>
    </div>
  </div>
)
