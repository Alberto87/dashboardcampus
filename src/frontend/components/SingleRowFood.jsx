import React, { useState } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'

import { SingleRowPicnicColumn } from './SingleRowPicnicColumn'
import { SingleRowAvatarColumn } from './SIngleRowAvatarColumn'

export const SingleRowFoodContainer = ({ item = {}, handleNoteModal, user }) => {
  var moment = require('moment')
  require('moment/locale/es')

  const badgeClass = (item.whenEat === 'after') ? 'badge badge-md badge-morada' : 'badge badge-md badge-azul'
  const badgeText = (item.whenEat === 'after') ? '30 Min después' : '30 Min antes'
  const permitRoute = (item.permitName === 'almuerzo') ? `/comidas-almuerzos/${item.id}` : `/comidas-cenas/${item.id}`
  const [showSubMenu, setShowMenu] = useState(false)

  const handleSubMenu = () => {
    if (showSubMenu) {
      setShowMenu(false)
    } else {
      setShowMenu(true)
    }
  }
  return (
    <tr>
      {
        (() => {
          if (user.role === 'residence' || user.role === 'kitchen') {
            return (
              <SingleRowAvatarColumn permit={item} />
            )
          } else {
            return null
          }
        })()
      }
      {
        item.picnic && (<SingleRowPicnicColumn item={item} />)
      }
      {
        !item.picnic && (
          <td>
            {
              item.eatHere && item.whenEat !== 'onTime' && (
                <div className='mb-1 d-inline-block'>
                  <span className={badgeClass}>{badgeText}</span>
                </div>
              )

            }
            {
              item.eatHere && item.whenEat === 'onTime' && (
                <span className='text-muted'>A tiempo</span>
              )
            }
            {
              !item.eatHere && (<span className='badge badge-md badge-warning'>Como fuera</span>)
            }
          </td>
        )
      }
      <td className='dia1'>
        <span>{moment(item.orderedDate).format('ddd, DD MMM')}</span>
      </td>
      {
        item.note === null && (
          <td>
            <span className='text-muted'>Sin nota</span>
          </td>
        )
      }
      {
        item.note === '' && (
          <td>
            <span className='text-muted'>Sin nota</span>
          </td>
        )
      }
      {
        item.note !== null && item.note !== '' && (
          <td>
            <button type='button' className='btn btn-sm btn-neutral-2' onClick={() => { handleNoteModal(item.note) }}>
              <i className='far fa-sticky-note mr-1' />
              Ver nota
            </button>
          </td>
        )
      }
      <td className='fecha-1'>
        <span className='text-light'>11:15h, 1 May 2020</span>
      </td>
      <td className='acciones-2'>
        <div className={showSubMenu ? 'dropdown show' : 'dropdown'}>
          <button className='accion accion2 btn btn-secondary btn-sm scale-in-center text-light' onClick={handleSubMenu}>
            <i className='fas fa-ellipsis-v' />
          </button>
          <div className={showSubMenu ? 'dropdown-menu dropdown-menu-right dropdown-menu-arrow show' : 'dropdown-menu dropdown-menu-right dropdown-menu-arrow'}>
            <Link to={permitRoute} className='dropdown-item' onClick={handleSubMenu}>{(item.picnic) ? 'Ver picnic' : 'Ver solicitud'}</Link>
          </div>
        </div>
      </td>
    </tr>
  )
}

const mapStateToProps = state => {
  return {
    user: state.user
  }
}

const ConnectedComponent = connect(mapStateToProps, null)(SingleRowFoodContainer)
export { ConnectedComponent as SingleRowFood }
