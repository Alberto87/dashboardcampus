import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { logoutUser } from '../actions'

import { SubMenu } from './SubMenu'

export const MainContentNavBarContainer = ({ logoutUser, permitFormIsOpening = false, alternativeOpeningModal, userInfo, user = {} }) => {
  const [opened, setOpened] = useState(false)
  const [show, showSubmenu] = useState(false)

  useEffect(() => {
    if (permitFormIsOpening) {
      setOpened(false)
      showSubmenu(false)
    }
  }, [permitFormIsOpening])

  const handleDropDown = (e) => {
    e.preventDefault()
    opened ? setOpened(false) : setOpened(true)
  }

  const handleSubmenu = () => {
    show ? showSubmenu(false) : showSubmenu(true)
  }

  const handleLogoutRquest = (e) => {
    e.preventDefault()
    logoutUser({}, '/login')
  }

  const name = user.role === 'student' ? userInfo.name.split(' ')[0] + ' ' + userInfo.name.split(' ')[0] : userInfo.guardianName.split(' ')[0] + ' ' + userInfo.guardianName.split(' ')[0]
  return (
    <nav className='navbar navbar-top navbar-expand navbar-dark bg-primary border-bottom'>
      <div className='container-fluid'>
        <div className='collapse navbar-collapse' id='navbarSupportedContent'>
          <ul className='navbar-nav align-items-center ml-md-auto'>
            <li className='nav-item d-xl-none'>
              <div className='pr-3 sidenav-toggler sidenav-toggler-dark active'>
                <div className='sidenav-toggler-inner'>
                  <i className='sidenav-toggler-line' />
                  <i className='sidenav-toggler-line' />
                  <i className='sidenav-toggler-line' />
                </div>
              </div>
            </li>
            <li className='nav-item d-sm-none'>
              <a href='' className='nav-link'>
                <i className='ni ni-zoom-split-in' />
              </a>
            </li>
            <li className='nav-item dropdown'>
              <span className='nav-link d-flex align-items-center' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                <i className='fas fa-bell mr-1' />
              </span>
            </li>
            {
              user.role === 'student' && (
                <li className='nav-item dropdown ml-1'>
                  <span className='btn-icon-only rounded-circle btn bg-primary boton-acciones-rapidas' onClick={handleSubmenu}>
                    <i className='fal fa-plus' />
                  </span>
                  <SubMenu show={show} alternativeOpeningModal={alternativeOpeningModal} />
                </li>
              )
            }
          </ul>
          <ul className='navbar-nav align-items-center ml-auto ml-md-0'>
            <li className='nav-item dropdown'>
              <a href='' className='nav-link pr-0' onClick={handleDropDown}>
                <div className='d-flex align-items-center'>
                  <span className='avatar avatar-sm rounded-circle'>
                    <img src={userInfo.avatar.url} alt='' />
                  </span>
                  <div className='media-body ml-2 d-none d-lg-block'>
                    <span className='mb-0 text-sm font-weight-bold'>{name}</span>
                  </div>
                </div>
              </a>
              <div className={opened ? 'dropdown-menu dropdown-menu-right show' : 'dropdown-menu dropdown-menu-right'}>
                <div className='dropdown-header noti-title'>
                  <h6 className='text-overflow m-0'>¡Bienvenid@!</h6>
                </div>
                <div className='dropdown-divider' />
                <a href='' className='dropdown-item' onClick={handleLogoutRquest}>
                  <i className='far fa-sign-out' />
                  <span>Cerrar sesión</span>
                </a>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

const mapDispatchToProps = {
  logoutUser
}

const mapStateToProps = state => {
  return {
    permitFormIsOpening: state.permitFormIsOpening,
    userInfo: state.initialData.userInfo,
    user: state.user
  }
}

const ConnectedComponent = connect(mapStateToProps, mapDispatchToProps)(MainContentNavBarContainer)
export { ConnectedComponent as MainContentNavBar }
