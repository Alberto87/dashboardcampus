import React, { useState } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { SingleRowAvatarColumn } from './SIngleRowAvatarColumn'

export const SingleRowShortContainer = ({ permit = {}, user }) => {
  var moment = require('moment')
  require('moment/locale/es')

  const [showSubMenu, setShowMenu] = useState(false)
  const permitRoute = `/permisos-salidas-nocturnas/${permit.id}`
  const handleSubMenu = () => {
    if (showSubMenu) {
      setShowMenu(false)
    } else {
      setShowMenu(true)
    }
  }

  return (
    <tr>
      {
        (() => {
          if (user.role === 'residence' || user.role === 'kitchen') {
            return (
              <SingleRowAvatarColumn permit={permit} />
            )
          } else {
            return null
          }
        })()
      }
      <td className='dia1'>
        <span>{moment(permit.dayStart).format('ddd, DD MMM')}</span>
      </td>
      <td className='fecha-1'>
        <span className='text-light'>11:15h, 1 May 2020</span>
      </td>
      <td className='acciones-2'>
        <div className={showSubMenu ? 'dropdown show' : 'dropdown'}>
          <button className='accion accion2 btn btn-secondary btn-sm scale-in-center text-light' onClick={handleSubMenu}>
            <i className='fas fa-ellipsis-v' />
          </button>
          <div className={showSubMenu ? 'dropdown-menu dropdown-menu-right dropdown-menu-arrow show' : 'dropdown-menu dropdown-menu-right dropdown-menu-arrow'}>
            <Link to={permitRoute} className='dropdown-item' onClick={handleSubMenu}>Ver permiso</Link>
          </div>
        </div>
      </td>
    </tr>
  )
}

const mapStateToProps = state => {
  return {
    user: state.user
  }
}

const ConnectedComponent = connect(mapStateToProps, null)(SingleRowShortContainer)
export { ConnectedComponent as SingleRowShort }
