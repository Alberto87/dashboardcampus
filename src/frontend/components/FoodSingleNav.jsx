import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'

export const FoodSingleNavContainer = ({ user, permitDetail = {}, residenceProfiles = [], item, handleRoute, handleEditing, editing, deletePermit }) => {
  var moment = require('moment')
  require('moment/locale/es')

  const [itemType, setItemType] = useState('')

  const [editable, setEditable] = useState(false)

  const [avatar, setAvatar] = useState({})

  useEffect(() => {
    if (user.role === 'residence') {
      setAvatar(permitDetail.studentAvatar)
    }
  }, [permitDetail])

  useEffect(() => {
    setItemType(item.permitName)
  }, [item])

  useEffect(() => {
    // Calculate limit hour of food
    const findPermit = residenceProfiles.find(itemFind => itemFind.name === itemType) || {}
    const limitHour = findPermit.limit_hour
    if (typeof limitHour !== 'undefined') {
      const currentTime = moment().format('HH:mm')
      const now = moment().format('YYYY-MM-DD' + ' ' + currentTime)
      const permitDate = moment(item.orderedDate).format('YYYY-MM-DD' + ' ' + limitHour)
      const a = moment(now)
      const b = moment(permitDate)
      const diff = b.diff(a, 'minutes', true)
      setEditable((diff > 0))
    }
  }, [itemType])

  return (
    <div className='d-flex justify-content-between'>
      <div>
        <div className='mt-4 mb-1 d-flex align-items-center' onClick={handleRoute}>
          <span href='enlace-tipo-1 d-inline-block'>
            <i className='far fa-angle-left mr-2' />
          </span>
          {
            user.role !== 'residence' && (
              <span className='enlace-tipo-1 d-inline-block' onClick={handleRoute}>
                {
                  item.permitName === 'almuerzo' && <span className='m-0'>Almuerzos</span>
                }
                {
                  item.permitName === 'cena' && <span className='m-0'>Cenas</span>
                }
              </span>
            )
          }
          {
            user.role === 'residence' && (
              <>
                <span className='avatar avatar-sm rounded-circle mr-2 ml-1 enlace-tipo-1'>
                  {
                    typeof avatar !== 'undefined' && (
                      <img src={avatar.url} alt='' />
                    )
                  }
                </span>
                <span className='enlace-tipo-1 d-inline-block'>
                  <span>{permitDetail.studentName}</span>
                </span>
              </>
            )
          }
        </div>
        {
          item.permitName === 'almuerzo' && <h1 className='mb-4'>Almuerzo</h1>
        }
        {
          item.permitName === 'cena' && <h1 className='mb-4'>Cena</h1>
        }
      </div>
      {
        item.picnic && !item.prepared && editable && !editing && user.role === 'student' && (
          <div className='d-flex align-items-center'>
            <span className='btn-icon btn btn-yellow text-white text-uppercase bg-yellow' onClick={handleEditing}>
              <i className='far fa-pencil mr-2' />
                  Editar Picnic
            </span>
            <span className='btn-icon btn btn-yellow text-white text-uppercase bg-yellow ml-3' onClick={deletePermit}>
              <i className='far fa-pencil mr-2' />
                  Borrar Picnic
            </span>
          </div>
        )
      }
      {
        !item.picnic && editable && !editing && user.role === 'student' && (
          <div className='d-flex align-items-center'>
            <span className='btn-icon btn btn-yellow text-white text-uppercase bg-yellow' onClick={handleEditing}>
              <i className='far fa-pencil mr-2' />
                  Editar Solicitud
            </span>
            <span className='btn-icon btn btn-yellow text-white text-uppercase bg-yellow ml-3' onClick={deletePermit}>
              <i className='far fa-pencil mr-2' />
                  Borrar Solicitud
            </span>
          </div>
        )
      }
      {/* {
        status === 'pending' && (

        )
      }
      {
        status === 'validated' && (
          <div className='d-flex align-items-center'>
            <button className='btn-icon btn btn-yellow text-white text-uppercase' type='button'>
              <i className='far fa-pencil mr-2' />
                  Cancelar permiso
            </button>
          </div>
        )
      } */}
    </div>
  )
}

const mapStateToProps = state => {
  return {
    user: state.user,
    permitDetail: state.permitDetail
  }
}

const ConnectedComponent = connect(mapStateToProps, null)(FoodSingleNavContainer)
export { ConnectedComponent as FoodSingleNav }
