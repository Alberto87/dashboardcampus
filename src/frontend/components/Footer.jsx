import React from 'react'

export const Footer = () => (
  <footer className='footer pt-0'>
    <div className='row align-items-center justify-content-lg-between'>
      <div className='col-lg-6'>
        <div className='copyright text-center text-lg-left text-muted'>
                    © 2020 <a href='' className='font-weight-bold ml-1  enlace-tipo-2'>Campus Laude</a>
        </div>
      </div>
      <div className='col-lg-6'>
        <ul className='nav nav-footer justify-content-center justify-content-lg-end'>
          <li className='nav-item'>
            <a href='' className='nav-link'>Política de privacidad</a>
          </li>
          <li className='nav-item'>
            <a href='' className='nav-link'>Aviso Legal</a>
          </li>
          <li className='nav-item'>
            <a href='' className='nav-link'>Contacto</a>
          </li>
        </ul>
      </div>
    </div>
  </footer>
)
