import React from 'react'
import { NotEditableTonightForm } from './Forms/NotEditableTonightForm'
import { NotEditableExtraordinaryForm } from './Forms/NotEditableExtraordinaryForm'
import { NotEditableAbscenceForm } from './Forms/NotEditableAbscenceForm'
import { EditableExtraordinaryForm } from './Forms/EditableExtraordinaryForm'
import { EditableAbscenceForm } from './Forms/EditableAbscenceForm'

export const PermitSingleBody = ({ permitDetail, editing, reciveForm, disableEditing, wantDeleting }) => {
  var moment = require('moment')
  require('moment/locale/es')
  return (
    <div className='card'>
      <div className='card-header'>
        <h3 className='mb-0'>Datos del permiso</h3>
      </div>
      {
        permitDetail.type === 'tonight' && !editing && (
          <NotEditableTonightForm permitDetail={permitDetail} />
        )
      }
      {
        permitDetail.type === 'tonight' && wantDeleting && (
          <NotEditableTonightForm permitDetail={permitDetail} />
        )
      }
      {
        permitDetail.type === 'extraordinary' && !editing && (
          <NotEditableExtraordinaryForm permitDetail={permitDetail} />
        )
      }
      {
        permitDetail.type === 'extraordinary' && wantDeleting && (
          <NotEditableExtraordinaryForm permitDetail={permitDetail} />
        )
      }

      {
        permitDetail.type === 'extraordinary' && editing && !wantDeleting && (
          <EditableExtraordinaryForm permitDetail={permitDetail} disableEditing={disableEditing} reciveForm={reciveForm} />
        )
      }
      {
        permitDetail.type === 'abscence' && !editing && (
          <NotEditableAbscenceForm permitDetail={permitDetail} />
        )
      }
      {
        permitDetail.type === 'abscence' && wantDeleting && (
          <NotEditableAbscenceForm permitDetail={permitDetail} />
        )
      }
      {
        permitDetail.type === 'abscence' && editing && !wantDeleting && (
          <EditableAbscenceForm permitDetail={permitDetail} disableEditing={disableEditing} reciveForm={reciveForm} />
        )
      }
    </div>
  )
}
