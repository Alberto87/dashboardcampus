import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'

export const HeaderWidthCalendarSelectionKitchenContainer = ({ properties, user, referenceFoodDate, residenceProfiles = [] }) => {
  var moment = require('moment')
  require('moment/locale/es')

  const [isToday, setIsToday] = useState(false)
  const [isOpen, setIsOpen] = useState(false)

  useEffect(() => {
    setIsToday(moment().format('YYYY-MM-DD') === moment(referenceFoodDate).format('YYYY-MM-DD'))
  }, [referenceFoodDate])

  useEffect(() => {
    const profile = residenceProfiles.filter(item => item.name === properties.type)
    const limitHour = profile[0].limit_hour

    if (moment().format('YYYY-MM-DD') === moment(referenceFoodDate).format('YYYY-MM-DD')) {
      const now = moment().format('YYYY-MM-DD HH:mm:ss')
      const permitDate = moment().format('YYYY-MM-DD' + ' ' + limitHour)
      const a = moment(now)
      const b = moment(permitDate)
      const diff = b.diff(a, 'minutes', true)
      console.log('a', now)
      console.log('b', permitDate)
      setIsOpen(diff > 0)
    }
  }, [residenceProfiles])

  return (
    <>
      <div className='header bg-primary pb-6'>
        <div className='container-fluid'>
          <div className='header-body'>
            <div className='row align-items-center py-4'>
              <div className='col-lg-6 col-7'>
                <h6 className='h2 text-white d-inline-block mb-0 mr-2'>{properties.breadcrumbs[0]}</h6>
                {
                  isToday && isOpen && (
                    <span>Horario abierto</span>
                  )
                }
                {
                  isToday && !isOpen && (
                    <span>Horario cerrado</span>
                  )
                }
                <nav className='d-none d-md-inline-block ml-md-2'>
                  <ol className='breadcrumb breadcrumb-links breadcrumb-dark'>
                    <li className='breadcrumb-item'>
                      <a href=''>
                        <i className='fas fa-home' />
                      </a>
                    </li>
                    <li className='breadcrumb-item'>
                      {properties.breadcrumbs[1]}
                    </li>
                    <li className='breadcrumb-item active'>{properties.breadcrumbs[2]}</li>
                  </ol>
                </nav>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

const mapStateToProps = state => {
  return {
    user: state.user,
    referenceFoodDate: state.referenceFoodDate,
    residenceProfiles: state.initialData.foodProfiles
  }
}

const ConnectedComponent = connect(mapStateToProps, null)(HeaderWidthCalendarSelectionKitchenContainer)
export { ConnectedComponent as HeaderWidthCalendarSelectionKitchen }
