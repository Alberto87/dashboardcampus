import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'

export const NavLinkWithToggle = ({ title, icon, links, linkActive, active }) => {
  const [opened, setOpened] = useState(false)
  const toggleNavLink = () => {
    opened ? setOpened(false) : setOpened(true)
  }

  useEffect(() => {
    setOpened(active)
  }, [active])

  return (
    <li className='nav-item'>
      <a className={opened ? 'nav-link nivel-1 active collapsed' : 'nav-link nivel-1'} data-toggle='collapse' aria-expanded={opened ? 'true' : 'false'} onClick={toggleNavLink}>
        <i className={opened ? icon + ' text-primary' : icon} />
        <span className='nav-link-text'>{title}</span>
      </a>
      <div className={opened ? 'collapse show' : 'collapse'} id='navbar-components'>
        <ul className='nav nav-sm flex-column'>
          {
            links.map((link, index) => {
              const classLink = (link.route === linkActive) ? 'nav-link active' : 'nav-link'
              return (
                <li className='nav-item' key={index}>
                  <Link to={link.route} className={classLink}>{link.name}</Link>
                </li>
              )
            })
          }
        </ul>
      </div>
    </li>
  )
}
