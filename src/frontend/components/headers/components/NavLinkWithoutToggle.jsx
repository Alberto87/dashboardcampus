import React from 'react'
import { Link } from 'react-router-dom'

export const NavLinkWithOutToggle = ({ title, icon, url, active }) => {
  return (
    <li className='nav-item'>
      <Link to={url} className={active ? 'nav-link nivel-1 active collapsed' : 'nav-link nivel-1 collapsed'}>
        <i className={icon} />
        <span className='nav-link-text'>{title}</span>
      </Link>
    </li>
  )
}
