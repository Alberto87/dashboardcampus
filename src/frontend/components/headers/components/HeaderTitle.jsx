import React from 'react'

export const HeaderTitle = ({ title }) => (
  <>
    <hr className='my-3' />
    <h6 className='navbar-heading p-0 text-muted'>{title}</h6>
  </>
)
