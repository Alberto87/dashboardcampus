import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { tutorChangeStudent } from '../../../actions'

const TutorStudentInfoContainer = ({ userInfo = {}, tutorStudentsInfo = [], tutorChangeStudent }) => {
  const [studentArray, setStudentArray] = useState(tutorStudentsInfo)
  const [openDropDown, setOpenDropDOwn] = useState(false)

  useEffect(() => {
    setStudentArray(tutorStudentsInfo)
  }, [tutorStudentsInfo])

  const openSubMenu = () => {
    setOpenDropDOwn(!openDropDown)
  }

  const changeStudent = (name) => {
    tutorChangeStudent({ name: name })
  }

  const classContainer = studentArray.length > 1 ? 'btn btn-outline-neutral d-inline dropdown-toggle d-flex align-items-center justify-content-between' : 'btn btn-outline-neutral d-inline d-flex align-items-center justify-content-between'
  const classDropDown = openDropDown ? 'dropdown-menu show' : 'dropdown-menu'
  return (
    <div className='btn-group'>
      <div className={classContainer} onClick={openSubMenu}>
        <span className='d-flex align-items-center'>
          <span className='avatar avatar-xs rounded-circle mr-2'>
            <img src={userInfo.avatar.url} alt='' />
          </span>
          <span>{userInfo.name.split(' ')[0]}</span>
        </span>
        <span className='d-flex'>
          <span className='text-muted'>
            <i className='far fa-user-tag ml-1' />
          </span>
        </span>
      </div>
      {
        studentArray.length > 1 && (
          <div className={classDropDown}>
            <div className='dropdown-header noti-title'>
              <h6 className='text-overflow m-0'>Residentes</h6>
            </div>
            {
              tutorStudentsInfo.map((item, index) => {
                const classActive = item.name === userInfo.name ? 'dropdown-item active d-flex align-items-center' : 'dropdown-item d-flex align-items-center'
                return (
                  <span className={classActive} key={index} onClick={() => changeStudent(item.name)}>
                    <span className='avatar avatar-xs rounded-circle mr-2'>
                      <img src={item.avatar.url} alt='' />
                    </span>
                    <div>{item.name.split(' ')[0]} {item.name.split(' ')[1]}</div>
                  </span>
                )
              })
            }
          </div>
        )
      }
    </div>
  )
}

const mapDispatchToProps = {
  tutorChangeStudent
}

const mapStateToProps = state => {
  return {
    userInfo: state.initialData.userInfo,
    tutorStudentsInfo: state.tutorStudentsInfo
  }
}

const ConnectedComponent = connect(mapStateToProps, mapDispatchToProps)(TutorStudentInfoContainer)
export { ConnectedComponent as TutorStudentInfo }
