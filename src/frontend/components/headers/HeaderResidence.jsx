import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { HeaderLayout } from '../../layouts/HeaderLayouts'
import { HeaderTitle } from './components/HeaderTitle'
import { NavLinkWithToggle } from './components/NavLinkWithToggle'
import { NavLinkWithOutToggle } from './components/NavLinkWithoutToggle'

export const HeaderResidenceContainer = ({ type, infoResidence = {} }) => {
  const [linkActive, setLinkActive] = useState(null)
  const [residentsActive, setResidentsActive] = useState(false)
  const [foodActive, setFoodActive] = useState(false)
  const [homeActive, setHomeActive] = useState(false)
  const [permitActive, setPermitActive] = useState(false)

  useEffect(() => {
    if (type === 'almuerzo') {
      setLinkActive('/comidas-almuerzos')
      setFoodActive(true)
      setPermitActive(false)
      setResidentsActive(false)
      setHomeActive(false)
    } else if (type === 'cena') {
      setLinkActive('/comidas-cenas')
      setFoodActive(true)
      setPermitActive(false)
      setHomeActive(false)
    } else if (type === 'tonight') {
      setLinkActive('/permisos-salidas-nocturnas')
      setFoodActive(false)
      setPermitActive(true)
      setResidentsActive(false)
      setHomeActive(false)
    } else if (type === 'extraordinary') {
      setLinkActive('/permisos-extraordinarios')
      setFoodActive(false)
      setPermitActive(true)
      setResidentsActive(false)
      setHomeActive(false)
    } else if (type === 'abscence') {
      setLinkActive('/permisos-ausencia')
      setFoodActive(false)
      setPermitActive(true)
      setResidentsActive(false)
      setHomeActive(false)
    } else if (type === 'residents') {
      setLinkActive(null)
      setFoodActive(false)
      setPermitActive(false)
      setResidentsActive(true)
      setHomeActive(false)
    } else {
      setLinkActive(null)
      setFoodActive(false)
      setPermitActive(false)
      setResidentsActive(false)
      setHomeActive(true)
    }
  }, [type])

  return (
    <HeaderLayout>
      <NavLinkWithOutToggle
        title='Home'
        icon='far fa-home'
        url='/'
        active={homeActive}
      />
      <HeaderTitle title={infoResidence.name} />
      <ul className='navbar-nav'>
        <NavLinkWithOutToggle
          title='Residentes'
          icon='far fa-user-friends'
          url='/residentes'
          active={residentsActive}
        />
        <NavLinkWithToggle
          title='Comidas'
          icon='far fa-hat-chef'
          links={[{ name: 'Almuerzo', route: '/comidas-almuerzos' }, { name: 'Cena', route: '/comidas-cenas' }]}
          linkActive={linkActive}
          active={foodActive}
        />
        <NavLinkWithToggle
          title='Permisos'
          icon='far fa-house-leave'
          links={[{ name: 'Salidas nocturnas', route: '/permisos-salidas-nocturnas' }, { name: 'Perm. extraordinarios', route: '/permisos-extraordinarios' }, { name: 'Perm. de ausencia', route: '/permisos-ausencia' }]}
          linkActive={linkActive}
          active={permitActive}
        />
      </ul>
    </HeaderLayout>
  )
}

const mapStateToProps = state => {
  return {
    infoResidence: state.initialData.infoResidence
  }
}

const ConnectedComponent = connect(mapStateToProps, null)(HeaderResidenceContainer)
export { ConnectedComponent as HeaderResidence }
