import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { HeaderLayout } from '../../layouts/HeaderLayouts'
import { HeaderTitle } from './components/HeaderTitle'
import { NavLinkWithToggle } from './components/NavLinkWithToggle'
import { NavLinkWithOutToggle } from './components/NavLinkWithoutToggle'

export const HeaderKitchenContainer = ({ type }) => {
  const [linkActive, setLinkActive] = useState(null)
  const [foodActive, setFoodActive] = useState(false)
  const [homeActive, setHomeActive] = useState(false)

  useEffect(() => {
    if (type === 'almuerzo') {
      setLinkActive('/comidas-almuerzos')
      setFoodActive(true)
      setHomeActive(false)
    } else if (type === 'cena') {
      setLinkActive('/comidas-cenas')
      setFoodActive(true)
      setHomeActive(false)
    } else {
      setLinkActive(null)
      setFoodActive(false)
      setHomeActive(true)
    }
  }, [type])

  return (
    <HeaderLayout>
      <NavLinkWithOutToggle
        title='Home'
        icon='far fa-home'
        url='/'
        active={homeActive}
      />
      {/* <HeaderTitle title={infoResidence.name} /> */}
      <ul className='navbar-nav'>
        <NavLinkWithToggle
          title='Comidas'
          icon='far fa-hat-chef'
          links={[{ name: 'Almuerzo', route: '/comidas-almuerzos' }, { name: 'Cena', route: '/comidas-cenas' }]}
          linkActive={linkActive}
          active={foodActive}
        />
      </ul>
    </HeaderLayout>
  )
}

// const mapStateToProps = state => {
//   return {
//     infoResidence: state.initialData.infoResidence
//   }
// }

const ConnectedComponent = connect(null, null)(HeaderKitchenContainer)
export { ConnectedComponent as HeaderKitchen }
