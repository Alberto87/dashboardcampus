import React from 'react'

export const ValidatingPermitBar = ({ validatePermitRequest, rejectPermitRequest }) => {
  return (
    <div className='barra-footer-fija '>
      <div className='izquierda'>
        <span className='text-sm text-muted mr-2'>
              ¡Todo listo para guardar!
        </span>
      </div>
      <div className='derecha'>
        <span className='btn-icon btn btn-outline-light' onClick={rejectPermitRequest}>Rechazar</span>
        <span className='btn-icon btn btn-yellow text-white bg-yellow' onClick={validatePermitRequest}>Aprobar</span>
      </div>
    </div>
  )
}
