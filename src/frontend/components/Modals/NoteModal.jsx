import React from 'react'
import { NewPermitLayout } from '../../layouts/NewPermitLayout'
import { ModalHeader } from '../NewPermit/components/ModalHeader'

export const NoteModal = ({ openNote, handleNoteModal, note }) => {
  return (
    <div className={openNote ? 'modal modal-background fade show' : 'modal modal-background fade show'}>
      <div className='modal-dialog modal- modal-dialog-centered modal- '>
        <div className='modal-content modal-content-2'>
          <ModalHeader handleNewPermit={handleNoteModal} />
          <div className='modal-body'>
            <p>{note}</p>
          </div>
          <div className='modal-footer'>
            <span className='btn btn-link  ml-auto' onClick={() => { handleNoteModal('') }}>Cerrar</span>
          </div>
        </div>
      </div>
    </div>
  )
}
