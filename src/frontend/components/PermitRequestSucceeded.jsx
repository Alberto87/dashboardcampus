import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { removePermitRequestSucceeded } from '../actions'
import { NewPermitLayout } from '../layouts/NewPermitLayout'

export const PermitRequestSucceededContainer = ({ permitRequestSucceeded = false, removePermitRequestSucceeded }) => {
  const [succeeded, setSucceeded] = useState(permitRequestSucceeded)
  const [type, setType] = useState(false)
  const [status, setStatus] = useState(false)

  useEffect(() => {
    if (permitRequestSucceeded) {
      setSucceeded(permitRequestSucceeded.open)
      if (typeof permitRequestSucceeded.permit.type !== 'undefined') {
        setType(permitRequestSucceeded.permit.type)
        setStatus(permitRequestSucceeded.status)
      } else {
        if (permitRequestSucceeded.permit.picnic) {
          setType('picnic')
        } else {
          setType(permitRequestSucceeded.permit.permitName)
        }
        setStatus(permitRequestSucceeded.status)
      }
    } else {
      setSucceeded(false)
    }
  }, [permitRequestSucceeded])

  const closeModal = () => {
    removePermitRequestSucceeded()
  }

  return (
    <div className={succeeded ? 'modal modal-background show' : 'modal modal-background fade d-none'}>
      <NewPermitLayout>
        <div className='card-body'>
          <div className='text-center py-3 px-0'>
            <span className='d-block text-green mb-4'>
              <i className='fad fa-check-circle' />
            </span>
            {
              succeeded && status === 'create' && (
                <>
                  {
                    type === 'tonight' && (
                      <>
                        <h1 className='display-3'>¡Salida notificada!</h1>
                        <small className='text-muted'>Tu salida ha sido notificada correctamente</small>
                      </>
                    )
                  }
                  {
                    type === 'extraordinary' && (
                      <>
                        <h1 className='display-3'>¡Permiso solicitado!</h1>
                        <small className='text-muted'>Tu permiso extraordinario se ha creado correctamente. Ahora toca esperar a que tu tutor lo valide.</small>
                      </>
                    )
                  }
                  {
                    type === 'abscence' && (
                      <>
                        <h1 className='display-3'>¡Permiso de ausencia solicitado!</h1>
                        <small className='text-muted'>Tu permiso de ausencia se ha creado correctamente. Ahora toca esperar a que tu tutor lo valide.</small>
                      </>
                    )
                  }
                  {
                    type === 'cena' && (
                      <>
                        <h1 className='display-3'>¡Cena editada!</h1>
                        <small className='text-muted'>Tu cena ha sido editada correctamente.</small>
                      </>
                    )
                  }
                  {
                    type === 'almuerzo' && (
                      <>
                        <h1 className='display-3'>Almuerzo editado!</h1>
                        <small className='text-muted'>!Tu almuerzo ha sido editado correctamente.</small>
                      </>
                    )
                  }
                  {
                    type === 'picnic' && (
                      <>
                        <h1 className='display-3'>¡Picnic solicitado correctamente!</h1>
                        <small className='text-muted'>!Tu picnic ha sido solicitado correctamente. </small>
                      </>
                    )
                  }
                </>
              )
            }
            {
              succeeded && status === 'delete' && (
                <>
                  {
                    type === 'tonight' && (
                      <>
                        <h1 className='display-3'>¡Notificación de salida eliminada!</h1>
                        <small className='text-muted'>Tu notificación de salida ha sido eliminada correctamente</small>
                      </>
                    )
                  }
                  {
                    type === 'extraordinary' && (
                      <>
                        <h1 className='display-3'>¡Permiso extraordinario borrado correctamente!</h1>
                        <small className='text-muted'>Tu permiso extraordinario se ha borrado correctamente.</small>
                      </>
                    )
                  }
                  {
                    type === 'abscence' && (
                      <>
                        <h1 className='display-3'>¡Permiso de ausencia borrado correctamente!</h1>
                        <small className='text-muted'>Tu permiso de ausencia se ha borrado correctamente.</small>
                      </>
                    )
                  }
                  {
                    type === 'cena' && (
                      <>
                        <h1 className='display-3'>¡Edicción de cena eliminada !</h1>
                        <small className='text-muted'>Tu edicción de cena ha sido eliminada correctamente.</small>
                      </>
                    )
                  }
                  {
                    type === 'almuerzo' && (
                      <>
                        <h1 className='display-3'>¡Edicción de almuerzo eliminada!</h1>
                        <small className='text-muted'>Tu edicción de almuerzo ha sido eliminada correctamente.</small>
                      </>
                    )
                  }
                  {
                    type === 'picnic' && (
                      <>
                        <h1 className='display-3'>¡Picnic eliminado correctamente!</h1>
                        <small className='text-muted'>!Tu picnic ha sido eliminado correctamente. </small>
                      </>
                    )
                  }
                </>
              )
            }
            {
              succeeded && status === 'modify' && (
                <>
                  {
                    type === 'extraordinary' && (
                      <>
                        <h1 className='display-3'>¡Permiso extraordinario editado correctamente!</h1>
                        <small className='text-muted'>Tu permiso extraordinario se ha editado correctamente.</small>
                      </>
                    )
                  }
                  {
                    type === 'abscence' && (
                      <>
                        <h1 className='display-3'>¡Permiso de ausencia editado correctamente!</h1>
                        <small className='text-muted'>Tu permiso de ausencia se ha editado correctamente.</small>
                      </>
                    )
                  }
                  {
                    type === 'cena' && (
                      <>
                        <h1 className='display-3'>¡Cena editada!</h1>
                        <small className='text-muted'>Tu cena ha sido editada correctamente.</small>
                      </>
                    )
                  }
                  {
                    type === 'almuerzo' && (
                      <>
                        <h1 className='display-3'>Almuerzo editado!</h1>
                        <small className='text-muted'>!Tu almuerzo ha sido editado correctamente.</small>
                      </>
                    )
                  }
                  {
                    type === 'picnic' && (
                      <>
                        <h1 className='display-3'>¡Picnic editado correctamente!</h1>
                        <small className='text-muted'>!Tu picnic ha sido editado correctamente. </small>
                      </>
                    )
                  }
                </>
              )
            }
            {
              succeeded && status === 'validated' && (
                <>
                  <h1 className='display-3'>¡Permiso validado!</h1>
                  <small className='text-muted'>El permiso se ha validado correctamente.</small>
                </>
              )
            }
            {
              succeeded && status === 'rejected' && (
                <>
                  <h1 className='display-3'>¡Permiso rechazado!</h1>
                  <small className='text-muted'>El permiso se ha rechazado correctamente.</small>
                </>
              )
            }
            {
              status === 'create' && (
                <div className='row d-flex justify-content-between'>
                  <div className='col' />
                  <div className='col d-flex justify-content-end'>
                    <span className='btn-icon btn btn-yellow text-white text-uppercase desactivo bg-yellow' onClick={closeModal}>Cerrar</span>
                  </div>
                </div>
              )
            }
          </div>
        </div>
      </NewPermitLayout>
    </div>
  )
}

const mapDispatchToProps = {
  removePermitRequestSucceeded
}

const mapStateToProps = state => {
  return {
    permitRequestSucceeded: state.permitRequestSucceeded
  }
}

const ConnectedComponent = connect(mapStateToProps, mapDispatchToProps)(PermitRequestSucceededContainer)
export { ConnectedComponent as PermitRequestSucceeded }
