import React from 'react'

export const Pagination = ({ numberToShow, totalLength, pageNumber, changePage, changeNumberToShow }) => {
  const pages = []

  for (let i = 1; i <= Math.ceil(totalLength / numberToShow); i++) {
    pages.push(i)
  }

  const onChangeSelect = (event) => {
    changeNumberToShow(event.target.value)
  }

  return (
    <div className='row align-items-center d-flex justify-content-between card-footer py-4 m-0'>
      <div className='col-md-6 col-xs-6 col-sm-6 col-lg-6'>
        <div className='dataTables_length m-0 p-0 text-sm'>
          <label htmlFor='' className='m-0 text-muted'>
                        Mostrar
            <select name='' id='' defaultValue={numberToShow} className='d-inline text-muted' onChange={onChangeSelect}>
              <option value='10'>10</option>
              <option value='25'>25</option>
              <option value='50'>50</option>
              <option value='100'>100</option>
            </select>
                        resultados.
          </label>
          <span className='react-bootstrap-table-pagination-total text-muted'> Mostrando {numberToShow} residentes de {totalLength}.</span>
        </div>
      </div>
      <div className='col-md-6 col-xs-6 col-sm-6 col-lg-6'>
        <nav className=''>
          <ul className='pagination justify-content-end mb-0'>
            {
              totalLength / numberToShow > 1 && (
                <>
                  <li className='page-item disabled'>
                    <span className='page-link'><i className='fas fa-angle-left' /></span>
                  </li>
                  {
                    pages.map((item, index) => {
                      const itemClass = (item === pageNumber) ? 'page-item active' : 'page-item'
                      return (
                        <li className={itemClass} key={index}>
                          <span className='page-link' onClick={() => changePage(item)}>{item}</span>
                        </li>
                      )
                    })
                  }
                </>
              )
            }
          </ul>
        </nav>
      </div>
    </div>
  )
}
