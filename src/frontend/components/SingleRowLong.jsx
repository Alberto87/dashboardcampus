import React, { useState } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { SingleRowPermitStatusColumn } from './SingleRowPermitStatusColumn'
import { SingleRowPermitReason } from './SingleRowPermitReason'
import { SingleRowAvatarColumn } from './SIngleRowAvatarColumn'

export const SingleRowLongContainer = ({ permit = {}, handleNoteModal, user }) => {
  var moment = require('moment')
  require('moment/locale/es')

  const [showSubMenu, setShowMenu] = useState(false)
  const permitRoute = `/permisos-ausencia/${permit.id}`
  const handleSubMenu = () => {
    if (showSubMenu) {
      setShowMenu(false)
    } else {
      setShowMenu(true)
    }
  }
  return (
    <tr>
      {
        (() => {
          if (user.role === 'residence' || user.role === 'kitchen') {
            return (
              <SingleRowAvatarColumn permit={permit} />
            )
          } else {
            return null
          }
        })()
      }
      <SingleRowPermitStatusColumn permit={permit} />
      <td className='dia1'>
        <span>{moment(permit.dayStart).format('ddd, DD MMM')}</span>
      </td>
      <td className='dia1'>
        <span>{moment(permit.dayEnd).format('ddd, DD MMM')}</span>
      </td>
      <SingleRowPermitReason permit={permit} />
      {
        permit.comment === '' && (
          <td>
            <span className='text-muted'>Sin nota</span>
          </td>
        )
      }
      {
        permit.comment !== '' && (
          <td>
            <button type='button' className='btn btn-sm btn-neutral-2' onClick={() => { handleNoteModal(permit.comment) }}>
              <i className='far fa-sticky-note mr-1' />
              Ver nota
            </button>
          </td>
        )
      }
      <td className='fecha-1'>
        <span className='text-light'>11:15h, 1 May 2020</span>
      </td>
      <td className='fecha-1'>
        <span className='text-light'>11:15h, 1 May 2020</span>
      </td>
      <td className='acciones-2'>
        <div className={showSubMenu ? 'dropdown show' : 'dropdown'}>
          <button className='accion accion2 btn btn-secondary btn-sm scale-in-center text-light' onClick={handleSubMenu}>
            <i className='fas fa-ellipsis-v' />
          </button>
          <div className={showSubMenu ? 'dropdown-menu dropdown-menu-right dropdown-menu-arrow show' : 'dropdown-menu dropdown-menu-right dropdown-menu-arrow'}>
            <Link to={permitRoute} className='dropdown-item' onClick={handleSubMenu}>Ver permiso</Link>
          </div>
        </div>
      </td>
    </tr>
  )
}

const mapStateToProps = state => {
  return {
    user: state.user
  }
}

const ConnectedComponent = connect(mapStateToProps, null)(SingleRowLongContainer)
export { ConnectedComponent as SingleRowLong }
