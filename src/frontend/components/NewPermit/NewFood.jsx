import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { MuiPickersUtilsProvider, DatePicker } from '@material-ui/pickers'
import MomentUtils from '@date-io/moment'
import { addFoodPermit, checkFoodMatchingPermit, modifyFoodPermit } from '../../actions'

import { NewPermitLayout } from '../../layouts/NewPermitLayout'
import { ModalHeader } from './components/ModalHeader'
import { ModalBody } from './components/ModalBody'
import { Instructions } from './components/Instructions'

const NewFoodContainer = ({
  openedModal,
  handleNewPermit,
  typeOfPermit,
  residenceProfiles = [],
  addFoodPermit,
  modifyFoodPermit,
  checkFoodMatchingPermit,
  matchingFoodPermit = {}
}) => {
  var moment = require('moment')
  require('moment/locale/es')

  const [startHour, setStartHour] = useState('')
  const [endHour, setEndHour] = useState('')
  const [started, setStarted] = useState(false)
  const [greenLight, setGreenLight] = useState(false)
  const [selectedDate, setSetlectedDate] = useState(moment().format('YYYY-MM-DD'))
  const [picnic, setPicnic] = useState(false)
  const [picnicSelected, setPicnicSelected] = useState(false)
  const [matchingPermit, setMatchingPermit] = useState(false)
  const [editedForm, setEditedForm] = useState({})
  const [minDate, setMinDate] = useState(moment().format('YYYY-MM-DD'))
  const [form, setForm] = useState(
    {
      mealName: typeOfPermit,
      eatHere: true,
      picnic: false,
      date: moment().format('YYYY-MM-DD HH:mm:ss'),
      onTime: true,
      anticipation: false,
      delay: false,
      note: '',
      picnic_date: null
    }
  )

  useEffect(() => {
    const selectedProfile = residenceProfiles.find(item => item.name === typeOfPermit)
    setStartHour(selectedProfile.start_hour)
    setEndHour(selectedProfile.end_hour)
  }, [])

  useEffect(() => {
    const selectedProfile = residenceProfiles.find(item => item.name === typeOfPermit)
    const limitHour = selectedProfile.limit_hour
    const limitDate = moment().format('YYYY-MM-DD' + ' ' + limitHour)
    const now = moment().format('YYYY-MM-DD HH:mm')
    const a = moment(now)
    const b = moment(limitDate)
    const diff = b.diff(a, 'minutes', true)
    if (diff < 0) {
      setMinDate(moment().add(1, 'd').format('YYYY-MM-DD'))
      setSetlectedDate(moment().add(1, 'd').format('YYYY-MM-DD'))
      checkMatchingPermits(moment().add(1, 'd').format('YYYY-MM-DD HH:mm:ss'))
    }
  }, [])

  useEffect(() => {
    setMatchingPermit(!(Object.keys(matchingFoodPermit).length === 0))
    if (!(Object.keys(matchingFoodPermit).length === 0)) {
      setEditedForm(matchingFoodPermit)
      setPicnic(matchingFoodPermit.eatHere)
      setPicnicSelected(matchingFoodPermit.picnic)
    }
  }, [matchingFoodPermit])

  // Setting date of permit
  const handleChangeDate = (date) => {
    if (started) {
      checkMatchingPermits(date)
    }
    const setDate = moment(date).format('YYYY-MM-DD HH:mm:ss')
    const partialForm = form
    partialForm.date = setDate
    const selected = moment(date).format('YYYY-MM-DD')
    setForm(partialForm)
    setSetlectedDate(selected)
  }

  // Set where student eat
  const handleInputWhere = (event) => {
    setStarted((event.target.value !== 'none'))
    checkMatchingPermits(selectedDate)
    if ((event.target.value !== 'none')) {
      if (!matchingPermit) {
        const partialForm = form
        partialForm.eatHere = (event.target.value === 'true')
        partialForm.picnic = (event.target.value === 'true') ? false : form.picnic
        setForm(partialForm)
        setPicnic((event.target.value === 'true'))
        if ((event.target.value === 'true')) {
          setPicnicSelected(false)
        }
      } else {
        const partialEditedForm = Object.assign({}, editedForm)
        partialEditedForm.eatHere = (event.target.value === 'true')
        partialEditedForm.picnic = (event.target.value === 'true') ? false : matchingFoodPermit.picnic
        setPicnic((event.target.value === 'true'))
        const picnicSelected = (event.target.value === 'true') ? false : partialEditedForm.picnic
        setPicnicSelected(picnicSelected)
        setGreenLight((JSON.stringify(partialEditedForm) !== JSON.stringify(matchingFoodPermit)))
        setEditedForm(partialEditedForm)
      }
    } else {
      setGreenLight(false)
      setPicnicSelected(false)
    }
  }

  // Check if matching permits

  const checkMatchingPermits = (selectedDate) => {
    const dateToCheck = moment(selectedDate).format('YYYY-MM-DD HH:mm:ss')
    const dateInfo = {
      dateToSend: dateToCheck,
      name: typeOfPermit
    }
    checkFoodMatchingPermit(dateInfo)
  }

  // Set when student eat
  const handleInputWhen = (event) => {
    if (!matchingPermit) {
      const inputValue = event.target.value
      if ((event.target.value === 'none')) {
        setGreenLight(false)
      } else {
        if (event.target.value === 'onTime' && form.note === '') {
          setGreenLight(false)
        } else {
          setGreenLight(true)
        }
      }

      if (inputValue !== 'none') {
        const partialForm = form
        partialForm.onTime = (inputValue === 'onTime')
        partialForm.anticipation = (inputValue === 'before')
        partialForm.delay = (inputValue === 'after')
        setForm(form)
      }
    } else {
      const inputValue = event.target.value
      if ((event.target.value === 'none')) {
        setGreenLight(false)
      } else {
        if (event.target.value === 'onTime' && editedForm.note === '') {
          setGreenLight(false)
        } else {
          setGreenLight(true)
        }

        if (inputValue !== 'none') {
          const partialEditedForm = Object.assign({}, editedForm)
          partialEditedForm.whenEat = inputValue
          setGreenLight((JSON.stringify(partialEditedForm) !== JSON.stringify(matchingFoodPermit)))
          setEditedForm(partialEditedForm)
        }
      }
    }
  }

  // Set note student eat
  const handleInputNote = (event) => {
    if (!matchingPermit) {
      const note = event.target.value
      const partialForm = form
      partialForm.note = note
      setForm(partialForm)

      if (form.onTime) {
        if (event.target.value === '') {
          setGreenLight(false)
        } else {
          setGreenLight(true)
        }
      }
    } else {
      const note = event.target.value
      const partialEditedForm = Object.assign({}, editedForm)
      partialEditedForm.note = note
      setEditedForm(partialEditedForm)
      if (partialEditedForm.whenEat === 'onTime') {
        if (event.target.value === '') {
          setGreenLight(false)
        } else {
          setGreenLight((JSON.stringify(partialEditedForm) !== JSON.stringify(matchingFoodPermit)))
        }
      }
    }
  }

  // Set picnic option
  const handleInputPicnic = (event) => {
    setGreenLight((event.target.value !== 'none'))
    if ((event.target.value !== 'none')) {
      if (!matchingPermit) {
        const partialForm = form
        partialForm.picnic = (event.target.value === 'true')
        setForm(partialForm)
        setPicnicSelected((event.target.value === 'true'))
      } else {
        const partialEditedForm = Object.assign({}, editedForm)
        partialEditedForm.picnic = (event.target.value === 'true')
        console.log(partialEditedForm)
        setPicnicSelected((event.target.value === 'true'))
        setGreenLight((JSON.stringify(partialEditedForm) !== JSON.stringify(matchingFoodPermit)))
        setEditedForm(partialEditedForm)
      }
    } else {
      setPicnicSelected(false)
    }
  }

  // Set picnic time

  const handleInputPicnicTime = (event) => {
    const picnicTime = !(event.target.value === '') ? moment(selectedDate + ' ' + event.target.value + ':00').format('YYYY-MM-DD HH:mm:ss') : null
    const partialForm = form
    partialForm.picnic_date = picnicTime
    setForm(partialForm)
    if (matchingPermit) {
      const editedPicnicTime = !(event.target.value === '') ? moment(selectedDate + ' ' + event.target.value + ':00').format('YYYY-MM-DD HH:mm:ss') : null
      const partialEditedForm = Object.assign({}, editedForm)
      partialEditedForm.picnic_date = editedPicnicTime
      setGreenLight((JSON.stringify(partialEditedForm) !== JSON.stringify(matchingFoodPermit)))
      setEditedForm(partialEditedForm)
    }
  }

  const handleSubmit = (event) => {
    event.preventDefault()
    if (!matchingPermit) {
      addFoodPermit(form)
    } else {
      modifyFoodPermit(editedForm)
    }
  }

  return (
    <div className={openedModal ? 'modal modal-background fade show' : 'modal modal-background fade d-none'}>
      <MuiPickersUtilsProvider utils={MomentUtils}>
        <NewPermitLayout>
          <ModalHeader
            handleNewPermit={handleNewPermit}
            typeOfPermit={typeOfPermit}
          />
          <ModalBody>
            <Instructions
              startHour={startHour}
              endHour={endHour}
              typeOfPermit={typeOfPermit}
            />
            <form action='' onSubmit={handleSubmit}>
              <div className='form-group row'>
                <label htmlFor='example-date-input' className='col-md-3 col-form-label form-control-label'>Día de comida</label>
                <div className='col-md-9'>
                  <div className='input-group-prepend'>
                    <span className='input-group-text'>
                      <i className='far fa-calendar-day text-muted' />
                    </span>
                    <DatePicker
                      minDate={minDate}
                      value={selectedDate}
                      disableToolbar
                      variant='inline'
                      disablePast
                      onChange={(date) => handleChangeDate(date)}
                    />
                  </div>
                  {
                    matchingPermit && (<p>Estás modificando un permiso existente</p>)
                  }
                </div>
              </div>

              <div className='form-group row'>
                {
                  typeOfPermit === 'cena'
                    ? <label htmlFor='' className='col-md-3 example-text-input'>¿Cenas en el alojamiento? *</label>
                    : <label htmlFor='' className='col-md-3 example-text-input'>Comes en el alojamiento? *</label>
                }
                <div className='col-md-9'>
                  <select name='' id='' className='form-control' onChange={handleInputWhere}>
                    <option value='none'> -- Selecciona una opción -- </option>
                    <option value='true' selected={matchingPermit && matchingFoodPermit.eatHere}>Sí</option>
                    <option value='false' selected={matchingPermit && !matchingFoodPermit.eatHere}>No</option>
                  </select>
                </div>
              </div>
              {
                started && (
                  <>
                    {
                      picnic && (
                        <div className='form-group row'>
                          {
                            typeOfPermit === 'cena'
                              ? <label htmlFor='' className='col-md-3 col-form-label form-control-label py-0'>¿A qué hora cenarás? *</label>
                              : <label htmlFor='' className='col-md-3 col-form-label form-control-label py-0'>¿A qué hora comerás? *</label>
                          }
                          <div className='col-md-9'>

                            <select name='' id='' className='form-control' onChange={handleInputWhen}>
                              <option value='none'> -- Selecciona una opción -- </option>
                              <option value='before' selected={matchingPermit && matchingFoodPermit.whenEat === 'before'}>30 minutos antes</option>
                              <option value='onTime' selected={matchingPermit && matchingFoodPermit.whenEat === 'onTime'}>A tiempo</option>
                              <option value='after' selected={matchingPermit && matchingFoodPermit.whenEat === 'after'}>30 minutos después</option>
                            </select>
                          </div>
                        </div>
                      )
                    }
                    {
                      !picnic && (
                        <div className='form-group row'>
                          <label htmlFor='' className='col-md-3 col-form-label form-control-label py-0'>¿Quieres un picnic? *</label>
                          <div className='col-md-9'>
                            <select name='' id='' className='form-control' onChange={handleInputPicnic}>
                              <option value='none'> -- Selecciona una opción -- </option>
                              <option value='true' selected={matchingPermit && matchingFoodPermit.picnic}>Sí</option>
                              <option value='false' selected={matchingPermit && !matchingFoodPermit.picnic}>No</option>
                            </select>
                          </div>
                        </div>
                      )
                    }
                    {
                      picnicSelected && (
                        <div className='form-group row'>
                          <label htmlFor='' className='col-md-3 col-form-label form-control-label py-0'>Hora de recogida</label>
                          <div className='col-md-9'>
                            <div className='input-group'>
                              <div className='input-group-prepend'>
                                <span className='input-group-text'>
                                  <i className='far fa-clock text-muted' />
                                </span>
                              </div>
                              {
                                matchingPermit && (
                                  <input className='form-control timepicker' placeholder='Selecciona un día' type='time' defaultValue={moment(matchingFoodPermit.picnic_date).format('HH:mm')} onChange={handleInputPicnicTime} />
                                )
                              }
                              {
                                !matchingPermit && (
                                  <input className='form-control timepicker' placeholder='Selecciona un día' type='time' onChange={handleInputPicnicTime} />
                                )
                              }

                            </div>
                          </div>
                        </div>
                      )
                    }
                    <div className='form-group row'>
                      <label htmlFor='' className='col-md-3 col-form-label form-control-label py-0'>Nota</label>
                      <div className='col-md-9'>
                        {
                          matchingPermit && (
                            <textarea name='' id='' className='form-control' rows='3' onChange={handleInputNote} defaultValue={matchingFoodPermit.note} />
                          )
                        }
                        {
                          !matchingPermit && (
                            <textarea name='' id='' className='form-control' rows='3' onChange={handleInputNote} />
                          )
                        }
                      </div>
                    </div>
                    {
                      greenLight && (
                        <div className='row d-flex justify-content-between'>
                          <div className='col' />
                          <div className='col d-flex justify-content-end'>
                            <button className='btn-icon btn btn-yellow text-white text-uppercase desactivo bg-yellow'>{matchingPermit ? 'Editar Permiso' : 'Añadir permiso'}</button>
                          </div>
                        </div>
                      )
                    }
                  </>
                )
              }
            </form>
          </ModalBody>
        </NewPermitLayout>
      </MuiPickersUtilsProvider>
    </div>
  )
}

const mapDispatchToProps = {
  addFoodPermit,
  checkFoodMatchingPermit,
  modifyFoodPermit
}

const mapStateToProps = state => {
  return {
    residenceProfiles: state.initialData.residenceProfiles,
    matchingFoodPermit: state.matchingFoodPermit
  }
}

const ConnectedComponent = connect(mapStateToProps, mapDispatchToProps)(NewFoodContainer)
export { ConnectedComponent as NewFood }
