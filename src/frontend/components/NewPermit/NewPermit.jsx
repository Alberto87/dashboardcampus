import React from 'react'
import { MuiPickersUtilsProvider } from '@material-ui/pickers'
import MomentUtils from '@date-io/moment'

import { NewPermitLayout } from '../../layouts/NewPermitLayout'
import { ModalHeader } from './components/ModalHeader'
import { ModalBody } from './components/ModalBody'
import { Instructions } from './components/Instructions'
import { TonightPermitForm } from '../Forms/TonightPermitForm'
import { ExtraordinaryPermitForm } from '../Forms/ExtraordinaryPermitForm'
import { AbscencePermitForm } from '../Forms/AbscencePermitForm'

export const NewPermit = ({ openedModal, handleNewPermit, type }) => {
  return (
    <div className={openedModal ? 'modal modal-background fade show' : 'modal modal-background fade d-none'}>
      <MuiPickersUtilsProvider utils={MomentUtils}>
        <NewPermitLayout>
          <ModalHeader handleNewPermit={handleNewPermit} typeOfPermit={type} />
          <ModalBody>
            <Instructions typeOfPermit={type} />
            {
              type === 'tonight' && (
                <TonightPermitForm />
              )
            }
            {
              type === 'extraordinary' && (
                <ExtraordinaryPermitForm />
              )
            }
            {
              type === 'abscence' && (
                <AbscencePermitForm />
              )
            }
          </ModalBody>
        </NewPermitLayout>
      </MuiPickersUtilsProvider>
    </div>
  )
}
