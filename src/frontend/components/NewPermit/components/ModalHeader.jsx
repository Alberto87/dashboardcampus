import React from 'react'

export const ModalHeader = ({ typeOfPermit, handleNewPermit }) => (
  <div className='modal-header custom-modal-header align-items-center'>
    {
      typeOfPermit === 'almuerzo' && (
        <h6 className='modal-title custom-modal-title'>
          <i className='far fa-suitcase mr-1' />
                Nuevo permiso de comida
        </h6>
      )
    }
    {
      typeOfPermit === 'cena' && (
        <h6 className='modal-title custom-modal-title'>
          <i className='far fa-suitcase mr-1' />
                Nuevo permiso de comida
        </h6>
      )
    }
    {
      typeOfPermit === 'tonight' && (
        <h6 className='modal-title custom-modal-title'>
          <i className='far fa-suitcase mr-1' />
                Nueva notificación de salida nocturna
        </h6>
      )
    }
    {
      typeOfPermit === 'extraordinary' && (
        <h6 className='modal-title custom-modal-title'>
          <i className='far fa-suitcase mr-1' />
                Nuevo permiso extraordinario
        </h6>
      )
    }
    {
      typeOfPermit === 'abscence' && (
        <h6 className='modal-title custom-modal-title'>
          <i className='far fa-suitcase mr-1' />
            Nuevo permiso de ausencia
        </h6>
      )
    }
    <span className='close' type='button' data-dismiss='moda1' aria-label='Close' onClick={handleNewPermit}>
      <span>×</span>
    </span>
  </div>
)
