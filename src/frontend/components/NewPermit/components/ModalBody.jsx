import React from 'react'

export const ModalBody = ({ children }) => (
  <div className='modal-body py-3 px-4'>
    <div className='card-body'>
      {children}
    </div>
  </div>
)
