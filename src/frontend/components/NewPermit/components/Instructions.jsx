import React from 'react'

export const Instructions = ({ startHour, endHour, typeOfPermit }) => {
  return (
    <div className='card card-instrucciones mb-4'>
      <div className='card-header'>
        {
          typeOfPermit === 'cena' && (
            <p className='mb-0 text-sm'>
              <i className='far fa-info-circle mr-1' />
                        El horario de la cena es de <strong>{startHour}h a {endHour}h</strong>.
            </p>
          )
        }
        {
          typeOfPermit === 'almuerzo' && (
            <p className='mb-0 text-sm'>
              <i className='far fa-info-circle mr-1' />
                        El horario del almuerzo es de <strong>{startHour}h a {endHour}h</strong>.
            </p>
          )
        }
        {
          typeOfPermit === 'tonight' && (
            <p className='mb-0 text-sm'>
              <i className='far fa-info-circle mr-1' />
                        Es una notificación de salida dentro de tu perfil de matricula. No necesita validación por parte de tu tutor.
            </p>
          )
        }
        {
          typeOfPermit === 'extraordinary' && (
            <p className='mb-0 text-sm'>
              <i className='far fa-info-circle mr-1' />
                        Este permiso necesita validación por parte de tu tutor.
            </p>
          )
        }
        {
          typeOfPermit === 'abscence' && (
            <>
              <p className='mb-0 text-sm'>
                <i className='far fa-info-circle mr-1' />
                Este permiso comprende mínimo 2 noches fuera del alojamiento.
              </p>
              <p className='mb-0 text-sm'>
                <i className='far fa-user-check mr-1' />
              Este permiso requiere validación por tu tutor.
              </p>
            </>
          )
        }
      </div>
    </div>
  )
}
